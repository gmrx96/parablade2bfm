import sys
import os
from matplotlib import lines
import matplotlib.pyplot as plt
import numpy as np


# from itertools import islice

# def take(n, iterable):
#     "Return first n items of the iterable as a list"
#     return list(islice(iterable, n))


# Read the results file ---------------------------------------
X = []
L = []
risposta = []
file1 = open('C:\\Users\\GMrx1\\Desktop\\saFA\\BFM_results.txt', 'r')
file2 = open('C:\\Users\\GMrx1\\Desktop\\saFA\\BFM_results_CFX_nopost.txt', 'r')
Lines = file1.readlines()
Lines2 = file2.readlines()
file1.close()
file2.close()

cfx_results=[]
for i in range(2,len(Lines2)):
    cfx_results.append(float(Lines2[i].split()[-1]))

index=0
while Lines[0].split()[index] != "eff":
    index +=1
#---------------------------------------------------------------


# Create sensitivity analysis class --------------------------------
SA = type('Sensitivity',(object,),{})

Var = {}
for i in range(2,index):
    Var[Lines[0].split()[i]] = float(Lines[1].split()[i])
setattr(SA,"variables",Var)

Res = {'eff': float(Lines[1].split()[index])}
setattr(SA,"responses",Res)
#-------------------------------------------------------------

# Get the finite differences (diagonal matrix)----------------------------------
i=2
FinD = {}
FinD_abs = {}
for j in range(2,len(Lines)):
    FinD[Lines[0].split()[i]] = (float(Lines[j].split()[index])-float(Lines[1].split()[index])) / (float(Lines[j].split()[i])-float(Lines[1].split()[i]))
    FinD_abs[Lines[0].split()[i]] = abs(  (float(Lines[j].split()[index])-float(Lines[1].split()[index])) / (float(Lines[j].split()[i])-float(Lines[1].split()[i]))  )
    i+=1    
setattr(SA,"forwarddiff",FinD)
setattr(SA,"forwarddiff_abs",FinD_abs)
#--------------------------------------------------------------
     
del Var,Res,FinD


chiavi = list(SA.forwarddiff.keys())
print(type(chiavi))

# -----------------------reeat for CFX-----------------
i=2
FinD_CFX = {}
FinD_CFX_abs = {}
for j in range(2,len(Lines2)):
    FinD_CFX[Lines2[0].split()[i]] = (float(Lines2[j].split()[index])-float(Lines2[1].split()[index])) / (float(Lines2[j].split()[i])-float(Lines2[1].split()[i]))
    FinD_CFX_abs[Lines2[0].split()[i]] = abs(  (float(Lines2[j].split()[index])-float(Lines2[1].split()[index])) / (float(Lines2[j].split()[i])-float(Lines2[1].split()[i]))  )
    i+=1    

# lista2 = FinD_CFX
print(list(FinD_CFX.values()))
#------------------------------------------------------

fig,ax = plt.subplots(2,1,figsize=(10, 10),sharex=True)
ax[0].barh(list(SA.forwarddiff.keys()),list(SA.forwarddiff.values()))
ax[1].barh(chiavi,list(FinD_CFX.values()))

# plt.barh(list(SA.forwarddiff.keys()),list(SA.forwarddiff.values()))
# # plt.barh(list(SA.forwarddiff.values()),list(SA.forwarddiff.keys()))
# plt.title('Sensitivity analysis')
# plt.ylabel('Forward finite differences')
# plt.xlabel('Values')
ax[0].grid()
ax[1].grid()
plt.show()


top20 = list(SA.forwarddiff_abs.values())
a=sorted(SA.forwarddiff_abs.items(), key=lambda x: x[1], reverse=True)
max_fd = a[0][-1]
top20 = a[:20]
top_n_keys= []
top_n_values = []
n=0
treshold = 0.05
for i in range(len(a)):
    if a[i][-1]/a[0][-1] >=treshold:
        top_n_keys.append(a[i][0])
        top_n_values.append(a[i][-1])
        n+=1
plt.barh(top_n_keys,top_n_values)
plt.title("Sensitivity analysis of top "+str(n)+" finite differences (>"+str(treshold)+"*MAX FD)")
plt.ylabel('Forward finite differences')
plt.xlabel('Values')
plt.grid()
plt.show()



