#import matplotlib.pyplot as plt
import csv
import os
import numpy as np
import math
import matplotlib


matplotlib.rc('font',family='Times New Roman')

#import pylab 
#params = {'text.usetex': False, 'mathtext.fontset': 'Times New Roman'}
#pylab.rcParams.update(params)

#matplotlib.rcParams['text.usetex'] = True 
#matplotlib.rcParams['text.latex.preamble'] = [r'\usepackage[cm]{sfmath}']
#matplotlib.rcParams['font.family'] = 'sans-serif'
#matplotlib.rcParams['font.sans-serif'] = 'cm'


plt = matplotlib.pyplot
plt.rcParams["font.family"] = "Times New Roman"

#plt.rcParams['mathtext.fontset'] = 'stix'

class Comparison_plot():
    
   

    def __init__(self,CFX_post_folder,PARAVIEW_post_folder):
        
        self.PARAVIEW_plot_folder = PARAVIEW_post_folder +"\\plots"
        if not os.path.isdir(self.PARAVIEW_plot_folder):
            os.system("mkdir "+self.PARAVIEW_plot_folder)

        self.CFX_axial_folder= CFX_post_folder+"\\axial_data"
        self.CFX_radial_folder= CFX_post_folder+"\\radial_data"
        self.CFX_objective_folder= CFX_post_folder+"\\objectives"

        self.PARAVIEW_axial_folder = PARAVIEW_post_folder+"\\axial_data"
        self.PARAVIEW_radial_folder = PARAVIEW_post_folder+"\\radial_data"
        self.PARAVIEW_time_folder = PARAVIEW_post_folder+"\\time"
        self.PARAVIEW_objective_folder = PARAVIEW_post_folder+"\\objectives"

        self.CFX_axial_files = []
        self.CFX_radial_files = []
        self.CFX_objective_file = os.path.join(self.CFX_objective_folder, os.listdir(self.CFX_objective_folder)[0])
        self.PARAVIEW_axial_files = []
        self.PARAVIEW_radial_files = []
        self.PARAVIEW_time_files = []
        self.PARAVIEW_objective_files = []

        self.PARAVIEW_axial_legend = []
        self.PARAVIEW_radial_legend = []
        self.my_colors=['m','c','r','b']
        self.my_markers=['o','s','v','D']

        self.station = ['Inlet','LE','TE','Outlet']
        self.csfont = {'fontname':'Times New Roman'}

        for file in os.listdir(self.PARAVIEW_axial_folder):
            self.PARAVIEW_axial_files.append(os.path.join(self.PARAVIEW_axial_folder, file))
            leg = file.split("\\")[-1]
            leg = leg.replace("_axial_data.csv","")
            if int(leg.split('x')[-1])>10:
                if 'HALL' in file:
                    # leg = leg.replace("flow","HALL_{FA}")
                    leg = "HALL$_{FA}"
                elif 'THOLLET' in file:
                    # leg = leg.replace("flow","THOLLET_{FA}")
                    leg = "THOLLET$_{FA}"
            else:
                if 'HALL' in file:
                    # leg = leg.replace("flow","HALL_{PER}") 
                    leg = "HALL$_{PER}"               
                elif 'THOLLET' in file:
                    # leg = leg.replace("flow","THOLLET_{PER}")
                    leg = "THOLLET$_{PER}"

            leg = leg+'$'

            self.PARAVIEW_axial_legend.append(leg)

        for file in os.listdir(self.PARAVIEW_radial_folder):
            self.PARAVIEW_radial_files.append(os.path.join(self.PARAVIEW_radial_folder, file))
            leg = '$'+file.split("\\")[-1]
            
            leg = leg.replace("_TE_radial_data.csv","")
            leg = leg.replace("_LE_radial_data.csv","")
            leg = leg.replace("_Outlet_radial_data.csv","")
            leg = leg.replace("_Inlet_radial_data.csv","")
            if int(leg.split('x')[-1])>10:
                if 'HALL' in file:
                    # leg = leg.replace("flow","HALL_{FA}")
                    leg = "HALL$_{FA}"
                elif 'THOLLET' in file:
                    # leg = leg.replace("flow","THOLLET_{FA}")
                    leg = "THOLLET$_{FA}"
            else:
                if 'HALL' in file:
                    # leg = leg.replace("flow","HALL_{PER}")
                    leg = "HALL$_{PER}"                
                elif 'THOLLET' in file:
                    # leg = leg.replace("flow","THOLLET_{PER}")
                    leg = "THOLLET$_{PER}"
            leg = leg+'$'
            self.PARAVIEW_radial_legend.append(leg)

        for file in os.listdir(self.PARAVIEW_time_folder):
            self.PARAVIEW_time_files.append(os.path.join(self.PARAVIEW_time_folder, file))
        for file in os.listdir(self.PARAVIEW_objective_folder):
            self.PARAVIEW_objective_files.append(os.path.join(self.PARAVIEW_objective_folder, file))

        self.R_radial_CFX = []

        self.Pav_radial_CFX = []
        self.Pav_radial_CFX_plus5=[]
        self.Pav_radial_CFX_min5=[]
        self.Pav_radial_CFX_plus10=[]
        self.Pav_radial_CFX_min10=[]

        self.Tav_radial_CFX = []
        self.Tav_radial_CFX_plus5=[]
        self.Tav_radial_CFX_min5=[] 
        self.Tav_radial_CFX_plus10=[]
        self.Tav_radial_CFX_min10=[]

        self.Ptotav_radial_CFX = []
        self.Ptotav_radial_CFX_plus5=[]
        self.Ptotav_radial_CFX_min5=[]
        self.Ptotav_radial_CFX_plus10=[]
        self.Ptotav_radial_CFX_min10=[]

        self.Ttotav_radial_CFX = []
        self.Ttotav_radial_CFX_plus5=[]
        self.Ttotav_radial_CFX_min5=[] 
        self.Ttotav_radial_CFX_plus10=[]
        self.Ttotav_radial_CFX_min10=[]

        self.alphaav_radial_CFX = []
        self.alphaav_radial_CFX_plus5=[]
        self.alphaav_radial_CFX_min5=[] 
        self.alphaav_radial_CFX_plus10=[]
        self.alphaav_radial_CFX_min10=[]

        self.Mach_radial_CFX = []
        self.Mach_radial_CFX_plus5=[]
        self.Mach_radial_CFX_min5=[] 
        self.Mach_radial_CFX_plus10=[]
        self.Mach_radial_CFX_min10=[]

        self.dS_radial_CFX = []
        self.dS_radial_CFX_plus5=[]
        self.dS_radial_CFX_min5=[] 
        self.dS_radial_CFX_plus10=[]
        self.dS_radial_CFX_min10=[]


        for file in os.listdir(self.CFX_axial_folder):
            self.CFX_axial_files.append(os.path.join(self.CFX_axial_folder, file))
        
        for file in os.listdir(self.CFX_radial_folder):
            self.CFX_radial_files.append(os.path.join(self.CFX_radial_folder, file))
       
        
        self.X_axial_para = []
        self.Pav_axial_para = []
        self.Ptotav_axial_para = []
        self.Tav_axial_para = []
        self.Ttotav_axial_para = []
        self.alphaav_axial_para = []
        self.mdot_axial_para = []
        self.Mach_axial_para = []
        self.mfluxav_axial_para = []
        self.dS_axial_para = []

        self.Ptotav_IN_para = []
        self.Ttotav_IN_para = []

        for axial_file in self.PARAVIEW_axial_files:
            self.get_paraview_axial_results(axial_file)
        
        for axial_file in self.CFX_axial_files:
            self.get_CFX_axial_results(axial_file)
        
        self.R_radial_para = []
        self.Pav_radial_para = []
        self.Tav_radial_para = []
        self.alphaav_radial_para = []
        self.Mach_radial_para = []
        self.Ptotav_radial_para = []
        self.Ttotav_radial_para = []

        for radial_file in self.CFX_radial_files:
            self.get_CFX_radial_results(radial_file)

        
        for radial_file in self.PARAVIEW_radial_files:
            self.get_paraview_radial_results(radial_file)
        
        # ----------------------------PLOTTING----------------------------------------------#
        self.csfont = {'fontname':'Times New Roman'}
        self.plot_axial_results()
        self.plot_axial_error()
        self.plot_radial_results()
        # self.plot_time()
        # self.plot_objectives()
        # self.plot_tangential_nodes()

  












    def plot_tangential_nodes(self):
        times = []
        nodes = []
        for file in self.PARAVIEW_time_files:
            node = file.split("\\")
            node=node[-1].split("x")
            
            # -------For tangential node study-------#
            node=node[1].split(".")
            node = int(node[0])
            #--------------------------------------#

            nodes.append(node)
            # nodes.sort()
            
            file1 = open(file, 'r')
            Lines = file1.readlines()
            time = Lines[0].split("=")
            time=float(time[-1].replace("s",""))
            times.append(time)
            # times.sort()
        plt.plot( nodes,times, '-o')
        plt.title('Computational time',fontsize = 22)
        plt.grid()
        plt.ylabel(r'$Time$ [$s$]',fontsize = 18)
        plt.xlabel('Tangential nodes',fontsize = 18)
        plt.show()

        eta_tt = []
        power = []
        m_dot = []
        alpha_out = []

        for j in range(len(nodes)):
            for file in self.PARAVIEW_objective_files:
                if str(nodes[j]) in file.split('\\')[-1]:
                    file1 = open(file, 'r')
                    Lines = file1.readlines()
                    eta_tt.append(float((Lines[0].split())[-1]))
                    power.append(float((Lines[1].split())[-1]))
                    m_dot.append(float((Lines[2].split())[-1]))
                    alpha_out.append(float((Lines[3].split())[-1]))
        
        rows=[]
        cfx_obj=np.ones((4,len(nodes)))
        with open(self.CFX_objective_file,'r') as csvfile:
            plots = csv.reader(csvfile, delimiter = ',')
            for row in plots:
                rows.append(row)
        for i in range(len(rows[1])):
            
            cfx_obj[i]=float(rows[1][i].split()[0])
        # a = np.ones((len(nodes),1))*float(rows[1][0])

        plt.plot( nodes, cfx_obj[0] , '--g',label = 'CFX')
        plt.plot( nodes,eta_tt, '-o',label = 'BFM')
        plt.title('Total to total efficiency', fontsize = 22)
        plt.grid()
        plt.legend()
        plt.ylabel(r'${\eta_{TT}}$',fontsize = 18)
        plt.xlabel('Tangential nodes',fontsize = 18)
        plt.show()

        plt.plot( nodes,cfx_obj[2], '--g',label = 'CFX')
        plt.plot( nodes,power, '-o',label = 'BFM')
        plt.title('Required power', fontsize = 22)
        plt.grid()
        plt.legend()
        plt.ylabel(r'$Power$  [W]',fontsize = 18)
        plt.xlabel('Tangential nodes',fontsize = 18)
        plt.show()
        
        plt.plot( nodes,cfx_obj[1], '--g',label = 'CFX')
        plt.plot( nodes,m_dot, '-o',label = 'BFM')
        plt.title('Design mass flow', fontsize = 22)
        plt.grid()
        plt.legend()
        plt.ylabel(r'${\rho}U_x$ [$\frac{kg}{s}$]',fontsize = 18)
        plt.xlabel('Tangential nodes',fontsize = 18)
        plt.show()
        
        plt.plot( nodes,cfx_obj[3], '--g',label = 'CFX')
        plt.plot( nodes,alpha_out, '-o',label = 'BFM')
        plt.title('Exit flow angle', fontsize = 22)
        plt.grid()
        plt.legend()
        plt.ylabel(r'$\alpha$ [$\deg$]',fontsize = 18)
        plt.xlabel('Tangential nodes',fontsize = 18)
        plt.show()


    def plot_objectives(self):
        eta_tt = []
        power = []
        m_dot = []
        alpha_out = []
        nodes = []
        
        for file in self.PARAVIEW_objective_files:
            
            node = file.split("\\")
            node=node[-1].split("x")
            node = int(node[0].replace("flow",""))
            nodes.append(node)
        nodes.sort()

        for j in range(len(nodes)):
            for file in self.PARAVIEW_objective_files:
                if str(nodes[j]) in file.split('\\')[-1]:
                    file1 = open(file, 'r')
                    Lines = file1.readlines()
                    eta_tt.append(float((Lines[0].split())[-1]))
                    power.append(float((Lines[1].split())[-1]))
                    m_dot.append(float((Lines[2].split())[-1]))
                    alpha_out.append(float((Lines[3].split())[-1]))
        
        rows=[]
        cfx_obj=np.ones((4,len(nodes)))
        with open(self.CFX_objective_file,'r') as csvfile:
            plots = csv.reader(csvfile, delimiter = ',')
            for row in plots:
                rows.append(row)
        for i in range(len(rows[1])):
            
            cfx_obj[i]=float(rows[1][i].split()[0])
        # a = np.ones((len(nodes),1))*float(rows[1][0])

        plt.plot( nodes, cfx_obj[0] , '--g',label = 'CFX')
        plt.plot( nodes,eta_tt, '-o',label = 'BFM')
        plt.title('Total to total efficiency', fontsize = 22)
        plt.grid()
        plt.legend()
        plt.ylabel(r'${\eta_{TT}}$',fontsize = 18)
        plt.xlabel('Axial nodes',fontsize = 18)
        plt.show()

        plt.plot( nodes,cfx_obj[2], '--g',label = 'CFX')
        plt.plot( nodes,power, '-o',label = 'BFM')
        plt.title('Required power', fontsize = 22)
        plt.grid()
        plt.legend()
        plt.ylabel(r'$Power$  [W]',fontsize = 18)
        plt.xlabel('Axial nodes',fontsize = 18)
        plt.show()
        
        plt.plot( nodes,cfx_obj[1], '--g',label = 'CFX')
        plt.plot( nodes,m_dot, '-o',label = 'BFM')
        plt.title('Design mass flow', fontsize = 22)
        plt.grid()
        plt.legend()
        plt.ylabel(r'${\rho}U_x$ [$\frac{kg}{s}$]',fontsize = 18)
        plt.xlabel('Axial nodes',fontsize = 18)
        plt.show()
        
        plt.plot( nodes,cfx_obj[3], '--g',label = 'CFX')
        plt.plot( nodes,alpha_out, '-o',label = 'BFM')
        plt.title('Exit flow angle', fontsize = 22)
        plt.grid()
        plt.legend()
        plt.ylabel(r'$\alpha$ [$\deg$]',fontsize = 18)
        plt.xlabel('Axial nodes',fontsize = 18)
        plt.show()
        
        # Tt_in = self.Ttotav_axial_CFX[]
        # Tt_out_s = Tt_in * (Pt_out/Pt_in) ** ((gamma - 1)/gamma)
        # eta_tt = (Tt_out_s - Tt_in) / (Tt_out - Tt_in)
        # power_in = self.fluid_parameters.C_p * mdot * (Tt_out - Tt_in)
        # angle_out = self.output_data.axial_data.Alpha[-1]

        # print('hi')
            # time = Lines[0].split("=")
            # time=float(time[-1].replace("s",""))
            # times.append(time)
            # times.sort()



    def plot_time(self):
        times = []
        nodes = []
        for file in self.PARAVIEW_time_files:
            node = file.split("\\")
            node=node[-1].split("x")
            
            # # -------For tangential node study-------#
            # node=node[1].split(".")
            # node = int(node[0])
            # #--------------------------------------#

            node = int(node[0].replace("time",""))
            nodes.append(node)
            nodes.sort()
            
            file1 = open(file, 'r')
            Lines = file1.readlines()
            time = Lines[0].split("=")
            time=float(time[-1].replace("s",""))
            times.append(time)
            times.sort()
        plt.plot( nodes,times, '-o')
        plt.title('Computational time',fontsize = 22)
        plt.grid()
        plt.ylabel(r'$Time$ [$s$]',fontsize = 18)
        plt.xlabel('Axial nodes',fontsize = 18)
        # plt.xlabel('Tangential nodes',fontsize = 18)
        plt.show()

        











    
    def plot_radial_results(self):
        # Plot reference CFX results and +-5%, +-10%bounds
        for s in self.station:
            plt.figure(figsize=(10,8))
            for f in range(len(self.CFX_radial_files)):
                if self.CFX_radial_files[f].split("_")[3]==s:
                    
                    plt.plot( self.Pav_radial_CFX[f],self.R_radial_CFX[f], '--g',  label = 'CFX')
                    plt.plot(self.Pav_radial_CFX_plus5[f],self.R_radial_CFX[f],'--y',  label = 'CFX +-5%')
                    plt.plot(self.Pav_radial_CFX_min5[f],self.R_radial_CFX[f],'--y')
                    plt.plot(self.Pav_radial_CFX_plus10[f],self.R_radial_CFX[f],'-.k',  label = 'CFX +-10%')
                    plt.plot(self.Pav_radial_CFX_min10[f],self.R_radial_CFX[f],'-.k')
        
            # Plot SU2 BFM results
            for i in range(len(self.PARAVIEW_radial_files)):
                #  print(self.PARAVIEW_radial_files[i].split("_")[3])
                 if self.PARAVIEW_radial_files[i].split("_")[3]==s:
                     plt.plot(self.Pav_radial_para[i],self.R_radial_para[i],color=self.my_colors[int(i/4)])
                     for j in range(len(self.R_radial_para[i])):
                        if j%5==0:
                            if 'THOLLET' in self.PARAVIEW_radial_files[i]:
                                if j==0:
                                    plt.plot(self.Pav_radial_para[i][j],self.R_radial_para[i][j],label = self.PARAVIEW_radial_legend[i],color=self.my_colors[int(i/4)],marker=self.my_markers[int(i/4)])
                                else:
                                    plt.plot(self.Pav_radial_para[i][j], self.R_radial_para[i][j],color=self.my_colors[int(i/4)],marker=self.my_markers[int(i/4)])
                            else:
                                if j==0:
                                    plt.plot(self.Pav_radial_para[i][j],self.R_radial_para[i][j],label = self.PARAVIEW_radial_legend[i],color=self.my_colors[int(i/4)],marker=self.my_markers[int(i/4)],markerfacecolor='none')
                                else:
                                    plt.plot(self.Pav_radial_para[i][j], self.R_radial_para[i][j],color=self.my_colors[int(i/4)],marker=self.my_markers[int(i/4)],markerfacecolor='none')
            plt.title(s+' radial static pressure', fontsize = 22,**self.csfont)
            plt.grid()
            plt.legend(bbox_to_anchor=(1.01,0.95),borderaxespad=0,prop={'size': 14})
            plt.subplots_adjust(right=0.8)   ##  Need to play with this number.
            plt.ylabel(r'$R$ [m]',fontsize = 18)
            plt.xlabel(r'$\frac{P}{P_{T_{in}}}$',fontsize = 22)
            plt.xticks(fontsize=14 )
            plt.yticks(fontsize= 14)
            plt.savefig(self.PARAVIEW_plot_folder+"\\radial_"+str(s)+"_P.pdf")
            plt.close()
            # plt.show()



        # Plot reference CFX results and +-5%, +-10%bounds
        for s in self.station:
            plt.figure(figsize=(10,8))
            for f in range(len(self.CFX_radial_files)):
                if self.CFX_radial_files[f].split("_")[3]==s:
                    plt.plot(self.Tav_radial_CFX[f], self.R_radial_CFX[f],   '--g',label = 'CFX')
                    plt.plot(self.Tav_radial_CFX_plus5[f], self.R_radial_CFX[f],'--y',  label = 'CFX +-5%')
                    plt.plot(self.Tav_radial_CFX_min5[f],self.R_radial_CFX[f],'--y')
                    plt.plot(self.Tav_radial_CFX_plus10[f],self.R_radial_CFX[f],'-.k',  label = 'CFX +-10%')
                    plt.plot(self.Tav_radial_CFX_min10[f],self.R_radial_CFX[f],'-.k')
        
        # Plot SU2 BFM results
            for i in range(len(self.PARAVIEW_radial_files)):
                #  print(self.PARAVIEW_radial_files[i].split("_")[3])
                 if self.PARAVIEW_radial_files[i].split("_")[3]==s:
                     plt.plot(self.Tav_radial_para[i],self.R_radial_para[i],color=self.my_colors[int(i/4)])
                     for j in range(len(self.R_radial_para[i])):
                        if j%5==0:
                            if 'THOLLET' in self.PARAVIEW_radial_files[i]:
                                if j==0:
                                    plt.plot(self.Tav_radial_para[i][j],self.R_radial_para[i][j],label = self.PARAVIEW_radial_legend[i],color=self.my_colors[int(i/4)],marker=self.my_markers[int(i/4)])
                                else:
                                    plt.plot(self.Tav_radial_para[i][j], self.R_radial_para[i][j],color=self.my_colors[int(i/4)],marker=self.my_markers[int(i/4)])
                            else:
                                if j==0:
                                    plt.plot(self.Tav_radial_para[i][j],self.R_radial_para[i][j],label = self.PARAVIEW_radial_legend[i],color=self.my_colors[int(i/4)],marker=self.my_markers[int(i/4)],markerfacecolor='none')
                                else:
                                    plt.plot(self.Tav_radial_para[i][j], self.R_radial_para[i][j],color=self.my_colors[int(i/4)],marker=self.my_markers[int(i/4)],markerfacecolor='none')
            plt.title(s+' radial static temperature', fontsize = 22,**self.csfont)
            plt.grid()
            plt.legend(bbox_to_anchor=(1.01,0.95),borderaxespad=0,prop={'size': 14})
            plt.subplots_adjust(right=0.8)   ##  Need to play with this number.
            plt.ylabel(r'$R$ [m]',fontsize = 18)
            plt.xlabel(r'$\frac{T}{T_{T_{in}}}$',fontsize = 22)
            plt.xticks(fontsize=14 )
            plt.yticks(fontsize= 14)
            plt.savefig(self.PARAVIEW_plot_folder+"\\radial_"+str(s)+"_T.pdf")
            plt.close()
            # plt.show()





        # # Plot reference CFX results and +-5%, +-10%bounds
        for s in self.station:
            plt.figure(figsize=(10,8))
            for f in range(len(self.CFX_radial_files)):
                if self.CFX_radial_files[f].split("_")[3]==s:
                    plt.plot(self.alphaav_radial_CFX[f], self.R_radial_CFX[f],   '--g',label = 'CFX')
                    plt.plot(self.alphaav_radial_CFX_plus5[f], self.R_radial_CFX[f],'--y',  label = 'CFX +-5%')
                    plt.plot(self.alphaav_radial_CFX_min5[f],self.R_radial_CFX[f],'--y')
                    plt.plot(self.alphaav_radial_CFX_plus10[f],self.R_radial_CFX[f],'-.k',  label = 'CFX +-10%')
                    plt.plot(self.alphaav_radial_CFX_min10[f],self.R_radial_CFX[f],'-.k')
        
        # # Plot SU2 BFM results
        # Plot SU2 BFM results
            for i in range(len(self.PARAVIEW_radial_files)):
                #  print(self.PARAVIEW_radial_files[i].split("_")[3])
                 if self.PARAVIEW_radial_files[i].split("_")[3]==s:
                     plt.plot(self.alphaav_radial_para[i],self.R_radial_para[i],color=self.my_colors[int(i/4)])
                     for j in range(len(self.R_radial_para[i])):
                        if j%5==0:
                            if 'THOLLET' in self.PARAVIEW_radial_files[i]:
                                if j==0:
                                    plt.plot(self.alphaav_radial_para[i][j],self.R_radial_para[i][j],label = self.PARAVIEW_radial_legend[i],color=self.my_colors[int(i/4)],marker=self.my_markers[int(i/4)])
                                else:
                                    plt.plot(self.alphaav_radial_para[i][j], self.R_radial_para[i][j],color=self.my_colors[int(i/4)],marker=self.my_markers[int(i/4)])
                            else:
                                if j==0:
                                    plt.plot(self.alphaav_radial_para[i][j],self.R_radial_para[i][j],label = self.PARAVIEW_radial_legend[i],color=self.my_colors[int(i/4)],marker=self.my_markers[int(i/4)],markerfacecolor='none')
                                else:
                                    plt.plot(self.alphaav_radial_para[i][j], self.R_radial_para[i][j],color=self.my_colors[int(i/4)],marker=self.my_markers[int(i/4)],markerfacecolor='none')
            plt.title(s+' radial flow angle', fontsize = 22,**self.csfont)
            plt.grid()
            plt.legend(bbox_to_anchor=(1.01,0.95),borderaxespad=0,prop={'size': 14})
            plt.subplots_adjust(right=0.8)   ##  Need to play with this number.
            plt.ylabel(r'$R$ [m]',fontsize = 18)
            plt.xlabel(r'$\alpha$ [$deg$]',fontsize = 18)
            plt.xticks(fontsize=14 )
            plt.yticks(fontsize= 14)
            plt.savefig(self.PARAVIEW_plot_folder+"\\radial_"+str(s)+"_alpha.pdf")
            plt.close()
            # plt.show()



        

        #  # Plot reference CFX results and +-5%, +-10%bounds
        for s in self.station:
            plt.figure(figsize=(10,8))
            for f in range(len(self.CFX_radial_files)):
                if self.CFX_radial_files[f].split("_")[3]==s:
                    plt.plot(self.Mach_radial_CFX[f], self.R_radial_CFX[f],   '--g',label = 'CFX')
                    plt.plot(self.Mach_radial_CFX_plus5[f], self.R_radial_CFX[f],'--y',  label = 'CFX +-5%')
                    plt.plot(self.Mach_radial_CFX_min5[f],self.R_radial_CFX[f],'--y')
                    plt.plot(self.Mach_radial_CFX_plus10[f],self.R_radial_CFX[f],'-.k',  label = 'CFX +-10%')
                    plt.plot(self.Mach_radial_CFX_min10[f],self.R_radial_CFX[f],'-.k')
    
        # # Plot SU2 BFM results
            for i in range(len(self.PARAVIEW_radial_files)):
                #  print(self.PARAVIEW_radial_files[i].split("_")[3])
                 if self.PARAVIEW_radial_files[i].split("_")[3]==s:
                     plt.plot(self.Mach_radial_para[i],self.R_radial_para[i],color=self.my_colors[int(i/4)])
                     for j in range(len(self.R_radial_para[i])):
                        if j%5==0:
                            if 'THOLLET' in self.PARAVIEW_radial_files[i]:
                                if j==0:
                                    plt.plot(self.Mach_radial_para[i][j],self.R_radial_para[i][j],label = self.PARAVIEW_radial_legend[i],color=self.my_colors[int(i/4)],marker=self.my_markers[int(i/4)])
                                else:
                                    plt.plot(self.Mach_radial_para[i][j], self.R_radial_para[i][j],color=self.my_colors[int(i/4)],marker=self.my_markers[int(i/4)])
                            else:
                                if j==0:
                                    plt.plot(self.Mach_radial_para[i][j],self.R_radial_para[i][j],label = self.PARAVIEW_radial_legend[i],color=self.my_colors[int(i/4)],marker=self.my_markers[int(i/4)],markerfacecolor='none')
                                else:
                                    plt.plot(self.Mach_radial_para[i][j], self.R_radial_para[i][j],color=self.my_colors[int(i/4)],marker=self.my_markers[int(i/4)],markerfacecolor='none')
            plt.title(s+' radial Mach', fontsize = 22,**self.csfont)
            plt.grid()
            plt.legend(bbox_to_anchor=(1.01,0.95),borderaxespad=0,prop={'size': 14})
            plt.subplots_adjust(right=0.8)   ##  Need to play with this number.
            plt.ylabel(r'$R$ [m]',fontsize = 18)
            plt.xlabel(r'$Mach$',fontsize = 18)
            plt.xticks(fontsize=14 )
            plt.yticks(fontsize= 14)
            plt.savefig(self.PARAVIEW_plot_folder+"\\radial_"+str(s)+"_Mach.pdf")
            plt.close()
            # plt.show()





             # Plot reference CFX results and +-5%, +-10%bounds
        for s in self.station:
            plt.figure(figsize=(10,8))
            for f in range(len(self.CFX_radial_files)):
                if self.CFX_radial_files[f].split("_")[3]==s:
                    
                    plt.plot( self.Ptotav_radial_CFX[f],self.R_radial_CFX[f], '--g',  label = 'CFX')
                    plt.plot(self.Ptotav_radial_CFX_plus5[f],self.R_radial_CFX[f],'--y',  label = 'CFX +-5%')
                    plt.plot(self.Ptotav_radial_CFX_min5[f],self.R_radial_CFX[f],'--y')
                    plt.plot(self.Ptotav_radial_CFX_plus10[f],self.R_radial_CFX[f],'-.k',  label = 'CFX +-10%')
                    plt.plot(self.Ptotav_radial_CFX_min10[f],self.R_radial_CFX[f],'-.k')
        
            # Plot SU2 BFM results
            for i in range(len(self.PARAVIEW_radial_files)):
                #  print(self.PARAVIEW_radial_files[i].split("_")[3])
                 if self.PARAVIEW_radial_files[i].split("_")[3]==s:
                     plt.plot(self.Ptotav_radial_para[i],self.R_radial_para[i],color=self.my_colors[int(i/4)])
                     for j in range(len(self.R_radial_para[i])):
                        if j%5==0:
                            if 'THOLLET' in self.PARAVIEW_radial_files[i]:
                                if j==0:
                                    plt.plot(self.Ptotav_radial_para[i][j],self.R_radial_para[i][j],label = self.PARAVIEW_radial_legend[i],color=self.my_colors[int(i/4)],marker=self.my_markers[int(i/4)])
                                else:
                                    plt.plot(self.Ptotav_radial_para[i][j], self.R_radial_para[i][j],color=self.my_colors[int(i/4)],marker=self.my_markers[int(i/4)])
                            else:
                                if j==0:
                                    plt.plot(self.Ptotav_radial_para[i][j],self.R_radial_para[i][j],label = self.PARAVIEW_radial_legend[i],color=self.my_colors[int(i/4)],marker=self.my_markers[int(i/4)],markerfacecolor='none')
                                else:
                                    plt.plot(self.Ptotav_radial_para[i][j], self.R_radial_para[i][j],color=self.my_colors[int(i/4)],marker=self.my_markers[int(i/4)],markerfacecolor='none')
            plt.title(s+' radial total pressure', fontsize = 22,**self.csfont)
            plt.grid()
            plt.legend(bbox_to_anchor=(1.01,0.95),borderaxespad=0,prop={'size': 14})
            plt.subplots_adjust(right=0.8)   ##  Need to play with this number.
            plt.ylabel(r'$R$ [m]',fontsize = 18)
            plt.xlabel(r'$\frac{P_T}{P_{T_{in}}}$',fontsize = 22)
            plt.xticks(fontsize=14 )
            plt.yticks(fontsize= 14)
            plt.savefig(self.PARAVIEW_plot_folder+"\\radial_"+str(s)+"_Ptot.pdf")
            plt.close()
            # plt.show()



        # Plot reference CFX results and +-5%, +-10%bounds
        for s in self.station:
            plt.figure(figsize=(10,8))
            for f in range(len(self.CFX_radial_files)):
                if self.CFX_radial_files[f].split("_")[3]==s:
                    plt.plot(self.Ttotav_radial_CFX[f], self.R_radial_CFX[f],   '--g',label = 'CFX')
                    plt.plot(self.Ttotav_radial_CFX_plus5[f], self.R_radial_CFX[f],'--y',  label = 'CFX +-5%')
                    plt.plot(self.Ttotav_radial_CFX_min5[f],self.R_radial_CFX[f],'--y')
                    plt.plot(self.Ttotav_radial_CFX_plus10[f],self.R_radial_CFX[f],'-.k',  label = 'CFX +-10%')
                    plt.plot(self.Ttotav_radial_CFX_min10[f],self.R_radial_CFX[f],'-.k')
        
        # Plot SU2 BFM results
            for i in range(len(self.PARAVIEW_radial_files)):
                #  print(self.PARAVIEW_radial_files[i].split("_")[3])
                 if self.PARAVIEW_radial_files[i].split("_")[3]==s:
                     plt.plot(self.Ttotav_radial_para[i],self.R_radial_para[i],color=self.my_colors[int(i/4)])
                     for j in range(len(self.R_radial_para[i])):
                        if j%5==0:
                            if 'THOLLET' in self.PARAVIEW_radial_files[i]:
                                if j==0:
                                    plt.plot(self.Ttotav_radial_para[i][j],self.R_radial_para[i][j],label = self.PARAVIEW_radial_legend[i],color=self.my_colors[int(i/4)],marker=self.my_markers[int(i/4)])
                                else:
                                    plt.plot(self.Ttotav_radial_para[i][j], self.R_radial_para[i][j],color=self.my_colors[int(i/4)],marker=self.my_markers[int(i/4)])
                            else:
                                if j==0:
                                    plt.plot(self.Ttotav_radial_para[i][j],self.R_radial_para[i][j],label = self.PARAVIEW_radial_legend[i],color=self.my_colors[int(i/4)],marker=self.my_markers[int(i/4)],markerfacecolor='none')
                                else:
                                    plt.plot(self.Ttotav_radial_para[i][j], self.R_radial_para[i][j],color=self.my_colors[int(i/4)],marker=self.my_markers[int(i/4)],markerfacecolor='none')
            plt.title(s+' radial total temperature', fontsize = 22,**self.csfont)
            plt.grid()
            plt.legend(bbox_to_anchor=(1.01,0.95),borderaxespad=0,prop={'size': 14})
            plt.subplots_adjust(right=0.8)   ##  Need to play with this number.
            plt.ylabel(r'$R$ [m]',fontsize = 18)
            plt.xlabel(r'$\frac{T_T}{T_{T_{in}}}$',fontsize = 22)
            plt.xticks(fontsize=14 )
            plt.yticks(fontsize= 14)
            plt.savefig(self.PARAVIEW_plot_folder+"\\radial_"+str(s)+"_Ttot.pdf")
            plt.close()
            # plt.show()














    
    def plot_axial_error(self):


        plt.figure(figsize=(10,8))
        for i in range(len(self.PARAVIEW_axial_files)):
            err=[]
            for j in range(len(self.Pav_axial_para[i])):
                err.append(((self.Pav_axial_para[i][j]-self.Pav_axial_CFX[j])*100/self.Pav_axial_CFX[j]))
            plt.plot(self.X_axial_para[i], err,label = self.PARAVIEW_axial_legend[i],color=self.my_colors[i])
        plt.title('Err static pressure', fontsize = 22,**self.csfont)
        plt.grid()
        plt.legend(bbox_to_anchor=(1.25,0.95),borderaxespad=0)
        plt.subplots_adjust(right=0.8)   ##  Need to play with this number.
        plt.xlabel(r'$X$ [m]',fontsize = 18)
        plt.ylabel(r'$\epsilon_P$ [%]',fontsize = 18)
        plt.savefig(self.PARAVIEW_plot_folder+"\\err_P.pdf")
        plt.close()
        # plt.show()


        plt.figure(figsize=(10,8))
        for i in range(len(self.PARAVIEW_axial_files)):
            err=[]
            for j in range(len(self.Ptotav_axial_para[i])):
                err.append(((self.Ptotav_axial_para[i][j]-self.Ptotav_axial_CFX[j])*100/self.Ptotav_axial_CFX[j]))
            plt.plot(self.X_axial_para[i], err,label = self.PARAVIEW_axial_legend[i],color=self.my_colors[i])
        plt.title('Err total pressure', fontsize = 22,**self.csfont)
        plt.grid()
        plt.legend(bbox_to_anchor=(1.25,0.95),borderaxespad=0)
        plt.subplots_adjust(right=0.8)   ##  Need to play with this number.
        plt.xlabel(r'$X$ [m]',fontsize = 18)
        plt.ylabel(r'$\epsilon_{P_{T}}$ [%]',fontsize = 18)
        plt.savefig(self.PARAVIEW_plot_folder+"\\err_Pt.pdf")
        plt.close()
        # plt.show()


        plt.figure(figsize=(10,8))
        for i in range(len(self.PARAVIEW_axial_files)):
            err=[]
            for j in range(len(self.Tav_axial_para[i])):
                err.append(((self.Tav_axial_para[i][j]-self.Tav_axial_CFX[j])*100/self.Tav_axial_CFX[j]))
            plt.plot(self.X_axial_para[i], err,label = self.PARAVIEW_axial_legend[i],color=self.my_colors[i])
        plt.title('Err static temperature', fontsize = 22,**self.csfont)
        plt.grid()
        plt.legend(bbox_to_anchor=(1.25,0.95),borderaxespad=0)
        plt.subplots_adjust(right=0.8)   ##  Need to play with this number.
        plt.xlabel(r'$X$ [m]',fontsize = 18)
        plt.ylabel(r'$\epsilon_T$ [%]',fontsize = 18)
        plt.savefig(self.PARAVIEW_plot_folder+"\\err_T.pdf")
        plt.close()
        # plt.show()


        plt.figure(figsize=(10,8))
        for i in range(len(self.PARAVIEW_axial_files)):
            err=[]
            for j in range(len(self.Ttotav_axial_para[i])):
                err.append(((self.Ttotav_axial_para[i][j]-self.Ttotav_axial_CFX[j])*100/self.Ttotav_axial_CFX[j]))
            plt.plot(self.X_axial_para[i], err,label = self.PARAVIEW_axial_legend[i],color=self.my_colors[i])
        plt.title('Err total temperature', fontsize = 22,**self.csfont)
        plt.grid()
        plt.legend(bbox_to_anchor=(1.25,0.95),borderaxespad=0)
        plt.subplots_adjust(right=0.8)   ##  Need to play with this number.
        plt.xlabel(r'$X$ [m]',fontsize = 18)
        plt.ylabel(r'$\epsilon_{T_{T}}$ [%]',fontsize = 18)
        plt.savefig(self.PARAVIEW_plot_folder+"\\err_Tt.pdf")
        plt.close()
        # plt.show()

        plt.figure(figsize=(10,8))
        for i in range(len(self.PARAVIEW_axial_files)):
            err=[]
            for j in range(len(self.alphaav_axial_para[i])):
                err.append(((self.alphaav_axial_para[i][j]-self.alphaav_axial_CFX[j])))
            plt.plot(self.X_axial_para[i], err,label = self.PARAVIEW_axial_legend[i],color=self.my_colors[i])
        plt.title('Err alpha', fontsize = 22,**self.csfont)
        plt.grid()
        plt.legend(bbox_to_anchor=(1.25,0.95),borderaxespad=0)
        plt.subplots_adjust(right=0.8)   ##  Need to play with this number.
        plt.xlabel(r'$X$ [m]',fontsize = 18)
        plt.ylabel(r'$\alpha$ [deg]',fontsize = 18)
        plt.savefig(self.PARAVIEW_plot_folder+"\\err_alpha.pdf")
        plt.close()
        # plt.show()

        plt.figure(figsize=(10,8))
        for i in range(len(self.PARAVIEW_axial_files)):
            err=[]
            for j in range(len(self.mfluxav_axial_para[i])):
                err.append(((self.mfluxav_axial_para[i][j]-self.mfluxav_axial_CFX[j])*100/self.mfluxav_axial_CFX[j]))
            plt.plot(self.X_axial_para[i], err,label = self.PARAVIEW_axial_legend[i],color=self.my_colors[i])
        plt.title('Err mass flux', fontsize = 22,**self.csfont)
        plt.grid()
        plt.legend(bbox_to_anchor=(1.25,0.95),borderaxespad=0)
        plt.subplots_adjust(right=0.8)   ##  Need to play with this number.
        plt.xlabel(r'$X$ [m]',fontsize = 18)
        plt.ylabel(r'$\epsilon_{rho U}$ [%]',fontsize = 18)
        plt.savefig(self.PARAVIEW_plot_folder+"\\err_mdot.pdf")
        plt.close()
        # plt.show()
        











    def plot_axial_results(self):
        
        # Plot reference CFX results and +-5%, +-10%bounds
        plt.figure(figsize=(10,8))
        plt.plot(self.Z_axial_CFX, self.Pav_axial_CFX, '--g',  label = 'CFX')
        plt.plot(self.Z_axial_CFX, self.Pav_axial_CFX_plus5,'--y',  label = 'CFX +-5%')
        plt.plot(self.Z_axial_CFX, self.Pav_axial_CFX_min5,'--y')
        plt.plot(self.Z_axial_CFX, self.Pav_axial_CFX_plus10,'-.k',  label = 'CFX +-10%')
        plt.plot(self.Z_axial_CFX, self.Pav_axial_CFX_min10,'-.k')
        
        # Plot SU2 BFM results
        for i in range(len(self.PARAVIEW_axial_files)):
            plt.plot(self.X_axial_para[i], self.Pav_axial_para[i],color=self.my_colors[i])
            for j in range(len(self.X_axial_para[i])):
                if j%5==0:
                    if 'THOLLET' in self.PARAVIEW_axial_files[i]:
                        if j==0:
                            plt.plot(self.X_axial_para[i][j],self.Pav_axial_para[i][j],label = self.PARAVIEW_axial_legend[i],color=self.my_colors[i],marker=self.my_markers[i])
                        else:
                            plt.plot(self.X_axial_para[i][j], self.Pav_axial_para[i][j],color=self.my_colors[i],marker=self.my_markers[i])
                    else:
                        if j==0:
                            plt.plot(self.X_axial_para[i][j],self.Pav_axial_para[i][j],label = self.PARAVIEW_axial_legend[i],color=self.my_colors[i],marker=self.my_markers[i],markerfacecolor='none')
                        else:
                            plt.plot(self.X_axial_para[i][j], self.Pav_axial_para[i][j],color=self.my_colors[i],marker=self.my_markers[i],markerfacecolor='none')
        plt.title('Mass flow averaged static pressure', fontsize = 22)
        plt.grid()

        plt.xlabel(r'$X$ [m]',fontsize = 18,**self.csfont)
        plt.ylabel(r'$\frac{P}{P_{T_{in}}}$',fontsize = 22,**self.csfont)
        plt.xticks(fontsize=14 )
        plt.yticks(fontsize= 14)
        plt.legend(bbox_to_anchor=(1.01,0.95),borderaxespad=0,prop={'size': 14})
        plt.subplots_adjust(right=0.8)   ##  Need to play with this number.
        
        plt.savefig(self.PARAVIEW_plot_folder+"\\P.pdf")
        plt.close()
        #plt.show()



        
        # Plot reference CFX results and +-5%, +-10%bounds
        plt.figure(figsize=(10,8))
        plt.plot(self.Z_axial_CFX, self.Ptotav_axial_CFX, '--g',label = 'CFX')
        plt.plot(self.Z_axial_CFX, self.Ptotav_axial_CFX_plus5,'--y',  label = 'CFX +-5%')
        plt.plot(self.Z_axial_CFX, self.Ptotav_axial_CFX_min5,'--y')
        plt.plot(self.Z_axial_CFX, self.Ptotav_axial_CFX_plus10,'-.k',  label = 'CFX +-10%')
        plt.plot(self.Z_axial_CFX, self.Ptotav_axial_CFX_min10,'-.k')
        
        # Plot SU2 BFM results
        for i in range(len(self.PARAVIEW_axial_files)):
            plt.plot(self.X_axial_para[i], self.Ptotav_axial_para[i],color=self.my_colors[i])
            for j in range(len(self.X_axial_para[i])):
                if j%5==0:
                    if 'THOLLET' in self.PARAVIEW_axial_files[i]:
                        if j==0:
                            plt.plot(self.X_axial_para[i][j],self.Ptotav_axial_para[i][j],label = self.PARAVIEW_axial_legend[i],color=self.my_colors[i],marker=self.my_markers[i])
                        else:
                            plt.plot(self.X_axial_para[i][j], self.Ptotav_axial_para[i][j],color=self.my_colors[i],marker=self.my_markers[i])
                    else:
                        if j==0:
                            plt.plot(self.X_axial_para[i][j],self.Ptotav_axial_para[i][j],label = self.PARAVIEW_axial_legend[i],color=self.my_colors[i],marker=self.my_markers[i],markerfacecolor='none')
                        else:
                            plt.plot(self.X_axial_para[i][j], self.Ptotav_axial_para[i][j],color=self.my_colors[i],marker=self.my_markers[i],markerfacecolor='none')
        plt.title('Mass flow averaged total pressure', fontsize = 22,**self.csfont)
        plt.grid()
        plt.legend(bbox_to_anchor=(1.01,0.95),borderaxespad=0,prop={'size': 14})
        plt.subplots_adjust(right=0.8)   ##  Need to play with this number.
        plt.xlabel(r'$X$ [m]',fontsize = 18)
        plt.ylabel(r'$\frac{P_{T}}{P_{T_{in}}}$',fontsize = 22)
        plt.xticks(fontsize=14 )
        plt.yticks(fontsize= 14)
        plt.savefig(self.PARAVIEW_plot_folder+"\\Ptot.pdf")
        plt.close()
        # plt.show()




        # Plot reference CFX results and +-5%, +-10%bounds
        plt.figure(figsize=(10,8))
        plt.plot(self.Z_axial_CFX, self.Tav_axial_CFX,  '--g',label = 'CFX')
        plt.plot(self.Z_axial_CFX, self.Tav_axial_CFX_plus5,'--y',  label = 'CFX +-5%')
        plt.plot(self.Z_axial_CFX, self.Tav_axial_CFX_min5,'--y')
        plt.plot(self.Z_axial_CFX, self.Tav_axial_CFX_plus10,'-.k',  label = 'CFX +-10%')
        plt.plot(self.Z_axial_CFX, self.Tav_axial_CFX_min10,'-.k')
        
        # Plot SU2 BFM results
        for i in range(len(self.PARAVIEW_axial_files)):
            plt.plot(self.X_axial_para[i], self.Tav_axial_para[i],color=self.my_colors[i])
            for j in range(len(self.X_axial_para[i])):
                if j%5==0:
                    if 'THOLLET' in self.PARAVIEW_axial_files[i]:
                        if j==0:
                            plt.plot(self.X_axial_para[i][j],self.Tav_axial_para[i][j],label = self.PARAVIEW_axial_legend[i],color=self.my_colors[i],marker=self.my_markers[i])
                        else:
                            plt.plot(self.X_axial_para[i][j], self.Tav_axial_para[i][j],color=self.my_colors[i],marker=self.my_markers[i])
                    else:
                        if j==0:
                            plt.plot(self.X_axial_para[i][j],self.Tav_axial_para[i][j],label = self.PARAVIEW_axial_legend[i],color=self.my_colors[i],marker=self.my_markers[i],markerfacecolor='none')
                        else:
                            plt.plot(self.X_axial_para[i][j], self.Tav_axial_para[i][j],color=self.my_colors[i],marker=self.my_markers[i],markerfacecolor='none')
        plt.title('Mass flow averaged static temperature', fontsize = 22,**self.csfont)
        plt.grid()
        plt.legend(bbox_to_anchor=(1.01,0.95),borderaxespad=0,prop={'size': 14})
        plt.subplots_adjust(right=0.8)   ##  Need to play with this number.
        plt.xlabel(r'$X$ [m]',fontsize = 18)
        plt.ylabel(r'$\frac{T}{T_{T_{in}}}$',fontsize = 22)
        plt.xticks(fontsize=14 )
        plt.yticks(fontsize= 14)
        plt.savefig(self.PARAVIEW_plot_folder+"\\T.pdf")
        plt.close()
        # plt.show()




        # Plot reference CFX results and +-5%, +-10%bounds
        plt.figure(figsize=(10,8))
        plt.plot(self.Z_axial_CFX, self.Ttotav_axial_CFX,  '--g',label = 'CFX')
        plt.plot(self.Z_axial_CFX, self.Ttotav_axial_CFX_plus5,'--y',  label = 'CFX +-5%')
        plt.plot(self.Z_axial_CFX, self.Ttotav_axial_CFX_min5,'--y')
        plt.plot(self.Z_axial_CFX, self.Ttotav_axial_CFX_plus10,'-.k',  label = 'CFX +-10%')
        plt.plot(self.Z_axial_CFX, self.Ttotav_axial_CFX_min10,'-.k')
        
        # Plot SU2 BFM results
        for i in range(len(self.PARAVIEW_axial_files)):
            plt.plot(self.X_axial_para[i], self.Ttotav_axial_para[i],color=self.my_colors[i])
            for j in range(len(self.X_axial_para[i])):
                if j%5==0:
                    if 'THOLLET' in self.PARAVIEW_axial_files[i]:
                        if j==0:
                            plt.plot(self.X_axial_para[i][j],self.Ttotav_axial_para[i][j],label = self.PARAVIEW_axial_legend[i],color=self.my_colors[i],marker=self.my_markers[i])
                        else:
                            plt.plot(self.X_axial_para[i][j], self.Ttotav_axial_para[i][j],color=self.my_colors[i],marker=self.my_markers[i])
                    else:
                        if j==0:
                            plt.plot(self.X_axial_para[i][j],self.Ttotav_axial_para[i][j],label = self.PARAVIEW_axial_legend[i],color=self.my_colors[i],marker=self.my_markers[i],markerfacecolor='none')
                        else:
                            plt.plot(self.X_axial_para[i][j], self.Ttotav_axial_para[i][j],color=self.my_colors[i],marker=self.my_markers[i],markerfacecolor='none')
        plt.title('Mass flow averaged total temperature', fontsize = 22,**self.csfont)
        plt.grid()
        plt.legend(bbox_to_anchor=(1.01,0.95),borderaxespad=0,prop={'size': 14})
        plt.subplots_adjust(right=0.8)   ##  Need to play with this number.
        plt.xlabel(r'$X$ [m]',fontsize = 18)
        plt.ylabel(r'$\frac{T_{T}}{T_{T_{in}}}$',fontsize = 22)
        plt.xticks(fontsize=14 )
        plt.yticks(fontsize= 14)
        plt.savefig(self.PARAVIEW_plot_folder+"\\Ttot.pdf")
        plt.close()
        # plt.show()





        # Plot reference CFX results and +-5%, +-10%bounds
        plt.figure(figsize=(10,8))
        plt.plot(self.Z_axial_CFX, self.alphaav_axial_CFX,  '--g',label = 'CFX')
        plt.plot(self.Z_axial_CFX, self.alphaav_axial_CFX_plus5,'--y',  label = 'CFX +-5%')
        plt.plot(self.Z_axial_CFX, self.alphaav_axial_CFX_min5,'--y')
        plt.plot(self.Z_axial_CFX, self.alphaav_axial_CFX_plus10,'-.k',  label = 'CFX +-10%')
        plt.plot(self.Z_axial_CFX, self.alphaav_axial_CFX_min10,'-.k')
        
        # Plot SU2 BFM results
        for i in range(len(self.PARAVIEW_axial_files)):
            plt.plot(self.X_axial_para[i], self.alphaav_axial_para[i],color=self.my_colors[i])
            # print('la lunghezza è ',len(self.X_axial_para[i]))
            for j in range(len(self.X_axial_para[i])):
                if j%5==0:
                    if 'THOLLET' in self.PARAVIEW_axial_files[i]:
                        if j==0:
                            plt.plot(self.X_axial_para[i][j],self.alphaav_axial_para[i][j],label = self.PARAVIEW_axial_legend[i],color=self.my_colors[i],marker=self.my_markers[i])
                        else:
                            plt.plot(self.X_axial_para[i][j], self.alphaav_axial_para[i][j],color=self.my_colors[i],marker=self.my_markers[i])
                    else:
                        if j==0:
                            plt.plot(self.X_axial_para[i][j],self.alphaav_axial_para[i][j],label = self.PARAVIEW_axial_legend[i],color=self.my_colors[i],marker=self.my_markers[i],markerfacecolor='none')
                        else:
                            plt.plot(self.X_axial_para[i][j], self.alphaav_axial_para[i][j],color=self.my_colors[i],marker=self.my_markers[i],markerfacecolor='none')
        plt.title('Mass flow averaged absolute angle', fontsize = 22,**self.csfont)
        plt.grid()
        # plt.legend(bbox_to_anchor=(1.25,0.95),borderaxespad=0)
        plt.legend(bbox_to_anchor=(1.01,0.95),borderaxespad=0,prop={'size': 14})
        # plt.subplots_adjust(right=0.8)   ##  Need to play with this number.
        plt.subplots_adjust(right=0.8)
        plt.xlabel(r'$X$ [m]',fontsize = 18)
        plt.ylabel(r'$\alpha$ [deg]',fontsize = 18)
        plt.xticks(fontsize=14 )
        plt.yticks(fontsize= 14)
        # plt.show()
        plt.savefig(self.PARAVIEW_plot_folder+"\\alpha.pdf")
        plt.close()
        # plt.show()




        # Plot reference CFX results and +-5%, +-10%bounds
        plt.figure(figsize=(10,8))
        plt.plot(self.Z_axial_CFX, self.mfluxav_axial_CFX,  '--g',label = 'CFX')
        plt.plot(self.Z_axial_CFX, self.mfluxav_axial_CFX_plus5,'--y',  label = 'CFX +-5%')
        plt.plot(self.Z_axial_CFX, self.mfluxav_axial_CFX_min5,'--y')
        plt.plot(self.Z_axial_CFX, self.mfluxav_axial_CFX_plus10,'-.k',  label = 'CFX +-10%')
        plt.plot(self.Z_axial_CFX, self.mfluxav_axial_CFX_min10,'-.k')
        
        # Plot SU2 BFM results
        for i in range(len(self.PARAVIEW_axial_files)):
            plt.plot(self.X_axial_para[i], self.mfluxav_axial_para[i],color=self.my_colors[i])
            for j in range(len(self.X_axial_para[i])):
                if j%5==0:
                    if 'THOLLET' in self.PARAVIEW_axial_files[i]:
                        if j==0:
                            plt.plot(self.X_axial_para[i][j],self.mfluxav_axial_para[i][j],label = self.PARAVIEW_axial_legend[i],color=self.my_colors[i],marker=self.my_markers[i])
                        else:
                            plt.plot(self.X_axial_para[i][j], self.mfluxav_axial_para[i][j],color=self.my_colors[i],marker=self.my_markers[i])
                    else:
                        if j==0:
                            plt.plot(self.X_axial_para[i][j],self.mfluxav_axial_para[i][j],label = self.PARAVIEW_axial_legend[i],color=self.my_colors[i],marker=self.my_markers[i],markerfacecolor='none')
                        else:
                            plt.plot(self.X_axial_para[i][j], self.mfluxav_axial_para[i][j],color=self.my_colors[i],marker=self.my_markers[i],markerfacecolor='none')
        plt.title('Mass flow averaged mass flux', fontsize = 22,**self.csfont)
        plt.grid()
        plt.legend(bbox_to_anchor=(1.01,0.95),borderaxespad=0,prop={'size': 14})
        plt.subplots_adjust(right=0.8)   ##  Need to play with this number.
        plt.xlabel(r'$X$ [m]',fontsize = 18,**self.csfont)
        plt.ylabel(r'$\rho$$U$  [$\frac{Kg}{m^2 s}$]',fontsize = 18,**self.csfont)
        plt.xticks(fontsize=14 )
        plt.yticks(fontsize= 14)
        plt.savefig(self.PARAVIEW_plot_folder+"\\mflux.pdf")
        plt.close()
        # plt.show()

        

         # Plot reference CFX results and +-5%, +-10%bounds
        plt.figure(figsize=(10,8))
        plt.plot(self.Z_axial_CFX, self.Mach_axial_CFX,  '--g',label = 'CFX')
        plt.plot(self.Z_axial_CFX, self.Mach_axial_CFX_plus5,'--y',  label = 'CFX +-5%')
        plt.plot(self.Z_axial_CFX, self.Mach_axial_CFX_min5,'--y')
        plt.plot(self.Z_axial_CFX, self.Mach_axial_CFX_plus10,'-.k',  label = 'CFX +-10%')
        plt.plot(self.Z_axial_CFX, self.Mach_axial_CFX_min10,'-.k')

        # Plot SU2 BFM results
        for i in range(len(self.PARAVIEW_axial_files)):
            plt.plot(self.X_axial_para[i], self.Mach_axial_para[i],color=self.my_colors[i])
            for j in range(len(self.X_axial_para[i])):
                if j%5==0:
                    if 'THOLLET' in self.PARAVIEW_axial_files[i]:
                        if j==0:
                            plt.plot(self.X_axial_para[i][j],self.Mach_axial_para[i][j],label = self.PARAVIEW_axial_legend[i],color=self.my_colors[i],marker=self.my_markers[i])
                        else:
                            plt.plot(self.X_axial_para[i][j], self.Mach_axial_para[i][j],color=self.my_colors[i],marker=self.my_markers[i])
                    else:
                        if j==0:
                            plt.plot(self.X_axial_para[i][j],self.Mach_axial_para[i][j],label = self.PARAVIEW_axial_legend[i],color=self.my_colors[i],marker=self.my_markers[i],markerfacecolor='none')
                        else:
                            plt.plot(self.X_axial_para[i][j], self.Mach_axial_para[i][j],color=self.my_colors[i],marker=self.my_markers[i],markerfacecolor='none')
        plt.title('Mass flow averaged Mach number', fontsize = 22,**self.csfont)
        plt.grid()
        plt.legend(bbox_to_anchor=(1.01,0.95),borderaxespad=0,prop={'size': 14})
        plt.subplots_adjust(right=0.8)   ##  Need to play with this number.
        plt.xlabel(r'$X$ [m]',fontsize = 18)
        plt.ylabel(r'$Mach$',fontsize = 18)
        plt.xticks(fontsize=14 )
        plt.yticks(fontsize= 14)
        plt.savefig(self.PARAVIEW_plot_folder+"\\Mach.pdf")
        plt.close()
        # plt.show()



         # Plot reference CFX results and +-5%, +-10%bounds
        plt.figure(figsize=(10,8))
        plt.plot(self.Z_axial_CFX, self.dS_axial_CFX,  '--g',label = 'CFX')
        plt.plot(self.Z_axial_CFX, self.dS_axial_CFX_plus5,'--y',  label = 'CFX +-5%')
        plt.plot(self.Z_axial_CFX, self.dS_axial_CFX_min5,'--y')
        plt.plot(self.Z_axial_CFX, self.dS_axial_CFX_plus10,'-.k',  label = 'CFX +-10%')
        plt.plot(self.Z_axial_CFX, self.dS_axial_CFX_min10,'-.k')

        # # Plot SU2 BFM results
        for i in range(len(self.PARAVIEW_axial_files)):
            plt.plot(self.X_axial_para[i], self.dS_axial_para[i],color=self.my_colors[i])
            for j in range(len(self.X_axial_para[i])):
                if j%5==0:
                    if 'THOLLET' in self.PARAVIEW_axial_files[i]:
                        if j==0:
                            plt.plot(self.X_axial_para[i][j],self.dS_axial_para[i][j],label = self.PARAVIEW_axial_legend[i],color=self.my_colors[i],marker=self.my_markers[i])
                        else:
                            plt.plot(self.X_axial_para[i][j], self.dS_axial_para[i][j],color=self.my_colors[i],marker=self.my_markers[i])
                    else:
                        if j==0:
                            plt.plot(self.X_axial_para[i][j],self.dS_axial_para[i][j],label = self.PARAVIEW_axial_legend[i],color=self.my_colors[i],marker=self.my_markers[i],markerfacecolor='none')
                        else:
                            plt.plot(self.X_axial_para[i][j], self.dS_axial_para[i][j],color=self.my_colors[i],marker=self.my_markers[i],markerfacecolor='none')
        plt.title('Mass flow averaged entropy rise', fontsize = 22,**self.csfont)
        plt.grid()
        plt.legend(bbox_to_anchor=(1.01,0.95),borderaxespad=0,prop={'size': 14})
        plt.subplots_adjust(right=0.8)   ##  Need to play with this number.
        plt.xlabel(r'$X$ [m]',fontsize = 18)
        plt.ylabel(r'$Mach$',fontsize = 18)
        plt.xticks(fontsize=14 )
        plt.yticks(fontsize= 14)
        plt.savefig(self.PARAVIEW_plot_folder+"\\dS.pdf")
        plt.close()
        # # plt.show()


               # Plot reference CFX results and +-5%, +-10%bounds
        plt.figure(figsize=(10,8))
        plt.plot(self.Z_axial_CFX, self.mdot_axial_CFX, '--g',label = r'$CFX$')
        
        
        # Plot SU2 BFM results
        #for i in range(len(self.PARAVIEW_axial_files)):
            #if i == 0:
                #plt.plot(self.X_axial_para[i], self.mdot_axial_para[i],label = r'$HALL_{FA}$',color='m')
            #elif i == 1:
                #plt.plot(self.X_axial_para[i], self.mdot_axial_para[i],label = r'$HALL_{PER}$',color='c')
            #elif i == 2:
                #plt.plot(self.X_axial_para[i], self.mdot_axial_para[i],label = r'$THOLLET_{FA}$',color='r')
            #else:
                #plt.plot(self.X_axial_para[i], self.mdot_axial_para[i],label = r'$THOLLET_{PER}$',color='b')
        for i in range(len(self.PARAVIEW_axial_files)):
            plt.plot(self.X_axial_para[i], self.mdot_axial_para[i],color=self.my_colors[i])
            for j in range(len(self.X_axial_para[i])):
                if j%5==0:
                    if 'THOLLET' in self.PARAVIEW_axial_files[i]:
                        if j==0:
                            plt.plot(self.X_axial_para[i][j],self.mdot_axial_para[i][j],label = self.PARAVIEW_axial_legend[i],color=self.my_colors[i],marker=self.my_markers[i])
                        else:
                            plt.plot(self.X_axial_para[i][j], self.mdot_axial_para[i][j],color=self.my_colors[i],marker=self.my_markers[i])
                    else:
                        if j==0:
                            plt.plot(self.X_axial_para[i][j],self.mdot_axial_para[i][j],label = self.PARAVIEW_axial_legend[i],color=self.my_colors[i],marker=self.my_markers[i],markerfacecolor='none')
                        else:
                            plt.plot(self.X_axial_para[i][j], self.mdot_axial_para[i][j],color=self.my_colors[i],marker=self.my_markers[i],markerfacecolor='none')
        # plt.plot(self.X_para, self.mfluxav_para,  'r',label = 'BFM')
        plt.title('Mass flow', fontsize = 22,**self.csfont)
        plt.grid()
        plt.legend(bbox_to_anchor=(1.28,0.95),borderaxespad=0,prop={'size': 14})
        plt.subplots_adjust(right=0.8)   ##  Need to play with this number.
        plt.xlabel(r'$X$ [m]',fontsize = 18,**self.csfont)
        plt.ylabel(r'$\dot{m}$ [$\frac{Kg}{s}$]',fontsize = 18,**self.csfont)
        plt.xticks(fontsize=14 )
        plt.yticks(fontsize= 14)
        plt.savefig(self.PARAVIEW_plot_folder+"\\mdot_comparison.pdf")
        plt.close()
        #plt.show()

        

    

    def get_paraview_radial_results(self,file):

        R = []
        Pav = []
        Tav = []
        alphaav = []
        Mach = []
        rows = []

        Ttotav = []
        Ptotav = []

        
        with open(file,'r') as csvfile:
            plots = csv.reader(csvfile, delimiter = ',')
            for row in plots:
                rows.append(row)

        for i in range (1,len(rows)):
            if  rows[i][1] != 'nan':
                R.append(float(rows[i][0]))

                # Pav.append(float(rows[i][2])/float(rows[1][5]))
                Pav.append(float(rows[i][2])/self.Ptotav_IN_para[-1])
                self.Ptotav_axial_para[0]
                # Tav.append(float(rows[i][3])/float(rows[1][6]))
                Tav.append(float(rows[i][3])/self.Ttotav_IN_para[-1])
                alphaav.append(-float(rows[i][7]))
                Mach.append(float(rows[i][8]))

                Ptotav.append(float(rows[i][5])/self.Ptotav_IN_para[-1])
                Ttotav.append(float(rows[i][6])/self.Ttotav_IN_para[-1])

            

        self.R_radial_para.append(R) 
        self.Pav_radial_para.append(Pav)
        self.Tav_radial_para.append(Tav)
        self.alphaav_radial_para.append(alphaav)
        self.Mach_radial_para.append(Mach)
        self.Ptotav_radial_para.append(Ptotav)
        self.Ttotav_radial_para.append(Ttotav)



    def get_CFX_radial_results(self,file):
    
        R = []
       

        Pav = []
        Pav_plus5 = []
        Pav_min5 = []
        Pav_plus10 = []
        Pav_min10 = []

        Tav = []
        Tav_plus5 = []
        Tav_min5 = []
        Tav_plus10 = []
        Tav_min10 = []

        alphaav = []
        alphaav_plus5 = []
        alphaav_min5 = []
        alphaav_plus10 = []
        alphaav_min10 = []

        Mach = []
        Mach_plus5 = []
        Mach_min5 = []
        Mach_plus10 = []
        Mach_min10 = []

        Ptotav = []
        Ptotav_plus5 = []
        Ptotav_min5 = []
        Ptotav_plus10 = []
        Ptotav_min10 = []

        Ttotav = []
        Ttotav_plus5 = []
        Ttotav_min5 = []
        Ttotav_plus10 = []
        Ttotav_min10 = []
        

        rows=[]
     

        with open(file,'r') as csvfile:
            plots = csv.reader(csvfile, delimiter = ',')
            for row in plots:
                rows.append(row)
        
        k=0
        for i in range (1,len(rows)-1):
            j = rows[i][1]
            
            
            if  float(j) < 100:
                k+=1
                R.append(float(rows[i][0]))
                
                # a = rows[k][1].split()
                Pav.append(float( rows[i][1] ))
                Pav_plus5.append(Pav[k-1]*1.05)
                Pav_min5.append(Pav[k-1]*0.95)
                Pav_plus10.append(Pav[k-1]*1.1)
                Pav_min10.append(Pav[k-1]*0.9)

                a = rows[i][2].split()
                Tav.append(float(  a[0]  ))
                Tav_plus5.append(Tav[k-1]*1.05)
                Tav_min5.append(Tav[k-1]*0.95)
                Tav_plus10.append(Tav[k-1]*1.1)
                Tav_min10.append(Tav[k-1]*0.9)


                a = rows[i][3].split()
                alphaav.append(-float(  a[0]  ))
                alphaav_plus5.append(alphaav[k-1]*1.05)
                alphaav_min5.append(alphaav[k-1]*0.95)
                alphaav_plus10.append(alphaav[k-1]*1.1)
                alphaav_min10.append(alphaav[k-1]*0.9)

                Mach.append(float(rows[i][4]))
                Mach_plus5.append(Mach[k-1]*1.05)
                Mach_min5.append(Mach[k-1]*0.95)
                Mach_plus10.append(Mach[k-1]*1.1)
                Mach_min10.append(Mach[k-1]*0.9)

                Ptotav.append(float( rows[i][5] ))
                Ptotav_plus5.append(Ptotav[k-1]*1.05)
                Ptotav_min5.append(Ptotav[k-1]*0.95)
                Ptotav_plus10.append(Ptotav[k-1]*1.1)
                Ptotav_min10.append(Ptotav[k-1]*0.9)

                a = rows[i][6].split()
                Ttotav.append(float(  a[0]  ))
                Ttotav_plus5.append(Ttotav[k-1]*1.05)
                Ttotav_min5.append(Ttotav[k-1]*0.95)
                Ttotav_plus10.append(Ttotav[k-1]*1.1)
                Ttotav_min10.append(Ttotav[k-1]*0.9)
            
        self.R_radial_CFX.append(R)

        self.Pav_radial_CFX.append(Pav)
        self.Pav_radial_CFX_plus5.append(Pav_plus5)
        self.Pav_radial_CFX_min5.append(Pav_min5 )
        self.Pav_radial_CFX_plus10.append(Pav_plus10) 
        self.Pav_radial_CFX_min10.append(Pav_min10)

        self.Tav_radial_CFX.append(Tav)
        self.Tav_radial_CFX_plus5.append(Tav_plus5)
        self.Tav_radial_CFX_min5.append(Tav_min5) 
        self.Tav_radial_CFX_plus10.append(Tav_plus10) 
        self.Tav_radial_CFX_min10.append(Tav_min10)

        self.alphaav_radial_CFX.append(alphaav)
        self.alphaav_radial_CFX_plus5.append(alphaav_plus5)
        self.alphaav_radial_CFX_min5.append(alphaav_min5) 
        self.alphaav_radial_CFX_plus10.append(alphaav_plus10) 
        self.alphaav_radial_CFX_min10.append(alphaav_min10)

        self.Mach_radial_CFX.append(Mach)
        self.Mach_radial_CFX_plus5.append(Mach_plus5)
        self.Mach_radial_CFX_min5.append(Mach_min5) 
        self.Mach_radial_CFX_plus10.append(Mach_plus10) 
        self.Mach_radial_CFX_min10.append(Mach_min10)

        self.Ptotav_radial_CFX.append(Ptotav)
        self.Ptotav_radial_CFX_plus5.append(Ptotav_plus5)
        self.Ptotav_radial_CFX_min5.append(Ptotav_min5 )
        self.Ptotav_radial_CFX_plus10.append(Ptotav_plus10) 
        self.Ptotav_radial_CFX_min10.append(Ptotav_min10)

        self.Ttotav_radial_CFX.append(Ttotav)
        self.Ttotav_radial_CFX_plus5.append(Ttotav_plus5)
        self.Ttotav_radial_CFX_min5.append(Ttotav_min5) 
        self.Ttotav_radial_CFX_plus10.append(Ttotav_plus10) 
        self.Ttotav_radial_CFX_min10.append(Ttotav_min10)

    
        
         
    
    
    def get_paraview_axial_results(self,file):

        X = []
        area = [] 
        Pav = []
        Ptotav = []
        Tav = []
        Ttotav = []
        alphaav = []
        mdot = []
        rows=[]
        Mach = []
        mfluxav = []
        dS = []


        
        with open(file,'r') as csvfile:
            plots = csv.reader(csvfile, delimiter = ',')
            for row in plots:
                rows.append(row)

        for i in range (1,len(rows)):
            X.append(float(rows[i][0]))
            mdot.append(float(rows[i][1]))
            area.append(float(rows[i][2]))
            Pav.append(float(rows[i][3])/float(rows[1][6]))
            Tav.append(float(rows[i][4])/float(rows[1][7]))
            
            dS.append(float(rows[i][5]))

            Ptotav.append(float(rows[i][6])/float(rows[1][6]))
            Ttotav.append(float(rows[i][7])/float(rows[1][7]))
          

            alphaav.append(-float(rows[i][8]))
            Mach.append(float(rows[i][9]))
            # mfluxav.append(float(rows[i][9]))

            mfluxav.append(float(rows[i][1])/float(rows[i][2]))
            

        self.X_axial_para.append(X) 
        self.Pav_axial_para.append(Pav)
        self.Ptotav_axial_para.append(Ptotav)
        self.Tav_axial_para.append(Tav)
        self.Ttotav_axial_para.append(Ttotav)
        self.alphaav_axial_para.append(alphaav)
        self.mdot_axial_para.append(mdot)
        self.Mach_axial_para.append(Mach)
        self.mfluxav_axial_para.append(mfluxav)
        self.dS_axial_para.append(dS)
        
        self.Ptotav_IN_para.append(float(rows[1][6]))
        self.Ttotav_IN_para.append(float(rows[1][7]))
    

    def get_CFX_axial_results(self,axial_file):
    
        Z = []
       

        Pav = []
        Pav_plus5 = []
        Pav_min5 = []
        Pav_plus10 = []
        Pav_min10 = []

        Ptotav = []
        Ptotav_plus5 = []
        Ptotav_min5 = []
        Ptotav_plus10 = []
        Ptotav_min10 = []

        Tav = []
        Tav_plus5 = []
        Tav_min5 = []
        Tav_plus10 = []
        Tav_min10 = []

        Ttotav = []
        Ttotav_plus5 = []
        Ttotav_min5 = []
        Ttotav_plus10 = []
        Ttotav_min10 = []

        alphaav = []
        alphaav_plus5 = []
        alphaav_min5 = []
        alphaav_plus10 = []
        alphaav_min10 = []

        mfluxav = []
        mfluxav_plus5 = []
        mfluxav_min5 = []
        mfluxav_plus10 = []
        mfluxav_min10 = []

        mdot = []
        mdot_plus5 = []
        mdot_min5 = []
        mdot_plus10 = []
        mdot_min10 = []

        Mach = []
        Mach_plus5 = []
        Mach_min5 = []
        Mach_plus10 = []
        Mach_min10 = []

        dS = []
        dS_plus5 = []
        dS_min5 = []
        dS_plus10 = []
        dS_min10 = []

        rows=[]
     

        with open(axial_file,'r') as csvfile:
            plots = csv.reader(csvfile, delimiter = ',')
            for row in plots:
                rows.append(row)
        
    
        for i in range (1,len(rows)-1):
            Z.append(float(rows[i][0]))
        
            Pav.append(float(rows[i][1]))
            Pav_plus5.append(Pav[i-1]*1.05)
            Pav_min5.append(Pav[i-1]*0.95)
            Pav_plus10.append(Pav[i-1]*1.1)
            Pav_min10.append(Pav[i-1]*0.9)

            Ptotav.append(float(rows[i][2]))
            Ptotav_plus5.append(Ptotav[i-1]*1.05)
            Ptotav_min5.append(Ptotav[i-1]*0.95)
            Ptotav_plus10.append(Ptotav[i-1]*1.1)
            Ptotav_min10.append(Ptotav[i-1]*0.9)

            Tav.append(float(rows[i][3]))
            Tav_plus5.append(Tav[i-1]*1.05)
            Tav_min5.append(Tav[i-1]*0.95)
            Tav_plus10.append(Tav[i-1]*1.1)
            Tav_min10.append(Tav[i-1]*0.9)

            Ttotav.append(float(rows[i][4]))
            Ttotav_plus5.append(Ttotav[i-1]*1.05)
            Ttotav_min5.append(Ttotav[i-1]*0.95)
            Ttotav_plus10.append(Ttotav[i-1]*1.1)
            Ttotav_min10.append(Ttotav[i-1]*0.9)

            a = rows[i][5].split()
            alphaav.append(-float(  a[0]  ))
            alphaav_plus5.append(alphaav[i-1]*1.05)
            alphaav_min5.append(alphaav[i-1]*0.95)
            alphaav_plus10.append(alphaav[i-1]*1.1)
            alphaav_min10.append(alphaav[i-1]*0.9)

            f = rows[i][6].split()
            mfluxav.append(float( f[0] ))
            mfluxav_plus5.append(mfluxav[i-1]*1.05)
            mfluxav_min5.append(mfluxav[i-1]*0.95)
            mfluxav_plus10.append(mfluxav[i-1]*1.1)
            mfluxav_min10.append(mfluxav[i-1]*0.9)

            m = rows[i][7].split()
            mdot.append(float( m[0] ))
            mdot_plus5.append(mdot[i-1]*1.05)
            mdot_min5.append(mdot[i-1]*0.95)
            mdot_plus10.append(mdot[i-1]*1.1)
            mdot_min10.append(mdot[i-1]*0.9)

           
            Mach.append(float(rows[i][8]))
            Mach_plus5.append(Mach[i-1]*1.05)
            Mach_min5.append(Mach[i-1]*0.95)
            Mach_plus10.append(Mach[i-1]*1.1)
            Mach_min10.append(Mach[i-1]*0.9)

            dS.append(float(rows[i][9]))
            dS_plus5.append(dS[i-1]*1.05)
            dS_min5.append(dS[i-1]*0.95)
            dS_plus10.append(dS[i-1]*1.1)
            dS_min10.append(dS[i-1]*0.9)

        self.Z_axial_CFX = Z

        self.Pav_axial_CFX = Pav
        self.Pav_axial_CFX_plus5=Pav_plus5
        self.Pav_axial_CFX_min5=Pav_min5 
        self.Pav_axial_CFX_plus10=Pav_plus10 
        self.Pav_axial_CFX_min10=Pav_min10

        self.Ptotav_axial_CFX = Ptotav
        self.Ptotav_axial_CFX_plus5=Ptotav_plus5
        self.Ptotav_axial_CFX_min5=Ptotav_min5 
        self.Ptotav_axial_CFX_plus10=Ptotav_plus10 
        self.Ptotav_axial_CFX_min10=Ptotav_min10

        self.Tav_axial_CFX = Tav
        self.Tav_axial_CFX_plus5=Tav_plus5
        self.Tav_axial_CFX_min5=Tav_min5 
        self.Tav_axial_CFX_plus10=Tav_plus10 
        self.Tav_axial_CFX_min10=Tav_min10

        self.Ttotav_axial_CFX = Ttotav
        self.Ttotav_axial_CFX_plus5=Ttotav_plus5
        self.Ttotav_axial_CFX_min5=Ttotav_min5 
        self.Ttotav_axial_CFX_plus10=Ttotav_plus10 
        self.Ttotav_axial_CFX_min10=Ttotav_min10

        self.alphaav_axial_CFX = alphaav
        self.alphaav_axial_CFX_plus5 = alphaav_plus5
        self.alphaav_axial_CFX_min5 = alphaav_min5 
        self.alphaav_axial_CFX_plus10 = alphaav_plus10 
        self.alphaav_axial_CFX_min10 = alphaav_min10

        self.mdot_axial_CFX = mdot
        self.mdot_axial_CFX_plus5=mdot_plus5
        self.mdot_axial_CFX_min5=mdot_min5 
        self.mdot_axial_CFX_plus10=mdot_plus10 
        self.mdot_axial_CFX_min10=mdot_min10

        self.mfluxav_axial_CFX = mfluxav
        self.mfluxav_axial_CFX_plus5=mfluxav_plus5
        self.mfluxav_axial_CFX_min5=mfluxav_min5 
        self.mfluxav_axial_CFX_plus10=mfluxav_plus10 
        self.mfluxav_axial_CFX_min10=mfluxav_min10

        self.Mach_axial_CFX = Mach
        self.Mach_axial_CFX_plus5=Mach_plus5
        self.Mach_axial_CFX_min5=Mach_min5 
        self.Mach_axial_CFX_plus10=Mach_plus10 
        self.Mach_axial_CFX_min10=Mach_min10

        self.dS_axial_CFX = dS
        self.dS_axial_CFX_plus5=dS_plus5
        self.dS_axial_CFX_min5=dS_min5
        self.dS_axial_CFX_plus10=dS_plus10
        self.dS_axial_CFX_min10=dS_min10

        
        





   