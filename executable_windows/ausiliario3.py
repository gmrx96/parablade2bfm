import matplotlib.pyplot as plt
from scipy.optimize import newton
import os
import numpy as np
import csv
import math


class Compute_GCI():
    # inserisci nella cartella apposita file obiettivi nominati come fine60x60x3

    def __init__(self,GCI_folder):


        self.GCI_folder = GCI_folder
        


        self.get_files()

        self.get_eps()

        self.get_p()

        self.Richardson_extrap()

        self.get_GCI()

        for i in range(0,3):
            self.plot_GCI(i)

        self.write_GCI()
    
    def write_GCI(self):
        csv_out = self.GCI_folder+"\\GCI_results.csv"
        self.output_file = open(csv_out, "w+", newline='')
        writer=csv.writer( self.output_file)
        allines=[]
        allines.append(['','Axial','Radial','Tangential', 'TOTAL'])
        
        line1 = ['perHall_GCI32_mdot']
        line2 = ['perHall_GCI21_mdot']
        line3 = ['perHall_asymp_mdot']
        line4 = ['perHall_extr_mdot_err3']
        line5 = ['perHall_extr_mdot_err2']
        line6 = ['perHall_extr_mdot_err1']

        line7 = ['perHall_GCI32_eff']
        line8 = ['perHall_GCI21_eff']
        line9 = ['perHall_asymp_eff']
        line10 = ['perHall_extr_eff_err3']
        line11 = ['perHall_extr_eff_err2']
        line12 = ['perHall_extr_eff_err1']


        
        for i in range(0,3):
            line1.append(self.perHall_GCI_mdot[i,0])
            line2.append(self.perHall_GCI_mdot[i,1])
            line3.append(self.perHall_GCI_mdot[i,0]/(self.perHall_GCI_mdot[i,1]*self.perHall_r[i,1]**self.perHall_p_mdot[i,0]))
            line4.append(self.perHall_err_extrapolated_mdot[i,0])
            line5.append(self.perHall_err_extrapolated_mdot[i,1])
            line6.append(self.perHall_err_extrapolated_mdot[i,2])

            line7.append(self.perHall_GCI_eff[i,0])
            line8.append(self.perHall_GCI_eff[i,1])
            line9.append(self.perHall_GCI_eff[i,0]/(self.perHall_GCI_eff[i,1]*self.perHall_r[i,1]**self.perHall_p_eff[i,0]))
            line10.append(self.perHall_err_extrapolated_eff[i,0])
            line11.append(self.perHall_err_extrapolated_eff[i,1])
            line12.append(self.perHall_err_extrapolated_eff[i,2])

        GCI32_tot_mdot = self.perHall_GCI_mdot[0,0]+self.perHall_GCI_mdot[1,0]+self.perHall_GCI_mdot[2,0]
        GCI21_tot_mdot = self.perHall_GCI_mdot[0,1]+self.perHall_GCI_mdot[1,1]+self.perHall_GCI_mdot[2,1]
        line1.append( GCI32_tot_mdot)
        line2.append( GCI21_tot_mdot)
        GCI32_tot_eff = self.perHall_GCI_eff[0,0]+self.perHall_GCI_eff[1,0]+self.perHall_GCI_eff[2,0]
        GCI21_tot_eff  = self.perHall_GCI_eff[0,1]+self.perHall_GCI_eff[1,1]+self.perHall_GCI_eff[2,1]
        line7.append( GCI32_tot_eff)
        line8.append( GCI21_tot_eff)
       
        
        line13 = ['FaHall_GCI32_mdot']
        line14 = ['FaHall_GCI21_mdot']
        line15 = ['FaHall_asymp_mdot']
        line16 = ['FaHall_extr_mdot_err3']
        line17 = ['FaHall_extr_mdot_err2']
        line18 = ['FaHall_extr_mdot_err1']

        line19 = ['FaHall_GCI32_eff']
        line20 = ['FaHall_GCI21_eff']
        line21 = ['FaHall_asymp_eff']
        line22 = ['FaHall_extr_eff_err3']
        line23 = ['FaHall_extr_eff_err2']
        line24 = ['FaHall_extr_eff_err1']

        for i in range(0,3):
            line13.append(self.FaHall_GCI_mdot[i,0])
            line14.append(self.FaHall_GCI_mdot[i,1])
            line15.append(self.FaHall_GCI_mdot[i,0]/(self.FaHall_GCI_mdot[i,1]*self.FaHall_r[i,1]**self.FaHall_p_mdot[i,0]))
            line16.append(self.FaHall_err_extrapolated_mdot[i,0])
            line17.append(self.FaHall_err_extrapolated_mdot[i,1])
            line18.append(self.FaHall_err_extrapolated_mdot[i,2])

            line19.append(self.FaHall_GCI_eff[i,0])
            line20.append(self.FaHall_GCI_eff[i,1])
            line21.append(self.FaHall_GCI_eff[i,0]/(self.FaHall_GCI_eff[i,1]*self.FaHall_r[i,1]**self.FaHall_p_eff[i,0]))
            line22.append(self.FaHall_err_extrapolated_eff[i,0])
            line23.append(self.FaHall_err_extrapolated_eff[i,1])
            line24.append(self.FaHall_err_extrapolated_eff[i,2])


        GCI32_tot_mdot = self.FaHall_GCI_mdot[0,0]+self.FaHall_GCI_mdot[1,0]+self.FaHall_GCI_mdot[2,0]
        GCI21_tot_mdot = self.FaHall_GCI_mdot[0,1]+self.FaHall_GCI_mdot[1,1]+self.FaHall_GCI_mdot[2,1]
        line13.append( GCI32_tot_mdot)
        line14.append( GCI21_tot_mdot)
        GCI32_tot_eff = self.FaHall_GCI_eff[0,0]+self.FaHall_GCI_eff[1,0]+self.FaHall_GCI_eff[2,0]
        GCI21_tot_eff  = self.FaHall_GCI_eff[0,1]+self.FaHall_GCI_eff[1,1]+self.FaHall_GCI_eff[2,1]
        line19.append( GCI32_tot_eff)
        line20.append( GCI21_tot_eff)
        
        allines.append(line1)
        allines.append(line2)
        allines.append(line3)
        allines.append(line4)
        allines.append(line5)
        allines.append(line6)
        allines.append(['','','',''])
        allines.append(line7)
        allines.append(line8)
        allines.append(line9)
        allines.append(line10)
        allines.append(line11)
        allines.append(line12)
        allines.append(['','','',''])
        allines.append(line13)
        allines.append(line14)
        allines.append(line15)
        allines.append(line16)
        allines.append(line17)
        allines.append(line18)
        allines.append(['','','',''])
        allines.append(line19)
        allines.append(line20)
        allines.append(line21)
        allines.append(line22)
        allines.append(line23)
        allines.append(line24)
        allines.append(['','','',''])
        writer.writerows(allines)
        
        self.output_file.close()

        


    def plot_GCI(self,i):

        perHall_x = self.perHall_h[i,:]
        FaHall_x = self.FaHall_h[i,:]
    
        

        fig,ax1 = plt.subplots(2,sharex=True)
     
        
        if i == 0:
            fig.suptitle('Axial grid study', fontsize = 16)
            ax1[1].set_xlabel('Axial nodes',fontsize = 12)
        elif i == 1:
            fig.suptitle('Radial grid study', fontsize = 16)
            ax1[1].set_xlabel('Radial nodes',fontsize = 12)
        if i == 2:
            fig.suptitle('Tangential grid study', fontsize = 16)
            ax1[1].set_xlabel('Tangential nodes',fontsize = 12)

        X = np.linspace(perHall_x[0], perHall_x[-1], 50)
        Y = np.ones(len(X)) *self.perHall_extrapolated_phi_mdot[i,0]
        ax1[0].set_ylabel('m_dot ',fontsize = 14)
        ax1[0].plot(perHall_x,self.perHall_phi_mdot[i,:],'b')
        ax1[0].plot(perHall_x,self.perHall_phi_mdot[i,:], 'ks', markerfacecolor='none', markeredgecolor='b')
        # ax1[0].plot(X,Y,'--b')
        ax1[0].axhline(y=self.perHall_extrapolated_phi_mdot[i,0],color='b', linestyle='--')

        
        X = np.linspace(FaHall_x[0], FaHall_x[-1], 50)
        Y = np.ones(len(X)) *self.FaHall_extrapolated_phi_mdot[i,0]
        if i ==2:
            ax2=[0,0]
            ax2[0] = ax1[0].twiny()
            ax2[0].plot(FaHall_x,self.FaHall_phi_mdot[i,:],'r')
            ax2[0].plot(FaHall_x,self.FaHall_phi_mdot[i,:], 'ks', markerfacecolor='none', markeredgecolor='r')
            # ax2[0].plot(X,Y,'--r')
            ax2[0].axhline(y=self.FaHall_extrapolated_phi_mdot[i,0],color='r', linestyle='--')
            ax2[0].set_xlabel('Tangential nodes', color='r')
            ax2[0].tick_params(axis='x', labelcolor='r')
            
      
        else:
            ax1[0].plot(FaHall_x,self.FaHall_phi_mdot[i,:],'r')
            ax1[0].plot(FaHall_x,self.FaHall_phi_mdot[i,:], 'ks', markerfacecolor='none', markeredgecolor='r')
            # ax1[0].plot(X,Y,'--r')  
            ax1[0].axhline(y=self.FaHall_extrapolated_phi_mdot[i,0],color='r', linestyle='--')
           
    
   


        X = np.linspace(perHall_x[0], perHall_x[-1], 50)
        Y = np.ones(len(X)) *self.perHall_extrapolated_phi_eff[i,0]
        ax1[1].set_ylabel('eff ',fontsize = 14)
        ax1[1].plot(perHall_x,self.perHall_phi_eff[i,:],'b')
        ax1[1].plot(perHall_x,self.perHall_phi_eff[i,:], 'ks', markerfacecolor='none', markeredgecolor='b')
        # ax1[1].plot(X,Y,'--b')
        ax1[1].axhline(y=self.perHall_extrapolated_phi_eff[i,0],color='b', linestyle='--')
       
        
        X = np.linspace(FaHall_x[0], FaHall_x[-1], 50)
        Y = np.ones(len(X)) *self.FaHall_extrapolated_phi_eff[i,0]
        if i ==2:
            ax2[1] = ax1[1].twiny()
            # ax2[1].get_shared_y_axes()
            ax2[1].plot(FaHall_x,self.FaHall_phi_eff[i,:],'r')
            ax2[1].plot(FaHall_x,self.FaHall_phi_eff[i,:], 'ks', markerfacecolor='none', markeredgecolor='r')
            # ax2[1].plot(X,Y,'--r')
            ax2[1].axhline(y=self.FaHall_extrapolated_phi_eff[i,0],color='r', linestyle='--')
            ax2[1].axis('off')
            
            

        else:
            ax1[1].plot(FaHall_x,self.FaHall_phi_eff[i,:],'r')
            ax1[1].plot(FaHall_x,self.FaHall_phi_eff[i,:], 'ks', markerfacecolor='none', markeredgecolor='r')
            # ax1[1].plot(X,Y,'--r')
            ax1[1].axhline(y=self.FaHall_extrapolated_phi_eff[i,0],color='r', linestyle='--')
        if i!=2:
            labels = ['perHall SU2 results','_nolegend_','perHall extrapolated value','FaHall results','_nolegend_','FaHall extrapolated value']
        else:
            labels = ['perHall SU2 results','_nolegend_','perHall extrapolated value','_nolegend_','_nolegend_','_nolegend_','FaHall results','_nolegend_','FaHall extrapolated value']
        
        
        
        fig.tight_layout() 
        fig.subplots_adjust(bottom=0.25)   ##  Need to play with this number.
        leg = fig.legend(labels=labels, loc="lower center", ncol=2,labelcolor='linecolor')

       
        # plt.show()
        if i == 0:
            plt.savefig(self.GCI_folder+"\\Axial_study.png")
        if i == 1:
            plt.savefig(self.GCI_folder+"\\Radial_study.png")
        if i == 2:
            plt.savefig(self.GCI_folder+"\\Tangential_study.png")





    def get_GCI(self):

        self.perHall_err_mdot=np.zeros((3,2))
        self.perHall_err_eff=np.zeros((3,2))

        self.perHall_err_extrapolated_mdot=np.zeros((3,3))
        self.perHall_err_extrapolated_eff=np.zeros((3,3))

        self.perHall_GCI_mdot=np.zeros((3,2))
        self.perHall_GCI_eff=np.zeros((3,2))


        for i in range(0,3):
            self.perHall_err_mdot[i,0] = abs((self.perHall_phi_mdot[i,1]-self.perHall_phi_mdot[i,0])/self.perHall_phi_mdot[i,1])
            self.perHall_GCI_mdot[i,0] = 1.25*self.perHall_err_mdot[i,0]/(self.perHall_r[i,0]**self.perHall_p_mdot[i,0]-1)*100
            self.perHall_err_mdot[i,0]=self.perHall_err_mdot[i,0]*100
            
            self.perHall_err_mdot[i,1] = abs((self.perHall_phi_mdot[i,2]-self.perHall_phi_mdot[i,1])/self.perHall_phi_mdot[i,2])
            self.perHall_GCI_mdot[i,1] = 1.25*self.perHall_err_mdot[i,1]/(self.perHall_r[i,1]**self.perHall_p_mdot[i,0]-1)*100
            self.perHall_err_mdot[i,1]=self.perHall_err_mdot[i,1]*100

            self.perHall_err_eff[i,0] = abs((self.perHall_phi_eff[i,1]-self.perHall_phi_eff[i,0])/self.perHall_phi_eff[i,1])
            self.perHall_GCI_eff[i,0] = 1.25*self.perHall_err_eff[i,0]/(self.perHall_r[i,0]**self.perHall_p_eff[i,0]-1)*100
            self.perHall_err_eff[i,0]=self.perHall_err_eff[i,0]*100

            self.perHall_err_eff[i,1] = abs((self.perHall_phi_eff[i,2]-self.perHall_phi_eff[i,1])/self.perHall_phi_eff[i,2])
            self.perHall_GCI_eff[i,1] = 1.25*self.perHall_err_eff[i,1]/(self.perHall_r[i,1]**self.perHall_p_eff[i,0]-1)*100
            self.perHall_err_eff[i,1]=self.perHall_err_eff[i,1]*100

            for j in range(0,3):
                self.perHall_err_extrapolated_mdot[i,j] = abs((self.perHall_extrapolated_phi_mdot[i,0]-self.perHall_phi_mdot[i,j])/self.perHall_extrapolated_phi_mdot[i,0]) *100
                self.perHall_err_extrapolated_eff[i,j] = abs((self.perHall_extrapolated_phi_eff[i,0]-self.perHall_phi_eff[i,j])/self.perHall_extrapolated_phi_eff[i,0]) *100


        self.FaHall_err_mdot=np.zeros((3,2))
        self.FaHall_err_eff=np.zeros((3,2))

        self.FaHall_err_extrapolated_mdot=np.zeros((3,3))
        self.FaHall_err_extrapolated_eff=np.zeros((3,3))

        self.FaHall_GCI_mdot=np.zeros((3,2))
        self.FaHall_GCI_eff=np.zeros((3,2))


        for i in range(0,3):
            self.FaHall_err_mdot[i,0] = abs((self.FaHall_phi_mdot[i,1]-self.FaHall_phi_mdot[i,0])/self.FaHall_phi_mdot[i,1])
            self.FaHall_GCI_mdot[i,0] = 1.25*self.FaHall_err_mdot[i,0]/(self.FaHall_r[i,0]**self.FaHall_p_mdot[i,0]-1)*100
            self.FaHall_err_mdot[i,0]=self.FaHall_err_mdot[i,0]*100

            self.FaHall_err_mdot[i,1] = abs((self.FaHall_phi_mdot[i,2]-self.FaHall_phi_mdot[i,1])/self.FaHall_phi_mdot[i,2])
            self.FaHall_GCI_mdot[i,1] = 1.25*self.FaHall_err_mdot[i,1]/(self.FaHall_r[i,1]**self.FaHall_p_mdot[i,0]-1)*100
            self.FaHall_err_mdot[i,1]=self.FaHall_err_mdot[i,1]*100

            self.FaHall_err_eff[i,0] = abs((self.FaHall_phi_eff[i,1]-self.FaHall_phi_eff[i,0])/self.FaHall_phi_eff[i,1])
            self.FaHall_GCI_eff[i,0] = 1.25*self.FaHall_err_eff[i,0]/(self.FaHall_r[i,0]**self.FaHall_p_eff[i,0]-1)*100
            self.FaHall_err_eff[i,0]=self.FaHall_err_eff[i,0]*100

            self.FaHall_err_eff[i,1] = abs((self.FaHall_phi_eff[i,2]-self.FaHall_phi_eff[i,1])/self.FaHall_phi_eff[i,2])
            self.FaHall_GCI_eff[i,1] = 1.25*self.FaHall_err_eff[i,1]/(self.FaHall_r[i,1]**self.FaHall_p_eff[i,0]-1)*100
            print(self.FaHall_GCI_eff[i,1])
            self.FaHall_err_eff[i,1]=self.FaHall_err_eff[i,1]*100

            for j in range(0,3):
                self.FaHall_err_extrapolated_mdot[i,j] = abs((self.FaHall_extrapolated_phi_mdot[i,0]-self.FaHall_phi_mdot[i,j])/self.FaHall_extrapolated_phi_mdot[i,0]) *100
                self.FaHall_err_extrapolated_eff[i,j] = abs((self.FaHall_extrapolated_phi_eff[i,0]-self.FaHall_phi_eff[i,j])/self.FaHall_extrapolated_phi_eff[i,0]) *100

        



    
    def Richardson_extrap(self):
        self.perHall_extrapolated_phi_mdot = np.zeros((3,1))
        self.perHall_extrapolated_phi_eff = np.zeros((3,1))

        for i in range(0,3):
           
            self.perHall_extrapolated_phi_mdot[i,0] = ((self.perHall_r[i,1]**self.perHall_p_mdot[i,0])* self.perHall_phi_mdot[i,2] - self.perHall_phi_mdot[i,1])/(self.perHall_r[i,1]**self.perHall_p_mdot[i,0]-1)
            # if i==2:
            #     self.perHall_extrapolated_phi_mdot[i,0] = ((self.perHall_r[i,1]**1.25)* self.perHall_phi_mdot[i,2] - self.perHall_phi_mdot[i,1])/(self.perHall_r[i,1]**1.25-1)

            # print(self.perHall_r[i,1])
            # print(self.perHall_p_mdot[i,0])
            # print(self.perHall_phi_mdot[i,2])
            # print(self.perHall_extrapolated_phi_mdot[i,0])
            self.perHall_extrapolated_phi_eff[i,0] = (self.perHall_r[i,1]**self.perHall_p_eff[i,0] * self.perHall_phi_eff[i,2] - self.perHall_phi_eff[i,1])/(self.perHall_r[i,1]**self.perHall_p_eff[i,0]-1)
        
        self.FaHall_extrapolated_phi_mdot = np.zeros((3,1))
        self.FaHall_extrapolated_phi_eff = np.zeros((3,1))

        for i in range(0,3):
            self.FaHall_extrapolated_phi_mdot[i,0] = (self.FaHall_r[i,1]**self.FaHall_p_mdot[i,0] * self.FaHall_phi_mdot[i,-1] - self.FaHall_phi_mdot[i,1])/(self.FaHall_r[i,1]**self.FaHall_p_mdot[i,0]-1)
            self.FaHall_extrapolated_phi_eff[i,0] = (self.FaHall_r[i,1]**self.FaHall_p_eff[i,0] * self.FaHall_phi_eff[i,-1] - self.FaHall_phi_eff[i,1])/(self.FaHall_r[i,1]**self.FaHall_p_eff[i,0]-1)
 



    def get_eps(self):

        # 1st row = axial
        # 2nd row = radial
        # 3rd row = tangential

        self.perHall_phi_mdot = np.zeros((3,3))
        self.perHall_phi_eff = np.zeros((3,3))
        self.perHall_eps_mdot=np.zeros((3,2))
        self.perHall_eps_eff=np.zeros((3,2))

      
        
        for i in range(0,3):
                f = open(self.perHall_axial_files[i], 'r')
                Lines = f.readlines()
                f.close()
                self.perHall_phi_eff[0,i]=float(Lines[0].split('=')[1])
                self.perHall_phi_mdot[0,i]=float(Lines[1].split('=')[1])

                f = open(self.perHall_radial_files[i], 'r')
                Lines = f.readlines()
                f.close()
                self.perHall_phi_eff[1,i]=float(Lines[0].split('=')[1])
                self.perHall_phi_mdot[1,i]=float(Lines[1].split('=')[1])

                f = open(self.perHall_tangential_files[i], 'r')
                Lines = f.readlines()
                f.close()
                self.perHall_phi_eff[2,i]=float(Lines[0].split('=')[1])
                self.perHall_phi_mdot[2,i]=float(Lines[1].split('=')[1])
       


        self.perHall_eps_mdot[0,:]=[ self.perHall_phi_mdot[0,0] -self.perHall_phi_mdot[0,1] , self.perHall_phi_mdot[0,1] -self.perHall_phi_mdot[0,2]  ]
        self.perHall_eps_mdot[1,:]=[ self.perHall_phi_mdot[1,0] -self.perHall_phi_mdot[1,1] , self.perHall_phi_mdot[1,1] -self.perHall_phi_mdot[1,2]  ]
        self.perHall_eps_mdot[2,:]=[ self.perHall_phi_mdot[2,0] -self.perHall_phi_mdot[2,1] , self.perHall_phi_mdot[2,1] -self.perHall_phi_mdot[2,2]  ]


        self.perHall_eps_eff[0,:]=[ self.perHall_phi_eff[0,0] -self.perHall_phi_eff[0,1] , self.perHall_phi_eff[0,1] -self.perHall_phi_eff[0,2]  ]
        self.perHall_eps_eff[1,:]=[ self.perHall_phi_eff[1,0] -self.perHall_phi_eff[1,1] , self.perHall_phi_eff[1,1] -self.perHall_phi_eff[1,2]  ]
        self.perHall_eps_eff[2,:]=[ self.perHall_phi_eff[2,0] -self.perHall_phi_eff[2,1] , self.perHall_phi_eff[2,1] -self.perHall_phi_eff[2,2]  ]



        self.FaHall_phi_mdot = np.zeros((3,3))
        self.FaHall_phi_eff = np.zeros((3,3))
        self.FaHall_eps_mdot=np.zeros((3,2))
        self.FaHall_eps_eff=np.zeros((3,2))

      
        
        for i in range(0,3):
                f = open(self.FaHall_axial_files[i], 'r')
                Lines = f.readlines()
                f.close()
                self.FaHall_phi_eff[0,i]=float(Lines[0].split('=')[1])
                self.FaHall_phi_mdot[0,i]=float(Lines[1].split('=')[1])

                f = open(self.FaHall_radial_files[i], 'r')
                Lines = f.readlines()
                f.close()
                self.FaHall_phi_eff[1,i]=float(Lines[0].split('=')[1])
                self.FaHall_phi_mdot[1,i]=float(Lines[1].split('=')[1])

                f = open(self.FaHall_tangential_files[i], 'r')
                Lines = f.readlines()
                f.close()
                self.FaHall_phi_eff[2,i]=float(Lines[0].split('=')[1])
                self.FaHall_phi_mdot[2,i]=float(Lines[1].split('=')[1])
       

        self.FaHall_eps_mdot[0,:]=[ self.FaHall_phi_mdot[0,1] -self.FaHall_phi_mdot[0,0] , self.FaHall_phi_mdot[0,2] -self.FaHall_phi_mdot[0,1]  ]
        self.FaHall_eps_mdot[1,:]=[ self.FaHall_phi_mdot[1,1] -self.FaHall_phi_mdot[1,0] , self.FaHall_phi_mdot[1,2] -self.FaHall_phi_mdot[1,1]  ]
        self.FaHall_eps_mdot[2,:]=[ self.FaHall_phi_mdot[2,1] -self.FaHall_phi_mdot[2,0] , self.FaHall_phi_mdot[2,2] -self.FaHall_phi_mdot[2,1]  ]

        self.FaHall_eps_eff[0,:]=[ self.FaHall_phi_eff[0,1] -self.FaHall_phi_eff[0,0] , self.FaHall_phi_eff[0,2] -self.FaHall_phi_eff[0,1]  ]
        self.FaHall_eps_eff[1,:]=[ self.FaHall_phi_eff[1,1] -self.FaHall_phi_eff[1,0] , self.FaHall_phi_eff[1,2] -self.FaHall_phi_eff[1,1]  ]
        self.FaHall_eps_eff[2,:]=[ self.FaHall_phi_eff[2,1] -self.FaHall_phi_eff[2,0] , self.FaHall_phi_eff[2,2] -self.FaHall_phi_eff[2,1]  ]


        
    def get_p(self):
        
        self.perHall_p_mdot=np.zeros((3,1))
        self.perHall_p_eff=np.zeros((3,1))
        self.perHall_h = np.zeros((3,3))
        self.perHall_r=np.zeros((3,2))

        self.perHall_h[0,0] = int(self.perHall_axial_files[0].split("\\")[-1].split('_')[0].split('x')[0])
        self.perHall_h[1,0] = int(self.perHall_radial_files[0].split("\\")[-1].split('_')[0].split('x')[1])
        self.perHall_h[2,0] = int(self.perHall_tangential_files[0].split("\\")[-1].split('_')[0].split('x')[2])

        self.perHall_h[0,1] = int(self.perHall_axial_files[1].split("\\")[-1].split('_')[0].split('x')[0])
        self.perHall_h[1,1]  = int(self.perHall_radial_files[1].split("\\")[-1].split('_')[0].split('x')[1])
        self.perHall_h[2,1]  = int(self.perHall_tangential_files[1].split("\\")[-1].split('_')[0].split('x')[2])

        self.perHall_h[0,2] = int(self.perHall_axial_files[2].split("\\")[-1].split('_')[0].split('x')[0])
        self.perHall_h[1,2]  = int(self.perHall_radial_files[2].split("\\")[-1].split('_')[0].split('x')[1])
        self.perHall_h[2,2]  = int(self.perHall_tangential_files[2].split("\\")[-1].split('_')[0].split('x')[2])

        
        
        self.perHall_r[0,0] = self.perHall_h[0,1]/self.perHall_h[0,0]
        self.perHall_r[0,1] = self.perHall_h[0,2]/self.perHall_h[0,1]

        self.perHall_r[1,0] = self.perHall_h[1,1]/self.perHall_h[1,0]
        self.perHall_r[1,1] = self.perHall_h[1,2]/self.perHall_h[1,1]

        self.perHall_r[2,0] = self.perHall_h[2,1]/self.perHall_h[2,0]
        self.perHall_r[2,1] = self.perHall_h[2,2]/self.perHall_h[2,1]

        # def q(x,r21,r32,s):
        #     return np.log((r21**x-s)/(r32**x-s))
        
        # # def p(x,r21,eps21,eps32):
        # #         return 1/np.log(r21)*abs(np.log(abs(eps32/eps21))+q(x))
        
        # def get_p(r21,r32,eps21,eps32,s):
        #     def p(x):
        #         return 1/np.log(r21)*abs(np.log(abs(eps32/eps21))+q(x,r21,r32,s))
        #     return p

        # f = get_f(K=2, B=3)
        # print newton(f, 3, maxiter=1000)

        # Get mass convergence order------------------------------------------------------------
 
        for i in range(0,3):
            p_guess_mdot = abs(np.log(abs(self.perHall_eps_mdot[i,0]/self.perHall_eps_mdot[i,1])))/np.log(self.perHall_r[i,1])
            s = (self.perHall_eps_mdot[i,0]/self.perHall_eps_mdot[i,1])/abs(self.perHall_eps_mdot[i,0]/self.perHall_eps_mdot[i,1])

            # g = get_p(self.perHall_r[i,1],self.perHall_r[i,0],self.perHall_eps_mdot[i,1],self.perHall_eps_mdot[i,0],s)
            # risultato= (newton(g, p_guess_mdot, maxiter=5000))

            p_guess_mdot = abs(np.log(abs(self.perHall_eps_mdot[i,0]/self.perHall_eps_mdot[i,1])))/np.log(self.perHall_r[i,1])
            s = (self.perHall_eps_mdot[i,0]/self.perHall_eps_mdot[i,1])/abs(self.perHall_eps_mdot[i,0]/self.perHall_eps_mdot[i,1])

            if i==2:
                p_guess_mdot=2
            p_old = p_guess_mdot

            err =1
            j=1
            while abs(err) > 0.001:
                j+=1
                q = np.log((self.perHall_r[i,1]**p_old-s)/(self.perHall_r[i,0]**p_old-s))
                p_new = 1/np.log(self.perHall_r[i,1])*abs(np.log(abs(self.perHall_eps_mdot[i,0]/self.perHall_eps_mdot[i,1]))+q)
                err = p_new-p_old
                p_old = p_new


 
            self.perHall_p_mdot[i,0] = p_new
        # ---------------------------------------------------------------------------------------------



        # Get efficiency convergence order------------------------------------------------------------
        for i in range(0,3):

            p_guess_eff = abs(np.log(abs(self.perHall_eps_eff[i,0]/self.perHall_eps_eff[i,1])))/np.log(self.perHall_r[i,1])
            s = (self.perHall_eps_eff[i,0]/self.perHall_eps_eff[i,1])/abs(self.perHall_eps_eff[i,0]/self.perHall_eps_eff[i,1])

            p_old = p_guess_eff
            err =1
            j=1
            while abs(err) > 0.001:
                j+=1
                q = np.log((self.perHall_r[i,1]**p_old-s)/(self.perHall_r[i,0]**p_old-s))
                p_new = 1/np.log(self.perHall_r[i,1])*abs(np.log(abs(self.perHall_eps_eff[i,0]/self.perHall_eps_eff[i,1]))+q)
            
                err = p_new-p_old
                p_old = p_new
 
            self.perHall_p_eff[i,0] = p_new
        #---------------------------------------------------------------------------------------------

        
        self.FaHall_p_mdot=np.zeros((3,1))
        self.FaHall_p_eff=np.zeros((3,1))
        self.FaHall_h = np.zeros((3,3))
        self.FaHall_r=np.zeros((3,2))

        self.FaHall_h[0,0] = int(self.FaHall_axial_files[0].split("\\")[-1].split('_')[0].split('x')[0])
        self.FaHall_h[1,0] = int(self.FaHall_radial_files[0].split("\\")[-1].split('_')[0].split('x')[1])
        self.FaHall_h[2,0] = int(self.FaHall_tangential_files[0].split("\\")[-1].split('_')[0].split('x')[2])

        self.FaHall_h[0,1] = int(self.FaHall_axial_files[1].split("\\")[-1].split('_')[0].split('x')[0])
        self.FaHall_h[1,1]  = int(self.FaHall_radial_files[1].split("\\")[-1].split('_')[0].split('x')[1])
        self.FaHall_h[2,1]  = int(self.FaHall_tangential_files[1].split("\\")[-1].split('_')[0].split('x')[2])

        self.FaHall_h[0,2] = int(self.FaHall_axial_files[2].split("\\")[-1].split('_')[0].split('x')[0])
        self.FaHall_h[1,2]  = int(self.FaHall_radial_files[2].split("\\")[-1].split('_')[0].split('x')[1])
        self.FaHall_h[2,2]  = int(self.FaHall_tangential_files[2].split("\\")[-1].split('_')[0].split('x')[2])

        
        
        self.FaHall_r[0,0] = self.FaHall_h[0,1]/self.FaHall_h[0,0]
        self.FaHall_r[0,1] = self.FaHall_h[0,2]/self.FaHall_h[0,1]

        self.FaHall_r[1,0] = self.FaHall_h[1,1]/self.FaHall_h[1,0]
        self.FaHall_r[1,1] = self.FaHall_h[1,2]/self.FaHall_h[1,1]

        self.FaHall_r[2,0] = self.FaHall_h[2,1]/self.FaHall_h[2,0]
        self.FaHall_r[2,1] = self.FaHall_h[2,2]/self.FaHall_h[2,1]

        
        # Get mass convergence order------------------------------------------------------------

        for i in range(0,3):

            p_guess_mdot = abs(np.log(abs(self.FaHall_eps_mdot[i,0]/self.FaHall_eps_mdot[i,1])))/np.log(self.FaHall_r[i,1])
            s = (self.FaHall_eps_mdot[i,0]/self.FaHall_eps_mdot[i,1])/abs(self.FaHall_eps_mdot[i,0]/self.FaHall_eps_mdot[i,1])

            p_old = p_guess_mdot
            err =1
            j=1
            while abs(err) > 0.001:
                j+=1
                q = np.log((self.FaHall_r[i,1]**p_old-s)/(self.FaHall_r[i,0]**p_old-s))
                p_new = 1/np.log(self.FaHall_r[i,1])*abs(np.log(abs(self.FaHall_eps_mdot[i,0]/self.FaHall_eps_mdot[i,1]))+q)
                err = p_new-p_old
                p_old = p_new
 
            self.FaHall_p_mdot[i,0] = p_new
        #---------------------------------------------------------------------------------------------



        # Get efficiency convergence order------------------------------------------------------------
        for i in range(0,3):

            p_guess_eff = abs(np.log(abs(self.FaHall_eps_eff[i,0]/self.FaHall_eps_eff[i,1])))/np.log(self.FaHall_r[i,1])
            s = (self.FaHall_eps_eff[i,0]/self.FaHall_eps_eff[i,1])/abs(self.FaHall_eps_eff[i,0]/self.FaHall_eps_eff[i,1])

            p_old = p_guess_eff
            err =1
            j=1
            while abs(err) > 0.001:
                j+=1
                q = np.log((self.FaHall_r[i,1]**p_old-s)/(self.FaHall_r[i,0]**p_old-s))
                p_new = 1/np.log(self.FaHall_r[i,1])*abs(np.log(abs(self.FaHall_eps_eff[i,0]/self.FaHall_eps_eff[i,1]))+q)
            
                err = p_new-p_old
                p_old = p_new
 
            self.FaHall_p_eff[i,0] = p_new
        #---------------------------------------------------------------------------------------------








    def get_files(self):
        self.perHall_axial_files = []
        self.perHall_radial_files = []
        self.perHall_tangential_files = []

        self.FaHall_axial_files = []
        self.FaHall_radial_files = []
        self.FaHall_tangential_files = []

        file = os.listdir(self.GCI_folder)

        for file in os.listdir(self.GCI_folder):
            if 'perHall_reference' in file:
                self.perHall_axial_files.append(self.GCI_folder+"\\"+file)
                self.perHall_radial_files.append(self.GCI_folder+"\\"+file)
                self.perHall_tangential_files.append(self.GCI_folder+"\\"+file)
            if 'FaHall_reference' in file:
                self.FaHall_axial_files.append(self.GCI_folder+"\\"+file)
                self.FaHall_radial_files.append(self.GCI_folder+"\\"+file)
                self.FaHall_tangential_files.append(self.GCI_folder+"\\"+file)

        for file in os.listdir(self.GCI_folder):
            if 'axial' in file:
                axial_folder = self.GCI_folder+"\\axial\\"
                axial_file = os.listdir(axial_folder)

                lista= []
                for f in axial_file:
                    k = int(f.split('_')[0].split('x')[0])
                    if k not in lista:
                        lista.append(k)
                lista.sort()
                i=0

                for i in lista:
                    for f in axial_file:
                        if 'perHall' in f and int(f.split('_')[0].split('x')[0])==i:
                            self.perHall_axial_files.append(os.path.join(axial_folder, f))
                        elif 'FaHall' in f and int(f.split('_')[0].split('x')[0])==i:
                            self.FaHall_axial_files.append(os.path.join(axial_folder, f))

            elif 'radial' in file:
                radial_folder = self.GCI_folder+"\\radial\\"
                radial_file = os.listdir(radial_folder)

                lista= []
                for f in radial_file:
                    k = int(f.split('_')[0].split('x')[1])
                    if k not in lista:
                        lista.append(k)
                lista.sort()
                i=0
                for i in lista:
                    for f in radial_file:
                        if 'perHall' in f and int(f.split('_')[0].split('x')[1])==i:
                            self.perHall_radial_files.append(os.path.join(radial_folder, f))
                        elif 'FaHall' in f and int(f.split('_')[0].split('x')[1])==i:
                            self.FaHall_radial_files.append(os.path.join(radial_folder, f))

            elif 'tangential' in file:
                tangential_folder = self.GCI_folder+"\\tangential\\"
                tangential_file = os.listdir(tangential_folder)
                lista= []
                for f in tangential_file:
                    k = int(f.split('_')[0].split('x')[2])
                    if k not in lista:
                        lista.append(k)
                lista.sort()
                i=0
                for i in lista:
                    for f in tangential_file:
                        if 'perHall' in f and int(f.split('_')[0].split('x')[2])==i:
                            self.perHall_tangential_files.append(os.path.join(tangential_folder, f))
                        elif 'FaHall' in f and int(f.split('_')[0].split('x')[2])==i:
                            self.FaHall_tangential_files.append(os.path.join(tangential_folder, f))

convergenza = Compute_GCI("C:\\Users\\GMrx1\\Desktop\\GCItest_1node")
print('Done')