
import numpy as np
import sys
import os

# from executable_windows.Parablade2BFM import ICEM

# from executable_windows.Parablade2BFM import ICEM

class CFX_Post_session:

    def __init__(self,IN,ICEM,P):

        # Create the directory to store CFX simulation results and postprocess
        DIR = os.getenv('P2BFM')
        self.CFX_POST_HOME = DIR+"\\CFX_Post"
        if not os.path.isdir(self.CFX_POST_HOME):
            os.system("mkdir "+self.CFX_POST_HOME)
        if not os.path.isdir(self.CFX_POST_HOME+"\\axial_data"):
            os.system("mkdir "+self.CFX_POST_HOME+"\\axial_data")
        if not os.path.isdir(self.CFX_POST_HOME+"\\radial_data"):
            os.system("mkdir "+self.CFX_POST_HOME+"\\radial_data")
        if not os.path.isdir(self.CFX_POST_HOME+"\\objectives"):
            os.system("mkdir "+self.CFX_POST_HOME+"\\objectives")

        self.P = P       
        self.Cp = IN["gamma"][0]*IN["R_gas"][0]/(IN["gamma"][0] - 1)   # Storing the
        #                                                                                  # specific enthalpy
        self.IN = IN
        self.X_LE = P.X_LE[0]
        self.X_TE = P.X_TE[0]
        self.Z_LE = P.Z_LE[0]
        self.Z_TE = P.Z_TE[0]
        self.header()
        n_planes = 100
        n_tables=0





        #---------------AXIAL DATA---------------------#
        
        # Create plane at the inlet
        # self.create_plane('XY',0,ICEM.coords_hub[0,0])
        self.planes=[]
        lunghezza = (ICEM.coords_hub[0,-1]-ICEM.coords_hub[0,0])*0.99
        h= lunghezza/(n_planes-1)
        
        # self.Z=[ICEM.coords_hub[0,0]]
        self.Z = []

        # Iterate to create all the planes
        for i in range(n_planes):
            # self.Z.append(self.Z[i-1]+h)
            self.Z.append(ICEM.coords_hub[0,0]+h*i)
            self.create_plane('XY',i,self.Z[i])
            self.planes.append(i)
        # self.create_plane('XY',n_planes,ICEM.coords_hub[0,-1]*0.99)

        
        # Create the expressions
        for i in self.planes:
            self.axial_expressions(i)

        # Create the table
        self.axial_table()
        n_tables+=1



        #----------------RADIAL DATA--------------------#
       

        raggio = np.sqrt(ICEM.coords_shroud[2,0]**2+ICEM.coords_shroud[1,0]**2)-np.sqrt(ICEM.coords_hub[2,0]**2+ICEM.coords_hub[1,0]**2)
        r= raggio/(n_planes-1)
        # self.R=[np.sqrt(ICEM.coords_hub[2,0]**2+ICEM.coords_hub[1,0]**2)]
        self.R = []
        # self.axial_station=[0,-1]
        self.axial_station = ['Inlet','LE','TE','Outlet']
        self.axial_station_X = [self.Z[0] , min(self.X_LE)-lunghezza*0.01 , max(self.X_TE)+lunghezza*0.01 ,self.Z[-1]]
        # self.axial_station_X = [self.Z[0] , min(self.X_LE)-h , max(self.X_TE)+h ,self.Z[-1]]
        
        # self.polyline=np.zeros((len(self.axial_station),n_planes))

        for i in range(n_planes):
            self.R.append(np.sqrt(ICEM.coords_hub[2,0]**2+ICEM.coords_hub[1,0]**2) + r*i)
            self.create_revolution_surface(i)
        
        k = 0
        for j in range(len(self.axial_station)):
            for i in range(n_planes):
                k+=1
            # I am studying just inlet and outlet plane
            
                self.create_contour(j,i,k)        # Create a contour "k" from axial station "j" and radial cylinder "i"
                self.create_polyline(k)           # Create a polyline from contour "k" to integrate my expressions
                self.radial_expressions(k)
            
            self.radial_table(j)
            n_tables+=1

        #---------------------OBJECTIVES------------------#

        self.objective_functions()
        self.objective_table(n_tables+1)
        n_tables+=1
        
        f = open(self.CFX_POST_HOME+"\\CFXsession_"+self.IN['MACHINE_NAME']+".cse", "a")

        # set the settings
        f.write(">quit\n")
        f.close()

        self.file = self.CFX_POST_HOME+"\\CFXsession_"+self.IN['MACHINE_NAME']+".cse"
    

    def objective_table(self,j):
         # Create the table to store the expression results and transfer it into .csv file
        f = open(self.CFX_POST_HOME+"\\CFXsession_"+self.IN['MACHINE_NAME']+".cse", "a")
        f.write("TABLE: Table "+str(j)+"\n")
        f.write("  Table Exists = True\n")
        f.write("END\n\n")

        # Insert Efficiency
        f.write("TABLE:Table "+str(j)+"\n")
        f.write("  TABLE CELLS:\n")
        f.write("    A1 = \"Efficiency\", False, False, False, Left, True, 0, Font Name, 1|1, %10.8e,True, ffffff, 000000, True\n")
        f.write("    A2 = \"=Etatt\", False, False, False, Left, True, 0, Font Name, 1|1, %10.8e,True, ffffff, 000000, True\n")
        f.write("  END\n")
        f.write("END\n\n")
    
        # Insert Mass flow averaged static pressure
        f.write("TABLE:Table "+str(j)+"\n")
        f.write("  TABLE CELLS:\n")
        f.write("    B1 = \"mdot\", False, False, False, Left, True, 0, Font Name, 1|1, %10.8e,True, ffffff, 000000, True\n")
        f.write("    B2 = \"=mdot\", False, False, False, Left, True, 0, Font Name, 1|1, %10.8e,True, ffffff, 000000, True\n")
        f.write("  END\n")
        f.write("END\n\n")

        # Insert Mass flow averaged static pressure
        f.write("TABLE:Table "+str(j)+"\n")
        f.write("  TABLE CELLS:\n")
        f.write("    C1 = \"Power\", False, False, False, Left, True, 0, Font Name, 1|1, %10.8e,True, ffffff, 000000, True\n")
        f.write("    C2 = \"=power\", False, False, False, Left, True, 0, Font Name, 1|1, %10.8e,True, ffffff, 000000, True\n")
        f.write("  END\n")
        f.write("END\n\n")

        # Insert Mass flow averaged static pressure
        f.write("TABLE:Table "+str(j)+"\n")
        f.write("  TABLE CELLS:\n")
        f.write("    D1 = \"alphaout\", False, False, False, Left, True, 0, Font Name, 1|1, %10.8e,True, ffffff, 000000, True\n")
        f.write("    D2 = \"=alphaout\", False, False, False, Left, True, 0, Font Name, 1|1, %10.8e,True, ffffff, 000000, True\n")
        f.write("  END\n")
        f.write("END\n\n")

        # Save the table
        f.write("TABLE:Table "+str(j)+"\n")
        f.write("  Export Table Only = True\n")
        f.write("  Table Export HTML Title =\n")
        f.write("  Table Export HTML Caption Position = Bottom\n")
        f.write("  Table Export HTML Caption =\n")
        f.write("  Table Export HTML Border Width = 1\n")
        f.write("  Table Export HTML Cell Padding = 5\n")
        f.write("  Table Export HTML Cell Spacing = 1\n")
        f.write("  Table Export Lines = All\n")
        f.write("  Table Export Trailing Separators = True\n")
        f.write("  Table Export Separator = Tab\n")
        f.write("END\n")
        f.write(">table save="+self.CFX_POST_HOME+"\\objectives\\"+self.IN['MACHINE_NAME']+"_objectives.csv, name=Table "+str(j)+"\n\n")
        

    

    def objective_functions(self):
        # Create the reference contours for polyline
        f = open(self.CFX_POST_HOME+"\\CFXsession_"+self.IN['MACHINE_NAME']+".cse", "a")

  
        # Mass flow averaged static pressure 
        f.write("LIBRARY:\n")
        f.write("  CEL:\n")
        f.write("    EXPRESSIONS:\n")
        f.write("      Ttoutis = massFlowAve(Total Temperature in Stn Frame)@R1 Inlet*(massFlowAve(Total Pressure in Stn Frame)@R1 Outlet/massFlowAve(Total Pressure in Stn Frame)@R1 Inlet)^(("+str(self.IN['gamma'][0])+" - 1)/"+str(self.IN['gamma'][0])+")\n")
        f.write("    END\n")
        f.write("  END\n")
        f.write("END\n\n")

        # massFlowAve(Total Temperature in Stn Frame)@R1 Inlet*(massFlowAve(Total Pressure in Stn Frame)@R1 Outlet/massFlowAve(Total Pressure in Stn Frame)@R1 Inlet)**((gamma - 1)/gamma)
        # Tt_out_s = Tt_in * (Pt_out/Pt_in) ** ((gamma - 1)/gamma)

        f.write("LIBRARY:\n")
        f.write("  CEL:\n")
        f.write("    EXPRESSIONS:\n")
        f.write("      Etatt = (Ttoutis - massFlowAve(Total Temperature in Stn Frame)@R1 Inlet)/((massFlowAve(Total Temperature in Stn Frame)@R1 Outlet-massFlowAve(Total Temperature in Stn Frame)@R1 Inlet))\n")
        f.write("    END\n")
        f.write("  END\n")
        f.write("END\n\n")
        # eta_tt = (Tt_out_s - Tt_in) / (Tt_out - Tt_in)

        f.write("LIBRARY:\n")
        f.write("  CEL:\n")
        f.write("    EXPRESSIONS:\n")
        f.write("      mdot = (-"+str(self.IN["N_BLADES"][0])+"*massFlow()@R1 Outlet)\n")
        f.write("    END\n")
        f.write("  END\n")
        f.write("END\n\n")

        f.write("LIBRARY:\n")
        f.write("  CEL:\n")
        f.write("    EXPRESSIONS:\n")
        f.write("      power = "+str(self.IN["R_gas"][0]/(1-1/self.IN['gamma'][0]))+"*mdot*(massFlowAve(Total Temperature in Stn Frame)@R1 Outlet-massFlowAve(Total Temperature in Stn Frame)@R1 Inlet)\n")
        f.write("    END\n")
        f.write("  END\n")
        f.write("END\n\n")
        # power_in = self.fluid_parameters.C_p * mdot * (Tt_out - Tt_in)

        f.write("LIBRARY:\n")
        f.write("  CEL:\n")
        f.write("    EXPRESSIONS:\n")
        f.write("      alphaout = massFlowAve(atan(Velocity  in Stn Frame v / Velocity  in Stn Frame w) )@R1 Outlet\n")
        f.write("    END\n")
        f.write("  END\n")
        f.write("END\n\n")
        # angle_out = self.output_data.axial_data.Alpha[-1]


    def radial_table(self,j):
          # Create the table to store the expression results and transfer it into .csv file
        f = open(self.CFX_POST_HOME+"\\CFXsession_"+self.IN['MACHINE_NAME']+".cse", "a")

        f.write("TABLE: Table "+str(j+2)+"\n")
        f.write("  Table Exists = True\n")
        f.write("END\n\n")

        # Insert R coordinates (first row)
        f.write("TABLE:Table "+str(j+2)+"\n")
        f.write("  TABLE CELLS:\n")
        f.write("    A1 = \"R [m]\", False, False, False, Left, True, 0, Font Name, 1|1, %10.8e,True, ffffff, 000000, True\n")
        for i in self.planes:
            f.write("    A"+str(i+2)+" = \""+str(self.R[i])+"\", False, False, False, Left, True, 0, Font Name, 1|1, %10.8e,True, ffffff, 000000, True\n")
        f.write("  END\n")
        f.write("END\n\n")
        
        # Insert Mass flow averaged static pressure
        f.write("TABLE:Table "+str(j+2)+"\n")
        f.write("  TABLE CELLS:\n")
        f.write("    B1 = \"Pmfavradial\", False, False, False, Left, True, 0, Font Name, 1|1, %10.8e,True, ffffff, 000000, True\n")
        for i in self.planes:
            f.write("    B"+str(i+2)+" = \"=Pmfavradial"+str(i+1+j*len(self.planes))+"\", False, False, False, Left, True, 0, Font Name, 1|1, %10.8e,True, ffffff, 000000, True\n")
        f.write("  END\n")
        f.write("END\n\n")
        

        # Insert Mass flow averaged static temperature
        f.write("TABLE:Table "+str(j+2)+"\n")
        f.write("  TABLE CELLS:\n")
        f.write("    C1 = \"Tmfavradial\", False, False, False, Left, True, 0, Font Name, 1|1, %10.8e,True, ffffff, 000000, True\n")
        for i in self.planes:
            f.write("    C"+str(i+2)+" = \"=Tmfavradial"+str(i+1+j*len(self.planes))+"\", False, False, False, Left, True, 0, Font Name, 1|1, %10.8e,True, ffffff, 000000, True\n")
        f.write("  END\n")
        f.write("END\n\n")


        # Insert Mass flow averaged absolute angle
        f.write("TABLE:Table "+str(j+2)+"\n")
        f.write("  TABLE CELLS:\n")
        f.write("    D1 = \"alphamfavradial [Degrees]\", False, False, False, Left, True, 0, Font Name, 1|1, %10.8e,True, ffffff, 000000, True\n")
        for i in self.planes:
            f.write("    D"+str(i+2)+" = \"=alphamfavradial"+str(i+1+j*len(self.planes))+"\", False, False, False, Left, True, 0, Font Name, 1|1, %10.8e,True, ffffff, 000000, True\n")
        f.write("  END\n")
        f.write("END\n\n")

        # Insert mass flow averaged MACH
        f.write("TABLE:Table "+str(j+2)+"\n")
        f.write("  TABLE CELLS:\n")
        f.write("    E1 = \"Machmfavradial\", False, False, False, Left, True, 0, Font Name, 1|1, %10.8e,True, ffffff, 000000, True\n")
        for i in self.planes:
            f.write("    E"+str(i+2)+" = \"=Machmfavradial"+str(i+1+j*len(self.planes))+"\", False, False, False, Left, True, 0, Font Name, 1|1, %10.8e,True, ffffff, 000000, True\n")
        f.write("  END\n")
        f.write("END\n\n")

        # Insert Mass flow averaged total pressure
        f.write("TABLE:Table "+str(j+2)+"\n")
        f.write("  TABLE CELLS:\n")
        f.write("    F1 = \"Ptotmfavradial\", False, False, False, Left, True, 0, Font Name, 1|1, %10.8e,True, ffffff, 000000, True\n")
        for i in self.planes:
            f.write("    F"+str(i+2)+" = \"=Ptotmfavradial"+str(i+1+j*len(self.planes))+"\", False, False, False, Left, True, 0, Font Name, 1|1, %10.8e,True, ffffff, 000000, True\n")
        f.write("  END\n")
        f.write("END\n\n")

         # Insert Mass flow averaged total temperature
        f.write("TABLE:Table "+str(j+2)+"\n")
        f.write("  TABLE CELLS:\n")
        f.write("    G1 = \"Ttotmfavradial\", False, False, False, Left, True, 0, Font Name, 1|1, %10.8e,True, ffffff, 000000, True\n")
        for i in self.planes:
            f.write("    G"+str(i+2)+" = \"=Ttotmfavradial"+str(i+1+j*len(self.planes))+"\", False, False, False, Left, True, 0, Font Name, 1|1, %10.8e,True, ffffff, 000000, True\n")
        f.write("  END\n")
        f.write("END\n\n")

        





        # Save the table
        f.write("TABLE:Table "+str(j+2)+"\n")
        f.write("  Export Table Only = True\n")
        f.write("  Table Export HTML Title =\n")
        f.write("  Table Export HTML Caption Position = Bottom\n")
        f.write("  Table Export HTML Caption =\n")
        f.write("  Table Export HTML Border Width = 1\n")
        f.write("  Table Export HTML Cell Padding = 5\n")
        f.write("  Table Export HTML Cell Spacing = 1\n")
        f.write("  Table Export Lines = All\n")
        f.write("  Table Export Trailing Separators = True\n")
        f.write("  Table Export Separator = Tab\n")
        f.write("END\n")
        f.write(">table save="+self.CFX_POST_HOME+"\\radial_data\\"+self.IN['MACHINE_NAME']+"_"+self.axial_station[j]+"_radial_results.csv, name=Table "+str(j+2)+"\n\n")
        

 


        f.close()


    def radial_expressions(self,k):
       
          # Create the reference contours for polyline
        f = open(self.CFX_POST_HOME+"\\CFXsession_"+self.IN['MACHINE_NAME']+".cse", "a")

  
        # Mass flow averaged static pressure 
        f.write("LIBRARY:\n")
        f.write("  CEL:\n")
        f.write("    EXPRESSIONS:\n")
        # f.write("      Pmfavradial"+str(k)+" = ave(Density*Velocity in Stn Frame w*Pressure)@Polyline "+str(k)+" /ave(Density*Velocity in Stn Frame w)@Polyline "+str(k)+"\n")
        f.write("      Pmfavradial"+str(k)+" = ave(Density*Velocity in Stn Frame w*Pressure)@Polyline "+str(k)+" /ave(Density*Velocity in Stn Frame w*Total Pressure in Stn Frame)@Plane 0\n")
        #  poly2 = ave(Density*Velocity in Stn Frame w*Pressure)@Polyline 1 /ave(Density*Velocity in Stn Frame w)@Polyline 1
        f.write("    END\n")
        f.write("  END\n")
        f.write("END\n\n")

        # Mass flow averaged total pressure 
        f.write("LIBRARY:\n")
        f.write("  CEL:\n")
        f.write("    EXPRESSIONS:\n")
        f.write("      Ptotmfavradial"+str(k)+" = ave(Density*Velocity in Stn Frame w*Total Pressure in Stn Frame)@Polyline "+str(k)+"  /ave(Density*Velocity in Stn Frame w*Total Pressure in Stn Frame)@Plane 0\n")
        # polyPtot = ave(Density*Velocity in Stn Frame w*Total Pressure in Stn Frame)@Polyline 10 / ave(Density*Velocity in Stn Frame w)@Polyline 10
        f.write("    END\n")
        f.write("  END\n")
        f.write("END\n\n")

        # Mass flow averaged static temperature
        f.write("LIBRARY:\n")
        f.write("  CEL:\n")
        f.write("    EXPRESSIONS:\n")
        f.write("      Tmfavradial"+str(k)+" = ave(Density*Velocity in Stn Frame w*Temperature)@Polyline "+str(k)+" /ave(Density*Velocity in Stn Frame w*Total Temperature in Stn Frame)@Plane 0\n")
        f.write("    END\n")
        f.write("  END\n")
        f.write("END\n\n")

        # Mass flow averaged total temperature 
        f.write("LIBRARY:\n")
        f.write("  CEL:\n")
        f.write("    EXPRESSIONS:\n")
        f.write("      Ttotmfavradial"+str(k)+" = ave(Density*Velocity in Stn Frame w*Total Temperature in Stn Frame)@Polyline "+str(k)+" /ave(Density*Velocity in Stn Frame w*Total Temperature in Stn Frame)@Plane 0\n")
        f.write("    END\n")
        f.write("  END\n")
        f.write("END\n\n")


        # Mass flow averaged flow angle 
        f.write("LIBRARY:\n")
        f.write("  CEL:\n")
        f.write("    EXPRESSIONS:\n")
        f.write("      alphamfavradial"+str(k)+" = ave(Density*Velocity in Stn Frame w*atan(Velocity  in Stn Frame v / Velocity  in Stn Frame w))@Polyline "+str(k)+" /ave(Density*Velocity in Stn Frame w)@Polyline "+str(k)+"\n")
        f.write("    END\n")
        f.write("  END\n")
        f.write("END\n\n")



        # Mass flow averaged total temperature 
        f.write("LIBRARY:\n")
        f.write("  CEL:\n")
        f.write("    EXPRESSIONS:\n")
        f.write("      Machmfavradial"+str(k)+" = ave(Density*Velocity in Stn Frame w*Mach Number in Stn Frame)@Polyline "+str(k)+" /ave(Density*Velocity in Stn Frame w)@Polyline "+str(k)+"\n")
        f.write("    END\n")
        f.write("  END\n")
        f.write("END\n\n")






    def create_polyline(self,k):
          # Create the reference contours for polyline
        f = open(self.CFX_POST_HOME+"\\CFXsession_"+self.IN['MACHINE_NAME']+".cse", "a")

        f.write("POLYLINE:Polyline "+str(k)+"\n")
        f.write("  Apply Instancing Transform = On\n")
        f.write("  Colour = 0, 1, 0\n")
        f.write("  Colour Map = Default Colour Map\n")
        f.write("  Colour Mode = Constant\n")
        f.write("  Colour Scale = Linear\n")
        f.write("  Colour Variable = Pressure\n")
        f.write("  Colour Variable Boundary Values = Hybrid\n")
        f.write("  Contour Level = 1\n")
        f.write("  Contour Name = /CONTOUR:Contour "+str(k)+"\n")
        f.write("  Domain List = /DOMAIN GROUP:All Domains\n")
        f.write("  Input File =\n")
        f.write("  Instancing Transform = /DEFAULT INSTANCE TRANSFORM:Default Transform\n")
        f.write("  Line Width = 2\n")
        f.write("  Max = 0.0 [Pa]\n")
        f.write("  Min = 0.0 [Pa]\n")
        f.write("  Option = From Contour\n")
        f.write("  Range = Global\n")
        f.write("  OBJECT VIEW TRANSFORM:\n")
        f.write("    Apply Reflection = Off\n")
        f.write("    Apply Rotation = Off\n")
        f.write("    Apply Scale = Off\n")
        f.write("    Apply Translation = Off\n")
        f.write("    Principal Axis = Z\n")
        f.write("    Reflection Plane Option = XY Plane\n")
        f.write("    Rotation Angle = 0.0 [degree]\n")
        f.write("    Rotation Axis From = 0 [m], 0 [m], 0 [m]\n")
        f.write("    Rotation Axis To = 0 [m], 0 [m], 0 [m]\n")
        f.write("    Rotation Axis Type = Principal Axis\n")
        f.write("    Scale Vector = 1 , 1 , 1\n")
        f.write("    Translation Vector = 0 [m], 0 [m], 0 [m]\n")
        f.write("    X = 0.0 [m]\n")
        f.write("    Y = 0.0 [m]\n")
        f.write("    Z = 0.0 [m]\n")
        f.write("  END\n")
        f.write("END\n")
   
        f.close()

        






    def create_contour(self,j,i,k):

         # Create the reference contours for polyline
        f = open(self.CFX_POST_HOME+"\\CFXsession_"+self.IN['MACHINE_NAME']+".cse", "a")

        f.write("CONTOUR:Contour "+str(k)+"\n")
        f.write("  Apply Instancing Transform = On\n")
        f.write("  Clip Contour = Off\n")
        f.write("  Colour Map = Default Colour Map\n")
        f.write("  Colour Scale = Linear\n")
        f.write("  Colour Variable = Z\n")
        f.write("  Colour Variable Boundary Values = Conservative\n")
        f.write("  Constant Contour Colour = Off\n")
        f.write("  Contour Range = User Specified\n")
        f.write("  Culling Mode = No Culling\n")
        f.write("  Domain List = /DOMAIN GROUP:All Domains\n")
        f.write("  Draw Contours = On\n")
        f.write("  Font = Sans Serif\n")
        f.write("  Fringe Fill = On\n")
        f.write("  Instancing Transform = /DEFAULT INSTANCE TRANSFORM:Default Transform\n")
        f.write("  Lighting = On\n")
        f.write("  Line Colour = 0, 0, 0\n")
        f.write("  Line Colour Mode = Default\n")
        f.write("  Line Width = 1\n")
        f.write("  Location List = /SURFACE OF REVOLUTION:Surface Of Revolution "+str(i)+"\n")
        f.write("  Max = "+str(self.axial_station_X[j])+" [m]\n")
        f.write("  Min = "+str(self.axial_station_X[j])+" [m]\n")
        f.write("  Number of Contours = 11\n")
        f.write("  Show Numbers = Off\n")
        f.write("  Specular Lighting = On\n")
        f.write("  Surface Drawing = Smooth Shading\n")
        f.write("  Text Colour = 0, 0, 0\n")
        f.write("  Text Colour Mode = Default\n")
        f.write("  Text Height = 0.024\n")
        f.write("  Transparency = 0.0\n")
        f.write("  Use Face Values = Off\n")
        f.write("  Value List = 0 [m],1 [m]\n")
        f.write("  OBJECT VIEW TRANSFORM:\n")
        f.write("    Apply Reflection = Off\n")
        f.write("    Apply Rotation = Off\n")
        f.write("    Apply Scale = Off\n")
        f.write("    Apply Translation = Off\n")
        f.write("    Principal Axis = Z\n")
        f.write("    Reflection Plane Option = XY Plane\n")
        f.write("    Rotation Angle = 0.0 [degree]\n")
        f.write("    Rotation Axis From = 0 [m], 0 [m], 0 [m]\n")
        f.write("    Rotation Axis To = 0 [m], 0 [m], 0 [m]\n")
        f.write("    Rotation Axis Type = Principal Axis\n")
        f.write("    Scale Vector = 1 , 1 , 1\n")
        f.write("    Translation Vector = 0 [m], 0 [m], 0 [m]\n")
        f.write("    X = 0.0 [m]\n")
        f.write("    Y = 0.0 [m]\n")
        f.write("    Z = 0.0 [m]\n")
        f.write("  END\n")
        f.write("END\n")

        
     
        f.close()
    



    def create_revolution_surface(self,i):

        f = open(self.CFX_POST_HOME+"\\CFXsession_"+self.IN['MACHINE_NAME']+".cse", "a")

        f.write("SURFACE OF REVOLUTION:Surface Of Revolution "+str(i)+"\n")
        f.write("  Apply Instancing Transform = On\n")
        f.write("  Apply Texture = Off\n")
        f.write("  Blend Texture = On\n")
        f.write("  Colour = 0.75, 0.75, 0.75\n")
        f.write("  Colour Map = Default Colour Map\n")
        f.write("  Colour Mode = Constant\n")
        f.write("  Colour Scale = Linear\n")
        f.write("  Colour Variable = Pressure\n")
        f.write("  Colour Variable Boundary Values = Hybrid\n")
        f.write("  Culling Mode = No Culling\n")
        f.write("  Domain List = /DOMAIN GROUP:All Domains\n")
        f.write("  Draw Faces = On\n")
        f.write("  Draw Lines = Off\n")
        f.write("  Ending Axial Shift = 0.0 [m]\n")
        f.write("  Ending Radial Shift = 0.0 [m]\n")
        f.write("  Instancing Transform = /DEFAULT INSTANCE TRANSFORM:Default Transform\n")
        f.write("  Lighting = On\n")
        f.write("  Line Colour = 0, 0, 0\n")
        f.write("  Line Colour Mode = Default\n")
        f.write("  Line Width = 1\n")
        f.write("  Max = 0.0 [Pa]\n")
        f.write("  Meridional Point 1 = "+str(self.Z[0])+" [m], "+str(self.R[i])+" [m]\n")
        f.write("  Meridional Point 2 = "+str(self.Z[-1]*1.01)+" [m], 2 [m]\n")
        f.write("  Meridional Points = 10\n")
        f.write("  Min = 0.0 [Pa]\n")
        f.write("  Option = Cylinder\n")
        f.write("  Principal Axis = Z\n")
        f.write("  Project to AR Plane = On\n")
        f.write("  Range = Global\n")
        f.write("  Render Edge Angle = 0 [degree]\n")
        f.write("  Rotation Axis From = 0 [m], 0 [m], 0 [m]\n")
        f.write("  Rotation Axis To = 1 [m], 0 [m], 0 [m]\n")
        f.write("  Rotation Axis Type = Principal Axis\n")
        f.write("  Specular Lighting = On\n")
        f.write("  Starting Axial Shift = 0.0 [m]\n")
        f.write("  Starting Radial Shift = 0.0 [m]\n")
        f.write("  Surface Drawing = Smooth Shading\n")
        f.write("  Texture Angle = 0\n")
        f.write("  Texture Direction = 0 , 1 , 0\n")
        f.write("  Texture File =\n")
        f.write("  Texture Material = Metal\n")
        f.write("  Texture Position = 0 , 0\n")
        f.write("  Texture Scale = 1\n")
        f.write("  Texture Type = Predefined\n")
        f.write("  Theta Points = 360\n")
        f.write("  Tile Texture = Off\n")
        f.write("  Transform Texture = Off\n")
        f.write("  Transparency = 0.0\n")
        f.write("  Use Angle Range = Off\n")
        f.write("  OBJECT VIEW TRANSFORM:\n")
        f.write("    Apply Reflection = Off\n")
        f.write("    Apply Rotation = Off\n")
        f.write("    Apply Scale = Off\n")
        f.write("    Apply Translation = Off\n")
        f.write("    Principal Axis = Z\n")
        f.write("    Reflection Plane Option = XY Plane\n")
        f.write("    Rotation Angle = 0.0 [degree]\n")
        f.write("    Rotation Axis From = 0 [m], 0 [m], 0 [m]\n")
        f.write("    Rotation Axis To = 1 [m], 0 [m], 0 [m]\n")
        f.write("    Rotation Axis Type = Principal Axis\n")
        f.write("    Scale Vector = 1 , 1 , 1\n")
        f.write("    Translation Vector = 0 [m], 0 [m], 0 [m]\n")
        f.write("    X = 0.0 [m]\n")
        f.write("    Y = 0.0 [m]\n")
        f.write("    Z = 0.0 [m]\n")
        f.write("  END\n")
        f.write("END\n\n")
      
        f.close()




    def axial_table(self):
        # Create the table to store the expression results and transfer it into .csv file
        f = open(self.CFX_POST_HOME+"\\CFXsession_"+self.IN['MACHINE_NAME']+".cse", "a")

        f.write("TABLE: Table 1\n")
        f.write("  Table Exists = True\n")
        f.write("END\n\n")

        # Insert Z coordinates (first row)
        f.write("TABLE:Table 1\n")
        f.write("  TABLE CELLS:\n")
        f.write("    A1 = \"Z [m]\", False, False, False, Left, True, 0, Font Name, 1|1, %10.8e,True, ffffff, 000000, True\n")
        for i in self.planes:
            f.write("    A"+str(i+2)+" = \""+str(self.Z[i])+"\", False, False, False, Left, True, 0, Font Name, 1|1, %10.8e,True, ffffff, 000000, True\n")
        f.write("  END\n")
        f.write("END\n\n")
        
        # Insert Mass flow averaged static pressure
        f.write("TABLE:Table 1\n")
        f.write("  TABLE CELLS:\n")
        f.write("    B1 = \"Pav/Ptin\", False, False, False, Left, True, 0, Font Name, 1|1, %10.8e,True, ffffff, 000000, True\n")
        for i in self.planes:
            f.write("    B"+str(i+2)+" = \"=Pmfav"+str(i)+"\", False, False, False, Left, True, 0, Font Name, 1|1, %10.8e,True, ffffff, 000000, True\n")
        f.write("  END\n")
        f.write("END\n\n")
        
        # Insert Mass flow averaged total pressure
        f.write("TABLE:Table 1\n")
        f.write("  TABLE CELLS:\n")
        f.write("    C1 = \"Ptotav/Ptin\", False, False, False, Left, True, 0, Font Name, 1|1, %10.8e,True, ffffff, 000000, True\n")
        for i in self.planes:
            f.write("    C"+str(i+2)+" = \"=Ptotmfav"+str(i)+"\", False, False, False, Left, True, 0, Font Name, 1|1, %10.8e,True, ffffff, 000000, True\n")
        f.write("  END\n")
        f.write("END\n\n")

        # Insert Mass flow averaged static temperature
        f.write("TABLE:Table 1\n")
        f.write("  TABLE CELLS:\n")
        f.write("    D1 = \"Tav/Ttin\", False, False, False, Left, True, 0, Font Name, 1|1, %10.8e,True, ffffff, 000000, True\n")
        for i in self.planes:
            f.write("    D"+str(i+2)+" = \"=Tmfav"+str(i)+"\", False, False, False, Left, True, 0, Font Name, 1|1, %10.8e,True, ffffff, 000000, True\n")
        f.write("  END\n")
        f.write("END\n\n")

        # Insert Mass flow averaged total temperature
        f.write("TABLE:Table 1\n")
        f.write("  TABLE CELLS:\n")
        f.write("    E1 = \"Ttotav/Ttin\", False, False, False, Left, True, 0, Font Name, 1|1, %10.8e,True, ffffff, 000000, True\n")
        for i in self.planes:
            f.write("    E"+str(i+2)+" = \"=Ttotmfav"+str(i)+"\", False, False, False, Left, True, 0, Font Name, 1|1, %10.8e,True, ffffff, 000000, True\n")
        f.write("  END\n")
        f.write("END\n\n")

        # Insert Mass flow averaged absolute angle
        f.write("TABLE:Table 1\n")
        f.write("  TABLE CELLS:\n")
        f.write("    F1 = \"Averaged Absolute Angle [Degrees]\", False, False, False, Left, True, 0, Font Name, 1|1, %10.8e,True, ffffff, 000000, True\n")
        for i in self.planes:
            f.write("    F"+str(i+2)+" = \"=alphamfav"+str(i)+"\", False, False, False, Left, True, 0, Font Name, 1|1, %10.8e,True, ffffff, 000000, True\n")
        f.write("  END\n")
        f.write("END\n\n")

        # Insert Mass flow averaged massflux
        f.write("TABLE:Table 1\n")
        f.write("  TABLE CELLS:\n")
        f.write("    G1 = \"Averaged Mass Flux [Kg/m^2/s]\", False, False, False, Left, True, 0, Font Name, 1|1, %10.8e,True, ffffff, 000000, True\n")
        for i in self.planes:
            f.write("    G"+str(i+2)+" = \"=massfluxmfav"+str(i)+"\", False, False, False, Left, True, 0, Font Name, 1|1, %10.8e,True, ffffff, 000000, True\n")
        f.write("  END\n")
        f.write("END\n\n")

        #  Insert Mass flow averaged massflux
        f.write("TABLE:Table 1\n")
        f.write("  TABLE CELLS:\n")
        f.write("    H1 = \"Averaged Mass Flow [Kg/s]\", False, False, False, Left, True, 0, Font Name, 1|1, %10.8e,True, ffffff, 000000, True\n")
        for i in self.planes:
            f.write("    H"+str(i+2)+" = \"=massflow"+str(i)+"\", False, False, False, Left, True, 0, Font Name, 1|1, %10.8e,True, ffffff, 000000, True\n")
        f.write("  END\n")
        f.write("END\n\n")

         # Insert Mass flow averaged massflux
        f.write("TABLE:Table 1\n")
        f.write("  TABLE CELLS:\n")
        f.write("    I1 = \"Averaged Mach\", False, False, False, Left, True, 0, Font Name, 1|1, %10.8e,True, ffffff, 000000, True\n")
        for i in self.planes:
            f.write("    I"+str(i+2)+" = \"=Machmfav"+str(i)+"\", False, False, False, Left, True, 0, Font Name, 1|1, %10.8e,True, ffffff, 000000, True\n")
        f.write("  END\n")
        f.write("END\n\n")

         # Insert Mass flow averaged entropy rise
        f.write("TABLE:Table 1\n")
        f.write("  TABLE CELLS:\n")
        f.write("    J1 = \"Averaged deltaS\", False, False, False, Left, True, 0, Font Name, 1|1, %10.8e,True, ffffff, 000000, True\n")
        for i in self.planes:
            f.write("    J"+str(i+2)+" = \"=deltaS"+str(i)+"\", False, False, False, Left, True, 0, Font Name, 1|1, %10.8e,True, ffffff, 000000, True\n")
        f.write("  END\n")
        f.write("END\n\n")





        # Save the table
        f.write("TABLE:Table 1\n")
        f.write("  Export Table Only = True\n")
        f.write("  Table Export HTML Title =\n")
        f.write("  Table Export HTML Caption Position = Bottom\n")
        f.write("  Table Export HTML Caption =\n")
        f.write("  Table Export HTML Border Width = 1\n")
        f.write("  Table Export HTML Cell Padding = 5\n")
        f.write("  Table Export HTML Cell Spacing = 1\n")
        f.write("  Table Export Lines = All\n")
        f.write("  Table Export Trailing Separators = True\n")
        f.write("  Table Export Separator = Tab\n")
        f.write("END\n")
        f.write(">table save="+self.CFX_POST_HOME+"\\axial_data\\"+self.IN['MACHINE_NAME']+"_results.csv, name=Table 1\n\n")
        

 


        f.close()





    def axial_expressions(self,i):
        # Create all the expressions to generate graphs
        f = open(self.CFX_POST_HOME+"\\CFXsession_"+self.IN['MACHINE_NAME']+".cse", "a")
        
        # Mass flow averaged static pressure 
        f.write("LIBRARY:\n")
        f.write("  CEL:\n")
        f.write("    EXPRESSIONS:\n")
        # f.write("      Pmfav"+str(i)+" = massFlowAve(Pressure)@Plane "+str(i)+" /massFlowAve(Total Pressure)@R1 Inlet\n")
        f.write("      Pmfav"+str(i)+" = massFlowAve(Pressure)@Plane "+str(i)+" /massFlowAve(Total Pressure in Stn Frame)@Plane 0\n")
        f.write("    END\n")
        f.write("  END\n")
        f.write("END\n\n")

        # Mass flow averaged total pressure
        f.write("LIBRARY:\n")
        f.write("  CEL:\n")
        f.write("    EXPRESSIONS:\n")
        # f.write("      Ptotmfav"+str(i)+" = massFlowAve(Total Pressure  in Stn Frame)@Plane "+str(i)+" /massFlowAve(Total Pressure  in Stn Frame)@R1 Inlet\n")
        f.write("      Ptotmfav"+str(i)+" = massFlowAve(Total Pressure  in Stn Frame)@Plane "+str(i)+" /massFlowAve(Total Pressure  in Stn Frame)@Plane 0\n")
        f.write("    END\n")
        f.write("  END\n")
        f.write("END\n\n")

        # Mass flow averaged static temperature
        f.write("LIBRARY:\n")
        f.write("  CEL:\n")
        f.write("    EXPRESSIONS:\n")
        # f.write("      Tmfav"+str(i)+" = massFlowAve(Temperature)@Plane "+str(i)+" /massFlowAve(Total Temperature)@R1 Inlet\n")
        f.write("      Tmfav"+str(i)+" = massFlowAve(Temperature)@Plane "+str(i)+" /massFlowAve(Total Temperature in Stn Frame)@Plane 0\n")
        f.write("    END\n")
        f.write("  END\n")
        f.write("END\n\n")

        # Mass flow averaged total temperature
        f.write("LIBRARY:\n")
        f.write("  CEL:\n")
        f.write("    EXPRESSIONS:\n")
        # f.write("      Ttotmfav"+str(i)+" = massFlowAve(Total Temperature  in Stn Frame)@Plane "+str(i)+" /massFlowAve(Total Temperature  in Stn Frame)@R1 Inlet\n")
        f.write("      Ttotmfav"+str(i)+" = massFlowAve(Total Temperature  in Stn Frame)@Plane "+str(i)+" /massFlowAve(Total Temperature  in Stn Frame)@Plane 0\n")
        f.write("    END\n")
        f.write("  END\n")
        f.write("END\n\n")

        # Mass flow averaged absolute flow angle
        f.write("LIBRARY:\n")
        f.write("  CEL:\n")
        f.write("    EXPRESSIONS:\n")
        f.write("      alphamfav"+str(i)+" = massFlowAve(atan(Velocity  in Stn Frame v / Velocity  in Stn Frame w) )@Plane "+str(i)+"\n")
        f.write("    END\n")
        f.write("  END\n")
        f.write("END\n\n")

        # Mass flux averaged mass flux
        f.write("LIBRARY:\n")
        f.write("  CEL:\n")
        f.write("    EXPRESSIONS:\n")
        f.write("      massfluxmfav"+str(i)+" = massFlowAve(Velocity  in Stn Frame w * Density)@Plane "+str(i)+"\n")
        f.write("    END\n")
        f.write("  END\n")
        f.write("END\n\n")

        # Mass flow averaged mass flow
        f.write("LIBRARY:\n")
        f.write("  CEL:\n")
        f.write("    EXPRESSIONS:\n")
        f.write("      massflow"+str(i)+" = -"+str(self.IN["N_BLADES"][0])+"*massFlow()@Plane "+str(i)+"\n")
        f.write("    END\n")
        f.write("  END\n")
        f.write("END\n\n")
        

        # Mass flow averaged Mach number
        f.write("LIBRARY:\n")
        f.write("  CEL:\n")
        f.write("    EXPRESSIONS:\n")
        f.write("      Machmfav"+str(i)+" = massFlowAve(Mach Number in Stn Frame)@Plane "+str(i)+"\n")
        f.write("    END\n")
        f.write("  END\n")
        f.write("END\n\n")
         
        # Mass flow averaged Entropy
        f.write("LIBRARY:\n")
        f.write("  CEL:\n")
        f.write("    EXPRESSIONS:\n")
        f.write("      deltaS"+str(i)+" = massFlowAve("+str(self.Cp)+"*ln(Total Temperature in Stn Frame/"+str(self.P.T_tin)+"[K])-"+str(self.P.R_gas)+"*ln(Total Pressure in Stn Frame/"+str(self.P.P_tin)+"[Pa]))@Plane "+str(i)+"\n")
        f.write("    END\n")
        f.write("  END\n")
        f.write("END\n\n")
 

        f.close()






    
    def create_plane(self,plane,i,Z):
        # Create the reference planes for mass-averaged values
        f = open(self.CFX_POST_HOME+"\\CFXsession_"+self.IN['MACHINE_NAME']+".cse", "a")

        f.write("PLANE:Plane "+str(i)+"\n")
        f.write("  Apply Instancing Transform = On\n")
        f.write("  Apply Texture = Off\n")
        f.write("  Blend Texture = On\n")
        f.write("  Bound Radius = 0.5 [m]\n")
        f.write("  Colour = 0.75, 0.75, 0.75\n")
        f.write("  Colour Map = Default Colour Map\n")
        f.write("  Colour Mode = Constant\n")
        f.write("  Colour Scale = Linear\n")
        f.write("  Colour Variable = Pressure\n")
        f.write("  Colour Variable Boundary Values = Hybrid\n")
        f.write("  Culling Mode = No Culling\n")
        f.write("  Direction 1 Bound = 1.0 [m]\n")
        f.write("  Direction 1 Orientation = 0 [degree]\n")
        f.write("  Direction 1 Points = 10\n")
        f.write("  Direction 2 Bound = 1.0 [m]\n")
        f.write("  Direction 2 Points = 10\n")
        f.write("  Domain List = /DOMAIN GROUP:All Domains\n")
        f.write("  Draw Faces = On\n")
        f.write("  Draw Lines = Off\n")
        f.write("  Instancing Transform = /DEFAULT INSTANCE TRANSFORM:Default Transform\n")
        f.write("  Invert Plane Bound = Off\n")
        f.write("  Lighting = On\n")
        f.write("  Line Colour = 0, 0, 0\n")
        f.write("  Line Colour Mode = Default\n")
        f.write("  Line Width = 1\n")
        f.write("  Max = 0.0 [Pa]\n")
        f.write("  Min = 0.0 [Pa]\n")
        f.write("  Normal = 1 , 0 , 0\n")
        f.write("  Option = "+plane+" Plane\n")
        f.write("  Plane Bound = None\n")
        f.write("  Plane Type = Slice\n")
        f.write("  Point = 0 [m], 0 [m], 0 [m]\n")
        f.write("  Point 1 = 0 [m], 0 [m], 0 [m]\n")
        f.write("  Point 2 = 1 [m], 0 [m], 0 [m]\n")
        f.write("  Point 3 = 0 [m], 1 [m], 0 [m]\n")
        f.write("  Range = Global\n")
        f.write("  Render Edge Angle = 0 [degree]\n")
        f.write("  Specular Lighting = On\n")
        f.write("  Surface Drawing = Smooth Shading\n")
        f.write("  Texture Angle = 0\n")
        f.write("  Texture Direction = 0 , 1 , 0\n")
        f.write("  Texture File =\n")
        f.write("  Texture Material = Metal\n")
        f.write("  Texture Position = 0 , 0\n")
        f.write("  Texture Scale = 1\n")
        f.write("  Texture Type = Predefined\n")
        f.write("  Tile Texture = Off\n")
        f.write("  Transform Texture = Off\n")
        f.write("  Transparency = 0.0\n")
        f.write("  X = 0.0 [m]\n")
        f.write("  Y = 0.0 [m]\n")
        f.write("  Z = "+str(Z)+" [m]\n")
        f.write("  OBJECT VIEW TRANSFORM:\n")
        f.write("    Apply Reflection = Off\n")
        f.write("    Apply Rotation = Off\n")
        f.write("    Apply Scale = Off\n")
        f.write("    Apply Translation = Off\n")
        f.write("    Principal Axis = Z\n")
        f.write("    Reflection Plane Option = XY Plane\n")
        f.write("    Rotation Angle = 0.0 [degree]\n")
        f.write("    Rotation Axis From = 0 [m], 0 [m], 0 [m]\n")
        f.write("    Rotation Axis To = 0 [m], 0 [m], 0 [m]\n")
        f.write("    Rotation Axis Type = Principal Axis\n")
        f.write("    Scale Vector = 1 , 1 , 1\n")
        f.write("    Translation Vector = 0 [m], 0 [m], 0 [m]\n")
        f.write("    X = 0.0 [m]\n")
        f.write("    Y = 0.0 [m]\n")
        f.write("    Z = 0.0 [m]\n")
        f.write("  END\n")
        f.write("END\n\n")

        f.close()
       

    def header(self):

         # Write the initial paragraph for the replay file
        f = open(self.CFX_POST_HOME+"\\CFXsession_"+self.IN['MACHINE_NAME']+".cse", "w")

        # set the settings
        f.write("COMMAND FILE:\n")
        f.write("  CFX Post Version = 19.5\n")
        f.write("END\n\n")

        f.write("DATA READER:\n")
        f.write("  Clear All Objects = false\n")
        f.write("  Append Results = false\n")
        f.write("  Edit Case Names = false\n")
        f.write("  Multi Configuration File Load Option = Last Case\n")
        f.write("  Open in New View = true\n")
        f.write("  Keep Camera Position = true\n")
        f.write("  Load Particle Tracks = true\n")
        f.write("  Multi Configuration File Load Option = Last Case\n")
        f.write("  Construct Variables From Fourier Coefficients = true\n")
        f.write("  Open to Compare = false\n")
        f.write("  Files to Compare =\n")
        f.write("END\n\n")

        f.write("DATA READER:\n")
        f.write("  Domains to Load=\n")
        f.write("END\n\n")
        # filename = 'C:/Users/GMrx1/Desktop/Bestfit.res'
        # C:\Users\GMrx1\Desktop\CFX_reference\notip_results
        filename = 'C:/Users/GMrx1/Desktop/reference_001.res'
        f.write(">load filename="+filename+", force_reload=true\n\n")
        print("CFX POST is processing the file: "+filename)
        f.write("VIEW:Turbo Initialisation View\n")
        f.write("  Object Visibility List = /WIREFRAME:Wireframe, /COMPHIGHLIGHT:Component 1\n")
        f.write("  Is A Figure = false\n")
        f.write("END\n\n")

        f.write("VIEWPORT MANAGER:\n")
        f.write("  Viewport Layout = No Split\n")
        f.write("  Maximized Viewport = -1\n")
        f.write("END\n\n")

        f.write(">setViewportView cmd=set, view=/VIEW:Turbo Initialisation View, viewport=1\n\n")
        
        f.write("VIEWPORT MANAGER:\n")
        f.write("  Synchronize Visibility = false\n")
        f.write("END\n\n")

        f.write("> turbo init\n\n")

        f.write("> update\n\n")

        f.close()





       
