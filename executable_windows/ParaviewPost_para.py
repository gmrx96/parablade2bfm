# The classes in this script are for postprocessing purposes using Paraview.
import numpy as np
# from executable_windows.Parablade2BFM import P
from paraview.simple import *
import matplotlib.pyplot as plt
import os
import sys
import csv
# from vtk.util import numpy_support
import vtk



# Defining the path to the Meangen2BFM installation folder  (here on windows I must fully specify it!!!)
HOME = "C:\\Users\\Gebruiker\\Desktop\\github\\Parablade2BFM"        
# Appending the executables folder to the path in order for the other scripts to be accessed
sys.path.append(HOME + "\\executables")



inFile = sys.argv[-1]   # Meangen2BFM configuration file
print("inFile is ",inFile)
fileName = sys.argv[1]
# inFile = HOME + '\\templates\\Para_fan_stage.cfg'
from SU2Writer_para import ReadUserInput
from ParabladeInput import ParabladeInput
from StagenReader import StagenReader

IN = ReadUserInput(inFile)  # Transforming the configuration file into a class containing all the design variables and
                            # simulation settings.
elements_id = str(int(IN['AXIAL_POINTS'][0]))+"x"+str(int(IN['RADIAL_POINTS'][0]))+"x"+str(int(IN['TANGENTIAL_POINTS'][0]))


class extractData_BFM:
    # This class is used for the extraction and processing of the flow field as output by SU2. The class writes user-
    # defined data in axial direction, averaged by the momentum in axial direction. Additionally, flow data trends in
    # radial direction are extracted at the inlet, outlet and row gaps in the machine.

    # fileName= "C:\\Users\\GMrx1\\Desktop\\RESULTS\\flow20x3.vtu"
    # fileName= "C:\\Users\\GMrx1\\Desktop\\github\\Parablade2BFM\\SU2_symulations\\BFM_80x40\\flow80x40.vtu"
    fileName=fileName
    print("fileName is ",fileName)
    axial_bounds = []   # Domain bounds in axial direction
    radial_bounds = []  # Domain bounds in radial direction
    output_datasets = []    # List with the names of output flow data sets
    output_data = type('', (), {})()    # Output data object
    fluid_parameters = type('', (), {})()   # Simulation fluid parameter object
    calculators = type('', (), {})()    # Paraview calculator object
    h = 1e-5    # Domain margin at the inlet and outlet for evaluation of inlet and outlet properties
    area=[]

    def __init__(self,IN):
        self.IN = IN    # Storing the Maengen2BFM input parameters
        self.fluid_parameters.gamma = self.IN["gamma"][0]   # Storing the specific heat ratio
        self.fluid_parameters.R_gas = self.IN["R_gas"][0]   # Storing the fluid gas constant
        self.fluid_parameters.C_p = self.IN["gamma"][0]*self.IN["R_gas"][0]/(self.IN["gamma"][0] - 1)   # Storing the
        #                                                                                  # specific enthalpy
    # def __init__(self):
    #     self.fluid_parameters.gamma = 1.4
    #     self.fluid_parameters.R_gas = 287
    #     self.fluid_parameters.C_p = 1.4*(287/(1.4-1))
        self.BFM_model = str(IN['BFM_MODEL'])
#         # Creating folder for output data
        if not os.path.isdir(HOME+"\\PARAVIEW_Post"):
            os.system("mkdir "+HOME+"\\PARAVIEW_Post")
        if not os.path.isdir(HOME+"\\PARAVIEW_Post\\plots"):
            os.system("mkdir "+HOME+"\\PARAVIEW_Post\\plots")
        if not os.path.isdir(HOME+"\\PARAVIEW_Post\\plots\\"+elements_id):
            os.system("mkdir "+HOME+"\\PARAVIEW_Post\\plots\\"+elements_id)
        if not os.path.isdir(HOME+"\\PARAVIEW_Post\\vtk_files"):
            os.system("mkdir "+HOME+"\\PARAVIEW_Post\\vtk_files")

        if IN["WEDGE"][0]==360:
            if not os.path.isdir(HOME+"\\PARAVIEW_Post\\vtk_files\\fullannulus"):
                os.system("mkdir "+HOME+"\\PARAVIEW_Post\\vtk_files\\fullannulus")
            if not os.path.isdir(HOME+"\\PARAVIEW_Post\\vtk_files\\fullannulus\\"+elements_id):
                os.system("mkdir "+HOME+"\\PARAVIEW_Post\\vtk_files\\fullannulus\\"+elements_id)
            self.vtk_folder = HOME+"\\PARAVIEW_Post\\vtk_files\\fullannulus\\"+elements_id
        else:
            if not os.path.isdir(HOME+"\\PARAVIEW_Post\\vtk_files\\periodic"):
                os.system("mkdir "+HOME+"\\PARAVIEW_Post\\vtk_files\\periodic")
            if not os.path.isdir(HOME+"\\PARAVIEW_Post\\vtk_files\\periodic\\"+elements_id):
                os.system("mkdir "+HOME+"\\PARAVIEW_Post\\vtk_files\\periodic\\"+elements_id)
            self.vtk_folder = HOME+"\\PARAVIEW_Post\\vtk_files\\periodic\\"+elements_id

#         # Reading SU2 output vtk file
        self.flow_3D_BFM = XMLUnstructuredGridReader(registrationName='flow.vtu', FileName=[self.fileName])
        self.flow_3D_BFM.PointArrayStatus = ['Density', 'Momentum', 'Energy', 'Turb_Kin_Energy', 'Omega', 'Pressure', 'Temperature', 'Mach', 'Pressure_Coefficient', 'Laminar_Viscosity', 'Skin_Friction_Coefficient', 'Heat_Flux', 'Y_Plus', 'Eddy_Viscosity', 'n', 'b', 'blockage_gradient', 'body_force_factor', 'rotation_factor', 'blade_count', 'W', 'BF']
        
        
   

        #--------------------------------- Extract the FA or periodic data and plot it ---------------------------------------#
        # Storing leading edge and trailing edge coordinates
        self.get_row_geom()

        # Obtaining flow domain bounds
        self.get_domain_bounds()

        # Defining flow data calculators
        self.set_functions()

        # Collecting axial and radial flow data
        self.station_slices=[]
        self.collect_data()

        # Computing and writing relevant performance objectives
        self.compute_objectives()

        # Plot the informations
        self.plot_results()
        


        # if IN["WEDGE"][0]==360:
 
        #     self.get_coords_FA()
        #     self.h_span = np.linspace(self.blade_bounds[0]*1.01,self.blade_bounds[1]*0.99,11)
        #     # self.radial_bounds = self.blade_bounds

        #     self.rad_slice = []
        #     self.groups=[]
        #     self.down_roll=[]
        #     self.up_roll=[]
        #     # self.h_span = np.linspace(self.radial_bounds[0]*1.01,self.radial_bounds[1]*0.99,11)
        #     for i in range(len(self.h_span)):
        #         self.create_FA_span_slice(i)
        
        #         self.unroll_FA(i)
            
        #         self.recreate_FA_span_vtk(i)

        # else:

            


        #     self.get_coords_periodic()

        #     self.h_span = np.linspace(self.blade_bounds[0]*1.01,self.blade_bounds[1]*0.99,11)

        #     self.rad_slice = []
        #     self.groups=[]
        #     self.down_roll=[]
        #     self.up_roll=[]
        #     # self.h_span = np.linspace(self.radial_bounds[0]*1.01,self.radial_bounds[1]*0.99,11)
            
            
        #     self.create_periodic_side_slice()

        #     for i in range(len(self.h_span)):
        #         self.create_periodic_span_slice(i)

        #         self.recreate_periodic_span_vtk(i)



        # #-------------------------------------------------------------------------------



        # for i in range(len(self.station_slices)):
        #     self.station_slices_vtksave(i)


        



    






    def create_periodic_side_slice(self):
        # right_side = Slice(registrationName="Rightside",Input=self.flow_3D_BFM)
        right_side = Slice(registrationName="Rightside",Input=self.flow_data)
        right_side.SliceType = "Plane"
        right_side.SliceType.Origin = [-0.40004816560412537, 0.09542, 0.0075]
        right_side.SliceType.Normal = [0, -0.007509786617188213, 0.09542088628297829]
        

        right_side_vtk = PointDatatoCellData(registrationName='right_side_vtk', Input=right_side)
        right_side_vtk.PointDataArraytoprocess = ['BF', 'Density', 'Energy', 'Mach', 'Momentum', 'Normals', 'Pressure', 'Pressure_Coefficient', 'Residual_Density', 'Residual_Energy', 'Residual_Momentum', 'Temperature', 'W', 'b', 'blade_count', 'blockage_gradient', 'body_force_factor', 'n', 'rotation_factor']
        
        SaveData( self.vtk_folder+"\\Rightside.vtk", proxy=right_side_vtk, CellDataArrays=['BF', 'Density', 'Energy', 'Mach', 'Momentum', 'Normals', 'Pressure', 'Pressure_Coefficient', 'Residual_Density', 'Residual_Energy', 'Residual_Momentum', 'Temperature', 'W', 'b', 'blade_count', 'blockage_gradient', 'body_force_factor', 'n', 'rotation_factor'])


        # left_slice = Slice(registrationName="Leftside",Input=self.flow_3D_BFM)
        left_slice = Slice(registrationName="Leftside",Input=self.flow_data)
        left_slice.SliceType = "Plane"
        left_slice.SliceType.Origin = [-0.40004816560412537, 0.095683, 0.00250555138]
        left_slice.SliceType.Normal = [0, -0.002505551386459919, 0.09568314713843072]

        pointDatatoCellData1 = PointDatatoCellData(registrationName='PointDatatoCellData1', Input=left_slice)
        pointDatatoCellData1.PointDataArraytoprocess = ['BF', 'Density', 'Energy', 'Mach', 'Momentum', 'Pressure', 'Pressure_Coefficient', 'Residual_Density', 'Residual_Energy', 'Residual_Momentum', 'Temperature', 'W', 'b', 'blade_count', 'blockage_gradient', 'body_force_factor', 'n', 'rotation_factor']


        # pointDatatoCellData1 = PointDatatoCellData(registrationName='left_side_vtk', Input=left_side)
        # pointDatatoCellData1.PointDataArraytoprocess = ['BF', 'Density', 'Energy', 'Mach', 'Momentum', 'Normals', 'Pressure', 'Pressure_Coefficient', 'Residual_Density', 'Residual_Energy', 'Residual_Momentum', 'Temperature', 'W', 'b', 'blade_count', 'blockage_gradient', 'body_force_factor', 'n', 'rotation_factor']
        
        SaveData(self.vtk_folder+"\\ProvaLeftside.vtk", proxy=pointDatatoCellData1, CellDataArrays=['BF', 'Density', 'Energy', 'Mach', 'Momentum', 'Pressure', 'Pressure_Coefficient', 'Residual_Density', 'Residual_Energy', 'Residual_Momentum', 'Temperature', 'W', 'b', 'blade_count', 'blockage_gradient', 'body_force_factor', 'n', 'rotation_factor'])
        # SaveData( self.vtk_folder+"\\Leftside.vtk", proxy=left_side_vtk, CellDataArrays=['BF', 'Density', 'Energy', 'Mach', 'Momentum', 'Normals', 'Pressure', 'Pressure_Coefficient', 'Residual_Density', 'Residual_Energy', 'Residual_Momentum', 'Temperature', 'W', 'b', 'blade_count', 'blockage_gradient', 'body_force_factor', 'n', 'rotation_factor'])

    



    def station_slices_vtksave(self,i):
        station = PointDatatoCellData(registrationName='pointDatatoCellDataCylUnroll', Input=self.station_slices[i])
        station.PointDataArraytoprocess = ['BF', 'Density', 'Energy', 'Mach', 'Momentum', 'Normals', 'Pressure', 'Pressure_Coefficient', 'Residual_Density', 'Residual_Energy', 'Residual_Momentum', 'Temperature', 'W', 'b', 'blade_count', 'blockage_gradient', 'body_force_factor', 'n', 'rotation_factor']
        
        if IN["WEDGE"][0]==360:
            SaveData( self.vtk_folder+"\\Circular_"+self.station_names[i]+".vtk", proxy=station, CellDataArrays=['BF', 'Density', 'Energy', 'Mach', 'Momentum', 'Normals', 'Pressure', 'Pressure_Coefficient', 'Residual_Density', 'Residual_Energy', 'Residual_Momentum', 'Temperature', 'W', 'b', 'blade_count', 'blockage_gradient', 'body_force_factor', 'n', 'rotation_factor'])
        else:
            turned = Calculator(registrationName='Up_view', Input=station)
            turned.CoordinateResults = -1
            turned.Function = '0*iHat+(cos(1.52)*coordsY -sin(1.52)*coordsZ)*jHat+(+sin(1.52)*coordsY+cos(1.52)*coordsZ)*kHat'
            SaveData( self.vtk_folder+"\\Wedge"+self.station_names[i]+".vtk", proxy=turned, CellDataArrays=['BF', 'Density', 'Energy', 'Mach', 'Momentum', 'Normals', 'Pressure', 'Pressure_Coefficient', 'Residual_Density', 'Residual_Energy', 'Residual_Momentum', 'Temperature', 'W', 'b', 'blade_count', 'blockage_gradient', 'body_force_factor', 'n', 'rotation_factor'])


    def recreate_periodic_span_vtk(self,i):
        unrolled = Calculator(registrationName='Up_view', Input=self.rad_slice[i])
        unrolled.CoordinateResults = -1
        unrolled.Function = 'coordsX*iHat+ ((atan(coordsY/coordsZ)+(1-(coordsZ/abs(coordsZ)))*1.570796326)*(coordsZ^2+coordsY^2)^(0.5))*jHat+0*kHat'

        pointDatatoCellDataCylUnroll = PointDatatoCellData(registrationName='pointDatatoCellDataCylUnroll', Input=unrolled)
        pointDatatoCellDataCylUnroll.PointDataArraytoprocess = ['BF', 'Density', 'Energy', 'Mach','Mrel', 'Momentum', 'Normals', 'Pressure', 'Pressure_Coefficient', 'Residual_Density', 'Residual_Energy', 'Residual_Momentum', 'Temperature', 'W', 'b', 'blade_count', 'blockage_gradient', 'body_force_factor', 'n', 'rotation_factor']
        
        SaveData(self.vtk_folder+"\\Span_"+str(i)+".vtk", proxy=pointDatatoCellDataCylUnroll, CellDataArrays=['BF', 'Density', 'Energy', 'Mach','Mrel', 'Momentum', 'Normals', 'Pressure', 'Pressure_Coefficient', 'Residual_Density', 'Residual_Energy', 'Residual_Momentum', 'Temperature', 'W', 'b', 'blade_count', 'blockage_gradient', 'body_force_factor', 'n', 'rotation_factor'])


    def recreate_FA_span_vtk(self,i):

        extractCylUnroll = ExtractSurface(registrationName='ExtractSurfaceCylUnroll'+str(i), Input=self.groups[i])
        generateCylUnrollNormals = GenerateSurfaceNormals(registrationName='generateCylUnrollNormals', Input=extractCylUnroll)
        pointDatatoCellDataCylUnroll = PointDatatoCellData(registrationName='pointDatatoCellDataCylUnroll', Input=generateCylUnrollNormals)
        pointDatatoCellDataCylUnroll.PointDataArraytoprocess = ['BF', 'Density', 'Energy', 'Mach','Mrel', 'Momentum', 'Normals', 'Pressure', 'Pressure_Coefficient', 'Residual_Density', 'Residual_Energy', 'Residual_Momentum', 'Temperature', 'W', 'b', 'blade_count', 'blockage_gradient', 'body_force_factor', 'n', 'rotation_factor']
        
        SaveData(self.vtk_folder+"\\Span_"+str(i)+".vtk", proxy=pointDatatoCellDataCylUnroll, CellDataArrays=['BF', 'Density', 'Energy', 'Mach','Mrel', 'Momentum', 'Normals', 'Pressure', 'Pressure_Coefficient', 'Residual_Density', 'Residual_Energy', 'Residual_Momentum', 'Temperature', 'W', 'b', 'blade_count', 'blockage_gradient', 'body_force_factor', 'n', 'rotation_factor'])





    def unroll_FA(self,i):
        up_clip = Clip(registrationName='up_clip', Input=self.rad_slice[i])
        up_clip.ClipType = 'Plane'
        up_clip.ClipType.Origin = [0.0, 0.0, 0.0]
        up_clip.ClipType.Normal = [0.0, -1.0, 0.0]

        up = Calculator(registrationName='Up_view', Input=up_clip)
        up.CoordinateResults = -1
        up.Function = 'coordsX*iHat+ ((atan(coordsY/coordsZ)+(1-(coordsZ/abs(coordsZ)))*1.570796326)*(coordsZ^2+coordsY^2)^(0.5))*jHat+0*kHat'
        
        
        down_clip = Clip(registrationName='down_clip', Input=self.rad_slice[i])
        down_clip.ClipType = 'Plane'
        down_clip.ClipType.Origin = [0.0, 0.0, 0.0]
        down_clip.ClipType.Normal = [0.0, +1.0, 0.0]

        down = Calculator(registrationName='Down_view', Input=down_clip)
        down.CoordinateResults = 1
        down.Function = 'coordsX*iHat+ ((atan(coordsY/coordsZ)-(1-(coordsZ/abs(coordsZ)))*1.570796326)*(coordsZ^2+coordsY^2)^(0.5))*jHat+0*kHat'
        
        self.down_roll.append(down)
        self.up_roll.append(up)
        groupDatasets1 = GroupDatasets(registrationName='unrolled_domain'+str(i), Input=[down, up])
        groupDatasets1.BlockNames = ['Down_view', 'Up_view']

        mergeBlocks1 = MergeBlocks(registrationName='UnrolledCylinder_'+str(i), Input=groupDatasets1)
        self.groups.append(mergeBlocks1)
        
    
    def create_periodic_span_slice(self,i):
        # rad_slice = Slice(registrationName="Span_"+str(i),Input=self.flow_3D_BFM)
        rad_slice = Slice(registrationName="Span_"+str(i),Input=self.flow_data)
        rad_slice.SliceType = "Cylinder"
        rad_slice.SliceType.Axis = [1,0,0]
        rad_slice.SliceType.Radius = self.h_span[i]
        rad_slice.SliceType.Center = [0.0, 0.0, 0.0]
        self.rad_slice.append(rad_slice)


    def create_FA_span_slice(self,i):
        # rad_slice = Slice(registrationName="Span_"+str(i),Input=self.flow_3D_BFM)
        rad_slice = Slice(registrationName="Span_"+str(i),Input=self.flow_data)
        rad_slice.SliceType = "Cylinder"
        rad_slice.SliceType.Axis = [1,0,0]
        rad_slice.SliceType.Radius = self.h_span[i]
        rad_slice.SliceType.Center = [0.0, 0.0, 0.0]
        self.rad_slice.append(rad_slice)




    def get_coords_periodic(self):

        # left_side = Slice(registrationName="Leftside",Input=self.flow_3D_BFM)
        left_side = Slice(registrationName="Leftside",Input=self.flow_data)
        left_side.SliceType = "Plane"
        left_side.SliceType.Normal = [0, -0.002505551386459919, 0.09568314713843072]
        left_side.SliceType.Origin = [-0.40004816560412537, 0.095683, 0.00250555138]

        clip1 = Clip(registrationName='Clip1', Input=left_side)
        clip1.ClipType = 'Plane'
        clip1.ClipType.Origin = [self.X_LE[0]*1.1, 0.0, 0.0]   # This way I get the spanwise limits according to LE height
        # clip1.ClipType.Origin = [self.X_TE[0]*1.1, 0.0, 0.0]   # This way I get the spanwise limits according to TE height
        clip1.ClipType.Normal = [-1.0, 0.0, 0.0]

        coords2 = Calculator(registrationName='Calculator2',Input=clip1)
        coords2.ResultArrayName = 'coordinates'
        coords2.Function = 'coords'

        boundsfunction2 = ProgrammableFilter(registrationName='newfilter',Input=coords2)
        boundsfunction2.OutputDataSetType = 'vtkTable'
        boundsfunction2.Script = """import numpy as np 
coords = inputs[0].PointData["coordinates"]
    
x, y, z = coords[:, 0], coords[:, 1], coords[:, 2]

x_min, y_min, z_min = min(x), min(y), min(z)
x_max, y_max, z_max = max(x), max(y), max(z)

x_bounds = np.array([x_min, x_max])
y_bounds = np.array([y_min, y_max])
z_bounds = np.array([z_min, z_max])
output.RowData.append(x_bounds, "x_bounds")
output.RowData.append(y_bounds, "y_bounds")
output.RowData.append(z_bounds, "z_bounds")"""

        
        
        bounds_data = paraview.servermanager.Fetch(boundsfunction2)
        self.blade_bounds = [bounds_data.GetRowData().GetArray("y_bounds").GetValue(0),
                        bounds_data.GetRowData().GetArray("y_bounds").GetValue(1)]
                        
        Delete(boundsfunction2)
        Delete(coords2)
        Delete(clip1)
        Delete(left_side)


    def get_coords_FA(self):
        # slice1 = Slice(registrationName='Slice1', Input=self.flow_3D_BFM)
        slice1 = Slice(registrationName='Slice1', Input=self.flow_data)
        slice1.SliceType = 'Plane'
        slice1.SliceType.Origin = [0.0, 0.0, 0.0]
        slice1.SliceType.Normal = [0.0, 0.0, 1.0]
        
        
        clip1 = Clip(registrationName='Clip1', Input=slice1)
        clip1.ClipType = 'Plane'
        clip1.ClipType.Origin = [0.0, 0.0, 0.0]
        clip1.ClipType.Normal = [0.0, -1.0, 0.0]

        clip2 = Clip(registrationName='Clip2', Input=clip1)
        clip2.ClipType = 'Plane'
        clip2.ClipType.Origin = [self.axial_bounds[-1]*0.95, 0.0, 0.0]
        clip2.ClipType.Normal = [-1.0, 0.0, 0.0]

        coords2 = Calculator(registrationName='Calculator2',Input=clip2)
        coords2.ResultArrayName = 'coordinates'
        coords2.Function = 'coords'

        boundsfunction2 = ProgrammableFilter(registrationName='newfilter',Input=coords2)
        boundsfunction2.OutputDataSetType = 'vtkTable'
        boundsfunction2.Script = """import numpy as np 
coords = inputs[0].PointData["coordinates"]
    
x, y, z = coords[:, 0], coords[:, 1], coords[:, 2]

x_min, y_min, z_min = min(x), min(y), min(z)
x_max, y_max, z_max = max(x), max(y), max(z)

x_bounds = np.array([x_min, x_max])
y_bounds = np.array([y_min, y_max])
z_bounds = np.array([z_min, z_max])
output.RowData.append(x_bounds, "x_bounds")
output.RowData.append(y_bounds, "y_bounds")
output.RowData.append(z_bounds, "z_bounds")"""

        
        
        bounds_data = paraview.servermanager.Fetch(boundsfunction2)
        self.blade_bounds = [bounds_data.GetRowData().GetArray("y_bounds").GetValue(0),
                        bounds_data.GetRowData().GetArray("y_bounds").GetValue(1)]
                        
        Delete(boundsfunction2)
        Delete(coords2)
        Delete(clip1)
        Delete(clip2)
        Delete(slice1)
       



    def plot_results(self):
        
        plt.figure(0)
        plt.plot(self.output_data.axial_data.X, self.output_data.axial_data.mdot)
        plt.title('Axial mass flow', fontsize = 16)
        plt.grid()
        plt.xlabel('X [m]',fontsize = 12)
        plt.ylabel('mdot',fontsize = 14)
        plt.savefig(HOME+"\\PARAVIEW_Post\\plots\\"+elements_id+"\\mdot.png")

        plt.figure(1)
        plt.plot(self.output_data.axial_data.X, self.output_data.axial_data.weighted_P)
        plt.title('Axial pressure', fontsize = 16)
        plt.grid()
        plt.xlabel('X [m]',fontsize = 12)
        plt.ylabel('wp',fontsize = 14)
        plt.savefig(HOME+"\\PARAVIEW_Post\\plots\\"+elements_id+"\\wp.png")

        plt.figure(2)
        plt.plot(self.output_data.axial_data.X, self.output_data.axial_data.weighted_T)
        plt.title('Axial temperature', fontsize = 16)
        plt.grid()
        plt.xlabel('X [m]',fontsize = 12)
        plt.ylabel('wT',fontsize = 14)
        plt.savefig(HOME+"\\PARAVIEW_Post\\plots\\"+elements_id+"\\wT.png")

        plt.figure(3)
        plt.plot(self.output_data.axial_data.X, self.output_data.axial_data.weighted_Pt)
        plt.title('Axial total pressure', fontsize = 16)
        plt.grid()
        plt.xlabel('X [m]',fontsize = 12)
        plt.ylabel('wPt',fontsize = 14)
        plt.savefig(HOME+"\\PARAVIEW_Post\\plots\\"+elements_id+"\\wPt.png")

        plt.figure(4)
        plt.plot(self.output_data.axial_data.X, self.output_data.axial_data.weighted_Tt)
        plt.title('Axial total temperature', fontsize = 16)
        plt.grid()
        plt.xlabel('X [m]',fontsize = 12)
        plt.ylabel('wTt',fontsize = 14)
        plt.savefig(HOME+"\\PARAVIEW_Post\\plots\\"+elements_id+"\\wTt.png")

        plt.figure(5)
        plt.plot(self.output_data.axial_data.X, self.output_data.axial_data.weighted_Alpha)
        plt.title('Axial angle', fontsize = 16)
        plt.grid()
        plt.xlabel('X [m]',fontsize = 12)
        plt.ylabel('alpha',fontsize = 14)
        plt.savefig(HOME+"\\PARAVIEW_Post\\plots\\"+elements_id+"\\alpha.png")
        
        






    def collect_data(self):
        # In this function, the axial and radial flow data are extracted and written to output files

        # Listing all calculators, alongside the axial coordinate and mass flow rate
        # self.output_datasets = ['X', 'mdot','area'] + list(self.calculators.__dict__.keys())
        

        
        self.output_datasets = ['X', 'mdot'] + list(self.calculators.__dict__.keys()) 

         # Extract flow data in radial direction at the inlet, outlet and row gaps
        self.extract_radial_data()
        # Writing radial flow data to output file
        self.write_radial_data()

        self.list_sigma = ['Pt_sigma','Tt_sigma','P_sigma','T_sigma','Mrel_sigma','QDM_sigma','Density_sigma']
        # self.list_sigma = ['P_sigma']
        # self.list_sigma = []
        # for i in list(self.calculators.__dict__.keys()) :
        #     self.list_sigma.append(i+'_sigma')
        self.list_calc = []

        for i in list(self.calculators.__dict__.keys()) :
            if 'weighted' in i:
                self.list_calc.append(i)
            # self.list_sigma.append(i+'_sigma'

        self.output_datasets = ['X', 'mdot','area'] + self.list_calc #+ self.list_sigma

        # Extract mass flux-averaged flow data in axial direction
        self.extract_axial_data()
        # Write axial data to output file
        self.write_axial_data()

        # # Extract flow data in radial direction at the inlet, outlet and row gaps
        # self.extract_radial_data()
        # # Writing radial flow data to output file
        # self.write_radial_data()














    def compute_objectives(self):
        # This function computes some relevant machine objectives which can be used for design optimization purposes.
        # Objectives such as mass flow rate, total-to-total efficiency, power and outlet flow angle are calculated and
        # written to a file.

        # Extracting the mass flow rate
        mdot = self.output_data.axial_data.mdot[0]

        # Inlet and outlet stagnation temperature
        Tt_in = self.output_data.axial_data.weighted_Tt[0]
        Tt_out = self.output_data.axial_data.weighted_Tt[-1]
        # Tt_out = self.output_data.axial_data.wTt[90]

        # Inlet and outlet stagnation pressure
        Pt_in = self.output_data.axial_data.weighted_Pt[0]
        Pt_out = self.output_data.axial_data.weighted_Pt[-1]
        # Pt_out = self.output_data.axial_data.wPt[90]

        gamma = self.fluid_parameters.gamma

        # Calculating the isentropic outlet stagnation temperature
        Tt_out_s = Tt_in * (Pt_out/Pt_in) ** ((gamma - 1)/gamma)

        # # Total-to-total efficiency is calculated depending on the machine type
        # if self.IN["TYPE"] == 'T':
        #     eta_tt = (Tt_out - Tt_in)/(Tt_out_s - Tt_in)
        # else:
        #     eta_tt = (Tt_out_s - Tt_in) / (Tt_out - Tt_in)
        eta_tt = (Tt_out_s - Tt_in) / (Tt_out - Tt_in)

        # Calculating the machine input power
        power_in = self.fluid_parameters.C_p * mdot * (Tt_out - Tt_in)

        # Outlet absolute flow angle
        angle_out = self.output_data.axial_data.weighted_Alpha[-1]

        # Opening the objective output file and writing the calculated objectives
        if not os.path.isdir(HOME+"\\PARAVIEW_Post\\objectives"):
            os.system("mkdir "+HOME+"\\PARAVIEW_Post\\objectives")
        txt_out = self.fileName.split("\\")[-1]
        txt_out = txt_out.replace(".vtu","_Machine_objectives.txt")
        txt_out = txt_out.replace("flow",self.BFM_model)
        txt_out = HOME+"\\PARAVIEW_Post\\objectives\\"+txt_out
        self.objective_file = open(txt_out, "w+")
        # self.objective_file = open("C:\\Users\\GMrx1\\Desktop\\github\\Parablade2BFM\\PARAVIEW_POST\\"+self.IN['MACHINE_NAME']+"_Machine_objectives.txt", "w+")
        self.objective_file.write("total-to-total_efficiency = "+str(eta_tt)+"\n")
        self.objective_file.write("power = "+str(power_in)+"\n")
        self.objective_file.write("mass_flow_rate = " +str(mdot)+"\n")
        self.objective_file.write("outlet_flow_angle = " + str(angle_out) + "\n")
        self.objective_file.close()
















    def write_axial_data(self):
        # This function takes the mass-flux-averaged data in axial direction and writes them to an output file.

        # Opening axial data output file
        if not os.path.isdir(HOME+"\\PARAVIEW_Post\\axial_data"):
            os.system("mkdir "+HOME+"\\PARAVIEW_Post\\axial_data")
        csv_out = self.fileName.split("\\")[-1]
        csv_out = csv_out.replace(".vtu","_axial_data.csv")
        csv_out = csv_out.replace("flow",self.BFM_model)
        csv_out = HOME+"\\PARAVIEW_Post\\axial_data\\"+csv_out
        self.output_file = open(csv_out, "w+", newline='')
        writer=csv.writer( self.output_file)
        allines=[]
        allines.append(self.output_datasets)
        
        axial_range = self.output_data.axial_data.X

        for i in range(len(axial_range)):
            section_data = []
            for j in range(len(self.output_datasets)):
                section_data.append(getattr(self.output_data.axial_data, self.output_datasets[j])[i])
            allines.append(section_data)

        writer.writerows(allines)
        
        self.output_file.close()

        
    def write_radial_data(self):
        station_names = list(self.output_data.radial_data.__dict__.keys())
        if not os.path.isdir(HOME+"\\PARAVIEW_Post\\radial_data"):
            os.system("mkdir "+HOME+"\\PARAVIEW_Post\\radial_data")
        for name in station_names:
            csv_out = self.fileName.split("\\")[-1]
            csv_out = csv_out.replace(".vtu", "_"+name +"_radial_data.csv")
            csv_out = csv_out.replace("flow",self.BFM_model)
            csv_out = HOME+"\\PARAVIEW_Post\\radial_data\\"+csv_out
            self.output_file = open(csv_out, "w+", newline='')
            # print("self.output_file is ",self.output_file)
            # rad_file = open("Performance_Data\\"+name +".txt", "w+")
            # rad_file = open("C:\\Users\\GMrx1\\Desktop\\github\\Parablade2BFM\\PARAVIEW_POST\\radial_data.txt", "w+")

            writer=csv.writer( self.output_file)
            allines=[]

            variable_names = list(getattr(self.output_data.radial_data, name).__dict__.keys())
           
            rad_object = getattr(self.output_data.radial_data, name)
            allines.append(variable_names)
           
            for i in range(len(rad_object.X)):
                rad_data = []
                for j in range(len(variable_names)):
                    # rad_data.append(getattr(self.output_data.radial_data, variable_names[j])[i])
                    rad_data.append(getattr(rad_object, variable_names[j])[i])
                allines.append(rad_data)
                # # rad_data = []
                # for var_name in variable_names:
                #     allines.append(getattr(rad_object, var_name)[i])
                # for j in range(len(self.output_datasets)):
                #     section_data.append(getattr(self.output_data.axial_data, self.output_datasets[j])[i])
                # allines.append(section_data)
            
            allines[0][0]="R"
            writer.writerows(allines)

        
            self.output_file.close()



            # variable_names = list(getattr(self.output_data.radial_data, name).__dict__.keys())
            # rad_object = getattr(self.output_data.radial_data, name)
            # rad_file.write("\t".join(variable_names)+"\n")
            # n = len(rad_object.X)
            # for i in range(n):
            #     rad_data = []
                # for var_name in variable_names:
                #     rad_data.append(getattr(rad_object, var_name)[i])

                # rad_file.write("\t".join([str(s) for s in rad_data])+"\n")
            # rad_file.close()



    def extract_radial_data(self):
        radial_range = np.linspace(self.radial_bounds[0], self.radial_bounds[-1], 100)
        # print(radial_range)
        stations = [self.axial_bounds[0] + self.h]
        station_names = ['Inlet']
        # print("X_LE is ",np.shape(self.X_LE))

        stations.append(self.X_LE[0]-self.h*5)
        # stations.append(self.X_LE[0]-self.h)
        station_names.append('LE')
        
        stations.append(self.X_TE[0]+self.h*5)
        # stations.append(self.X_TE[0]+self.h)
        station_names.append('TE')
        

        #---------------------------------------------------#
        # This part is valid for multiple rows machine
        if isinstance(self.X_LE,tuple):
            for i in range(np.shape(self.X_LE)[1]-1):
                stations.append(0.5*(0.5*(self.X_TE[0, i] + self.X_LE[0, i+1]) + 0.5*(self.X_TE[-1, i] + self.X_LE[-1, i+1])))
                station_names.append('Rowgap_'+str(i+1))
        #--------------------------------------------------------#


        stations.append(self.axial_bounds[-1] - self.h)
        station_names.append('Outlet')
        # stations = [self.axial_bounds[0] + self.h, self.axial_bounds[-1] - self.h]
        # station_names = ['Inlet', 'Outlet']
        self.station_names = station_names

        slice_ax = Slice(Input=self.flow_data)
        slice_ax.SliceType.Normal = [1, 0, 0]
        slice_ax.SliceType.Origin = [stations[0] , 0, 0]

        slice_rad = Slice(Input=slice_ax)
        slice_rad.SliceType = 'Cylinder'
        slice_rad.SliceType.Center = [self.axial_bounds[0] , 0.0, 0.0]
        slice_rad.SliceType.Axis = [1.0, 0.0, 0.0]
        slice_rad.SliceType.Radius = radial_range[0]

        IV = IntegrateVariables(Input=slice_rad)
        setattr(self.output_data, 'radial_data', type('', (), {})())
        for i in range(len(stations)):
            setattr(self.output_data.radial_data, station_names[i], type('', (), {})())
            station_object = getattr(self.output_data.radial_data, station_names[i])
            for s in self.output_datasets:
                setattr(station_object, s, [])
            # print("station at",i," is",stations[i])
            # print("station are",stations)
            slice_ax.SliceType.Origin = [stations[i], 0, 0]
            self.station_slices.append(slice_ax)

            for j in range(len(radial_range)):
                slice_rad.SliceType.Radius = radial_range[j]
                iv_data = paraview.servermanager.Fetch(IV)

                if iv_data.GetCellData().GetArray("Length") == None:
                    L = 1.0
                    mom = float('Nan')
                else:
                    L = iv_data.GetCellData().GetArray("Length").GetValue(0)
                    mom = iv_data.GetPointData().GetArray("Momentum").GetValue(0)
                station_object.X.append(radial_range[j])
                station_object.mdot.append(mom/L)

                for s in self.output_datasets[2:]:
                    flow_dat = iv_data.GetPointData().GetArray(s).GetValue(0) / mom
                    output_data_list = getattr(station_object, s)
                    output_data_list.append(flow_dat)




    def extract_axial_data(self):
        self.listacheck=[]
        axial_range = np.linspace(self.axial_bounds[0] + self.h, self.axial_bounds[1] - self.h, 100)
        setattr(self.output_data, 'axial_data', type('', (), {})())

        for s in self.output_datasets:
            setattr(self.output_data.axial_data, s, [])

        rad_slice = Slice(Input=self.flow_data)
        rad_slice.SliceType.Normal = [1, 0, 0]
        # rad_slice.SliceType.Origin = [0, 0, self.axial_bounds[0]+self.h]
        rad_slice.SliceType.Origin = [self.axial_bounds[0]+self.h, 0, 0]


        IV = IntegrateVariables(Input=rad_slice)

              
        
        for x in axial_range:
            rad_slice.SliceType.Origin = [x, 0, 0]  

            iv_data = paraview.servermanager.Fetch(IV)

            # mom = iv_data.GetPointData().GetArray("Momentum").GetValue(2)
            mom = iv_data.GetPointData().GetArray("Momentum").GetValue(0)
            # print("Iv data ha i campi", iv_data.GetPointData())
            area = iv_data.GetCellData().GetArray("Area").GetValue(0)
            # self.area.append(area)
            blockage_f = (iv_data.GetPointData().GetArray("b").GetValue(0))/area
            # print("Il blockage factor è ",blockage_f)

            self.output_data.axial_data.X.append(x)
            self.output_data.axial_data.mdot.append(mom*360/self.IN["WEDGE"][0])
            # self.output_data.axial_data.mdot.append(mom*blockage_f*360/self.IN["WEDGE"][0])
            self.output_data.axial_data.area.append(area*360/self.IN["WEDGE"][0])
            
    
            # for s in self.output_datasets[2:]:
            for s in self.output_datasets[3:]:
                if 'sigma' not in s:
                    flow_dat = iv_data.GetPointData().GetArray(s).GetValue(0)/mom
                        # flow_dat = iv_data.GetPointData().GetArray(s).GetValue(0)/area
                        # flow_dat = iv_data.GetPointData().GetArray(s).GetValue(0)


                    output_data_list = getattr(self.output_data.axial_data, s)
                    output_data_list.append(flow_dat)
                




        # ------------------------------- standard deviation funzionante---------------------------------------

        # for s in self.list_sigma:
        #     print('studio la ', s)
        #     for x in range(len(axial_range)):
        #         rad_slice.SliceType.Origin = [axial_range[x], 0, 0]

        #         span30 = Slice(Input=rad_slice)
        #         span30.SliceType = 'Cylinder'
        #         span30.SliceType.Center = [0.0, 0.0, 0.0]
        #         span30.SliceType.Axis = [1.0, 0.0, 0.0]
        #         span30.SliceType.Radius = 0.2392956

        #         # span70 = Slice(Input=rad_slice)
        #         # span70.SliceType = 'Cylinder'
        #         # span70.SliceType.Center = [0.0, 0.0, 0.0]
        #         # span70.SliceType.Axis = [1.0, 0.0, 0.0]
        #         # span70.SliceType.Radius = 0.207389

        #         # span90 = Slice(Input=rad_slice)
        #         # span90.SliceType = 'Cylinder'
        #         # span90.SliceType.Center = [0.0, 0.0, 0.0]
        #         # span90.SliceType.Axis = [1.0, 0.0, 0.0]
        #         # span90.SliceType.Radius = 0.2392956

        #         iv2 = IntegrateVariables(Input=span30)
        #         iv_data2 = paraview.servermanager.Fetch(iv2)
        #         mom = iv_data2.GetPointData().GetArray("Momentum").GetValue(0)

        #         DS = DescriptiveStatistics(Input=span30)
        #         DS.VariablesofInterest = [s.replace('_sigma','')]
        #         ds_data = paraview.servermanager.Fetch(DS)
        #         dev = ds_data.GetBlock(1).GetColumnByName('Standard Deviation').GetValue(0)*100/ds_data.GetBlock(0).GetColumnByName('Mean').GetValue(0)
        #         # print('DS della sezione x=',x,' è ',ds_data.GetBlock(1).GetColumnByName('Standard Deviation').GetValue(0))

        #         # slice_data = paraview.servermanager.Fetch(span30)
                
        #         # # print('corrispettivo è ',getattr(self.output_data.axial_data, 'weighted_'+s.replace('_sigma',''))[x])
        #         # media = iv_data2.GetPointData().GetArray('weighted_'+s.replace('_sigma','')).GetValue(0)/mom #getattr(self.output_data.axial_data, 'weighted_'+s.replace('_sigma',''))[x]
        #         # print('la media è ',media)
        #         # lista_slice_s= np.array(list(vtk.util.numpy_support.vtk_to_numpy(slice_data.GetPointData().GetArray(s.replace('_sigma','')))))
        #         # dev = np.sqrt( sum((lista_slice_s - media )**2) / len(lista_slice_s))

        #         output_data_list = getattr(self.output_data.axial_data, s)
        #         output_data_list.append(dev)
        #         # print('la deviazione standard è ',dev)
        #         # lista_slice_m= np.array(list(vtk.util.numpy_support.vtk_to_numpy(slice_data.GetPointData().GetArray('Momentum'))))
                

        #     # print('check è ', lista_slice_s)

        #     # del DS
                
        # -----------------------------------------------------------------------------------------------------------------------
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
    def set_functions(self):

        # self.define_function(name="wP", input_data=self.flow_3D_BFM, function='Momentum_Z*Pressure')
        # self.define_function(name="wT", input_data=self.flow_3D_BFM, function='Momentum_Z*Temperature')
        self.define_function(name="weighted_P", input_data=self.flow_3D_BFM, function='Momentum_X*Pressure')
        self.define_function(name="weighted_T", input_data=self.flow_3D_BFM, function='Momentum_X*Temperature')
        # self.define_function(name="massa_BF", input_data=self.flow_3D_BFM, function='Momentum*b')


        self.flow_data = AppendAttributes(Input=[getattr(self.calculators, s) for s in list(self.calculators.__dict__.keys())])
        slice_inlet = Slice(Input=self.flow_data)
        slice_inlet.SliceType.Origin = [self.axial_bounds[0] + self.h, 0, 0]
        slice_inlet.SliceType.Normal = [1, 0, 0]

        IV = IntegrateVariables(Input=slice_inlet)                                # Define the close integral over inlet slice area
        iv_data = paraview.servermanager.Fetch(IV)
        # mom_inlet = iv_data.GetPointData().GetArray("Momentum").GetValue(2)
        mom_inlet = iv_data.GetPointData().GetArray("Momentum").GetValue(0)
        area = iv_data.GetCellData().GetArray("Area").GetValue(0)
        # print('The area cell is ',area)

        T_in = iv_data.GetPointData().GetArray("weighted_T").GetValue(0)/mom_inlet        # I am defining the reference T and P values at the inlet
                                                                                  # as mass flow averaged accross inlet slice
        P_in = iv_data.GetPointData().GetArray("weighted_P").GetValue(0) / mom_inlet

        Delete(IV)
        Delete(slice_inlet)
        Delete(self.flow_data)

        self.define_function(name='weighted_ds', input_data=self.flow_3D_BFM,
                            #  function='Momentum_Z*('+str(self.fluid_parameters.C_p)+'*ln(Temperature/'+str(T_in)+') - '
                             function='Momentum_X*('+str(self.fluid_parameters.C_p)+'*ln(Temperature/'+str(T_in)+') - '
                                      + str(self.fluid_parameters.R_gas)+'*ln(Pressure/'+str(P_in)+'))')

        self.define_function(name='weighted_Pt', input_data=self.flow_3D_BFM,
                            #  function='Momentum_Z*(Pressure + 0.5 * Density * ((Momentum_X/Density)^2 + '
                            # function='(Pressure + 0.5 * Density * ((Momentum_X/Density)^2 + '
                            #           '(Momentum_Y/Density)^2 + (Momentum_Z/Density)^2))')
                            function='Momentum_X*(Pressure *(1+0.2*(Mach)^2)^3.5 )')

        self.define_function(name='weighted_Tt', input_data=self.flow_3D_BFM,
                            #  function='Momentum_Z*(Temperature + 0.5 * (1/'
                            # function='Momentum_X*(Temperature + 0.5 * (1/'
                            #           + str(self.fluid_parameters.C_p)+') * ((Momentum_X/Density)^2 + (Momentum_Y/Density)^2 +(Momentum_Z/Density)^2))')
                            function='Momentum_X*(Temperature *(1+0.2*(Mach)^2))')
                            # function='(Temperature *(1+0.2*(Mach)^2))')

        self.define_function(name='weighted_Alpha', input_data=self.flow_3D_BFM,
                            #  function='Momentum_Z*(180/'+str(np.pi)+')*atan((Momentum_Y * coordsX/sqrt(coordsX^2 + coordsY^2) - Momentum_X*coordsY/sqrt(coordsX^2 + coordsY^2))/Momentum_Z)')
                            function='Momentum_X*(180/'+str(np.pi)+')*atan((Momentum_Y * coordsZ/sqrt(coordsZ^2 + coordsY^2) - Momentum_Z*coordsY/sqrt(coordsZ^2 + coordsY^2))/Momentum_X)')
                            # function='atan(Momentum_Z/Momentum_X)*(180/'+str(np.pi)+')')
                            # function='-atan(Momentum_Z/Momentum_X)*(180/'+str(np.pi)+')')
        # atan(Velocity  in Stn Frame v / Velocity  in Stn Frame w)
        # self.define_function(name='wMach', input_data=self.flow_3D_BFM, function='Momentum_Z*Mach')
        self.define_function(name='weighted_Mach', input_data=self.flow_3D_BFM, function='Momentum_X*Mach')



        self.define_function(name='Pt', input_data=self.flow_3D_BFM,
                            function='(Pressure *(1+0.2*(Mach)^2)^3.5 )')

        self.define_function(name='Tt', input_data=self.flow_3D_BFM,
                            function='(Temperature *(1+0.2*(Mach)^2))')

        self.define_function(name='P', input_data=self.flow_3D_BFM,
                            function='(Pressure)')

        self.define_function(name='T', input_data=self.flow_3D_BFM,
                            function='(Temperature)')
        
        self.define_function(name='QDM', input_data=self.flow_3D_BFM,
                            function='(Momentum_X)')

        self.define_function(name='Mrel',input_data=self.flow_3D_BFM,
                            function = 'sqrt( (Momentum_X/Density)^2  +  (   sqrt( (Momentum_Y/Density)^2  +  (Momentum_Z/Density)^2  )  -  1680.02*sqrt(coordsY^2+coordsZ^2)  )^2   )  / sqrt( 1.4*287.06*Temperature )')
        
        # self.define_function(name='Mach', input_data=self.flow_3D_BFM,
        #                     function='(Mach)')

        # self.define_function(name='my_Alpha', input_data=self.flow_3D_BFM,
        #                     function='(180/'+str(np.pi)+')*atan((Momentum_Y * coordsZ/sqrt(coordsZ^2 + coordsY^2) - Momentum_Z*coordsY/sqrt(coordsZ^2 + coordsY^2))/Momentum_X)')
                            
        # self.define_function(name='my_Mach', input_data=self.flow_3D_BFM, function='Mach')
        
    # Appending all the calculators stored in the class to the main flow so that the
        self.flow_data = AppendAttributes(Input=[getattr(self.calculators, s) for s in list(self.calculators.__dict__.keys())])



    # Save all the defined calculator objects as main class methods/attributes so I can access them later
    def define_function(self, name, input_data, function):
        C = Calculator(Input=input_data)
        C.ResultArrayName = name
        C.Function = function
        RenameSource(name, C)
        setattr(self.calculators, name, C)
        # if 'weighted_' in name:
            # setattr(self.calculators, name, C)
















    def get_domain_bounds(self):

 

        #-----------------------------------------------

        coords = Calculator(Input=self.flow_3D_BFM)
        coords.ResultArrayName = 'coordinates'
        coords.Function = 'coords'

        boundsfunction = ProgrammableFilter(Input=coords)
        boundsfunction.OutputDataSetType = 'vtkTable'
        boundsfunction.Script = """import numpy as np 
coords = inputs[0].PointData["coordinates"]
    
x, y, z = coords[:, 0], coords[:, 1], coords[:, 2]

x_min, y_min, z_min = min(x), min(y), min(z)
x_max, y_max, z_max = max(x), max(y), max(z)

x_bounds = np.array([x_min, x_max])
y_bounds = np.array([y_min, y_max])
z_bounds = np.array([z_min, z_max])
output.RowData.append(x_bounds, "x_bounds")
output.RowData.append(y_bounds, "y_bounds")
output.RowData.append(z_bounds, "z_bounds")"""

        bounds_data = paraview.servermanager.Fetch(boundsfunction)

        self.axial_bounds = [bounds_data.GetRowData().GetArray("x_bounds").GetValue(0),
                        bounds_data.GetRowData().GetArray("x_bounds").GetValue(1)]
        z_bounds = [bounds_data.GetRowData().GetArray("z_bounds").GetValue(0),
                        bounds_data.GetRowData().GetArray("z_bounds").GetValue(1)]
        y_bounds = [bounds_data.GetRowData().GetArray("y_bounds").GetValue(0),
                        bounds_data.GetRowData().GetArray("y_bounds").GetValue(1)]

        self.radial_bounds = [np.sqrt(z_bounds[0]**2 + y_bounds[0]**2)*1.01, np.sqrt(z_bounds[1]**2 + y_bounds[1]**2)*0.99]

        Delete(boundsfunction)
        Delete(coords)

       #-----------------------------------------

        if self.IN["WEDGE"][0]==360:
            slice1 = Slice(registrationName='Slice1', Input=self.flow_3D_BFM)
            slice1.SliceType = 'Plane'
            slice1.SliceType.Origin = [0.0, 0.0, 0.0]
            slice1.SliceType.Normal = [0.0, 0.0, 1.0]
        
        
            clip1 = Clip(registrationName='Clip1', Input=slice1)
            clip1.ClipType = 'Plane'
            clip1.ClipType.Origin = [0.0, 0.0, 0.0]
            clip1.ClipType.Normal = [0.0, -1.0, 0.0]

            clip2 = Clip(registrationName='Clip2', Input=clip1)
            clip2.ClipType = 'Plane'
            clip2.ClipType.Origin = [self.axial_bounds[0]+0.01*abs(self.axial_bounds[0]), 0.0, 0.0]
            clip2.ClipType.Normal = [-1.0, 0.0, 0.0]

            coords2 = Calculator(registrationName='Calculator2',Input=clip2)
            coords2.ResultArrayName = 'coordinates'
            coords2.Function = 'coords'

            boundsfunction2 = ProgrammableFilter(registrationName='newfilter',Input=coords2)
            boundsfunction2.OutputDataSetType = 'vtkTable'
            boundsfunction2.Script = """import numpy as np 
coords = inputs[0].PointData["coordinates"]
    
x, y, z = coords[:, 0], coords[:, 1], coords[:, 2]

x_min, y_min, z_min = min(x), min(y), min(z)
x_max, y_max, z_max = max(x), max(y), max(z)

x_bounds = np.array([x_min, x_max])
y_bounds = np.array([y_min, y_max])
z_bounds = np.array([z_min, z_max])
output.RowData.append(x_bounds, "x_bounds")
output.RowData.append(y_bounds, "y_bounds")
output.RowData.append(z_bounds, "z_bounds")"""

            bounds_data = paraview.servermanager.Fetch(boundsfunction2)

            # self.axial_bounds = [bounds_data.GetRowData().GetArray("x_bounds").GetValue(0),
            #             bounds_data.GetRowData().GetArray("x_bounds").GetValue(1)]
            z_bounds = [bounds_data.GetRowData().GetArray("z_bounds").GetValue(0),
                        bounds_data.GetRowData().GetArray("z_bounds").GetValue(1)]
            y_bounds = [bounds_data.GetRowData().GetArray("y_bounds").GetValue(0),
                        bounds_data.GetRowData().GetArray("y_bounds").GetValue(1)]

            self.radial_bounds = [np.sqrt(z_bounds[0]**2 + y_bounds[0]**2)*1.01, np.sqrt(z_bounds[1]**2 + y_bounds[1]**2)*0.99]
        
            Delete(boundsfunction2)
            Delete(coords2)
            Delete(clip1)
            Delete(clip2)
            Delete(slice1)

            #-----------------------------------------------
    



    def get_row_geom(self):
        # dir = os.getcwd()
        # # os.chdir(dir+"\\MeangenOutput/")
        P = ParabladeInput(self.IN)
        # S = StagenReader()
        self.X_LE = P.X_LE[0]
        self.X_TE = P.X_TE[0]
        self.Z_LE = P.Z_LE[0]
        self.Z_TE = P.Z_TE[0]
        # self.X_LE = [-0.00121553, 0.00617632, 0.01708539, 0.02375666 ]
        # self.X_TE = [0.08969732, 0.08953834, 0.07773417, 0.06895075, 0.06516138]
        # self.Z_LE = [0.09571595, 0.15195953, 0.20441029, 0.255249  ]
        # self.Z_TE = [0.11793934, 0.16173485, 0.20107889, 0.24823232]
        # os.chdir(dir)

controllo=extractData_BFM(IN)

# controllo=extractData_BFM()