import numpy as np
import os
import time
# from StagenReader import StagenReader
from SU2Writer_para import writeBFMinput, ReadUserInput
import math
import fileinput
import shutil
import re

class ParabladeInput:
     def __init__(self, IN):

        HOME = os.environ["P2BFM"]
        template_dir = HOME + "\\templates"
        testcases_dir = HOME + "\\testcases"
        sensitivity_dir = HOME + "\\Sensitivity_Analysis"
        optimization_dir = HOME + "\\Optimization"

        # Getting the input parameters
        self.IN = IN

        # Storing input parameters into the class.
        self.machineName = IN["MACHINE_NAME"]
        self.n_stage = int(IN["N_stage"][0])    # Stage count
        self.n_rows = int(IN["N_rows"][0])      # Row count
        # self.Dimension = int(IN["N_dim"][0])    # Parablade output dimension
        self.Omega = IN["Omega"][0]             # Rotation speed
        # self.mdot = IN["mass_flow"][0]          # Mass flow rate
        self.R_gas = IN["R_gas"][0]             # Gas constant
        self.gamma = IN["gamma"][0]             # Specific heat ratio
        self.P_tin = IN["P_t_in"][0]            # Inlet total pressure
        self.T_tin = IN["T_t_in"][0]            # Inlet total temperature
        self.machineType = IN["TYPE"]           # Machine type
        self.Np_mesh = int(IN["AXIAL_POINTS"][0])
        self.Np_radial = int(IN["RADIAL_POINTS"][0])
        

        self.N_b_R = []
        self.N_b_S = []
        self.X_LE =[]
        self.X_TE = []
        self.Z_LE= []
        self.Z_TE= []
        

        # Getting installation directory.
        HOME = os.getenv('P2BFM')

        # # Storing leading edge and trailing edge coordinates
        # self.X_LE = self.S.X_LE
        # self.X_TE = self.S.X_TE
        # self.Z_LE = self.S.Z_LE
        # self.Z_TE = self.S.Z_TE
        
            # Select the input file----------------------------------------------------------------------------------------------------------------
        for i in range(1,self.n_stage+1):
            for j in range(1,self.n_rows+1):
                if IN['SENSITIVITY'] == 'YES':
                    file1=open(testcases_dir+"\\"+self.machineName+"\\Stage_"+str(i)+"\\Bladerow_"+str(j)+"\\Bladerow.cfg",'r')
                    Lines1 = file1.readlines()
                    file1.close()

                    file2=open( sensitivity_dir+"\\bladerow.i",'r')
                    Lines2 = file2.readlines()
                    file2.close()

                    file3 = open(sensitivity_dir+"\\bladerow.cfg",'w')
                    for k in range(len(Lines1)):
                        new_line = str(Lines1[k].split("=")[0]) + "="
                        for n in range(len(Lines2)):
                            if Lines1[k].split("=")[0] in Lines2[n].split()[0]:
                                new_line = new_line + Lines2[n].split("=")[1].replace("\n","") + ","
                        if new_line == str(Lines1[k].split("=")[0]) + "=":
                            file3.write(str(Lines1[k]))
                        else:
                            new_line = new_line[:-1] + "\n"
                            file3.write(new_line)
                    file3.close()

                    self.Bladerow= sensitivity_dir+"\\Bladerow.cfg"

                elif IN['OPTIMIZATION'] == 'YES':
                    file1=open(testcases_dir+"\\"+self.machineName+"\\Stage_"+str(i)+"\\Bladerow_"+str(j)+"\\Bladerow.cfg",'r')
                    Lines1 = file1.readlines()
                    file1.close()

                    file2=open( optimization_dir+"\\bladerow.i",'r')
                    Lines2 = file2.readlines()
                    file2.close()

                    file3 = open(optimization_dir+"\\bladerow.cfg",'w')
                    for k in range(len(Lines1)):
                    # for k in range(0,22):
                        new_line_key = Lines1[k].split("=")[0]
                        new_line_values = Lines1[k].replace(" ","").replace("\n","").split("=")[-1].split(',')
                        for n in range(len(Lines2)):
                            if new_line_key in Lines2[n].split()[0]:
                                index = int(Lines2[n].split()[0][-1])
                                new_line_values[index] = Lines2[n].split("=")[1].replace("\n","") 
                        file3.write(new_line_key+"=")
                        for v in range(len(new_line_values)-1):
                            file3.write(new_line_values[v]+", ")
                        file3.write(new_line_values[-1])
                        file3.write("\n")

                        # new_line = str(Lines1[k].split("=")[0]) + "="
                        # for n in range(len(Lines2)):
                        #     if Lines1[k].split("=")[0] in Lines2[n].split()[0]:
                        #         new_line = new_line + Lines2[n].split("=")[1].replace("\n","") + ","
                        # if new_line == str(Lines1[k].split("=")[0]) + "=":
                        #     file3.write(str(Lines1[k]))
                        # else:
                        #     new_line = new_line[:-1] + "\n"
                        #     file3.write(new_line)
                    file3.close()

                    self.Bladerow= optimization_dir+"\\Bladerow.cfg"

                else:
                    self.Bladerow= testcases_dir+"\\"+self.machineName+"\\Stage_"+str(i)+"\\Bladerow_"+str(j)+"\\Bladerow.cfg"

                # ------------------------------------------------------------------------------------------------------------------------               

            # if self.n_rows==2:
            #     self.Bladerow_2= testcases_dir+"\\"+self.machineName+"\\Stage_"+str(i)+"\\Bladerow_2.cfg"

                Para_INfile = ReadUserInput(self.Bladerow)

                # get blade numbers from the Bladerow file
                self.N_b_R.append(Para_INfile["N_BLADES"])        # Blade count in rotor row
                self.N_b_S.append(Para_INfile["N_BLADES"])        # Blade count in stator row
                self.X_LE.append(np.transpose(Para_INfile["x_leading"]))
                self.X_TE.append(np.transpose(Para_INfile["x_trailing"]))
                self.Z_LE.append(np.transpose(Para_INfile["z_leading"]))
                self.Z_TE.append(np.transpose(Para_INfile["z_trailing"]))
            
            # Write a non-corrected file to take the curves for turbogrid
                f =  open(template_dir+"\\Turbogrid_Bladerow_"+str(j)+".cfg","w")
                f.write("N_POINTS = " +str(self.Np_mesh)+"\n")
                for k in Para_INfile:
                    f.write(k+" = ")
                    if isinstance(Para_INfile[k], str):
                        f.write(Para_INfile[k]+"\n")
                    else:
                        for m in range(len((Para_INfile[k]))-1):                   
                            f.write(str(Para_INfile[k][m])+", ")
                        f.write(str(Para_INfile[k][-1])+"\n")
                f.close()

                


            # Correct the Parablade input file in order to extend the blade over hub and shroud so that the interpolation
            # for BFM do not miss a spot (no tip gap model)
                 
                
                if IN['SENSITIVITY'] == 'YES' or IN['OPTIMIZATION'] == 'YES':
                    os.remove(self.Bladerow)
                    self.newBladerow = self.Bladerow
                else:
                    self.newBladerow = template_dir +"\\Bladerow_"+str(j)+".cfg"

                # for l in range(1,self.n_rows+1):
                if j==1:
                    f = open(self.newBladerow,"w")
                    f.write("BODY-FORCE = YES\n")
                    f.write("N_POINTS = " +str(self.Np_mesh)+"\n")
                    # f = open(template_dir +"\\Bladerow_"+str(j)+"_BFM.cfg","w")
                    corr = 0.02*(Para_INfile['z_leading'][-1]-Para_INfile['z_trailing'][0])
                    # corr = 0.0*(Para_INfile['z_leading'][-1]-Para_INfile['z_trailing'][0])
                    for k in Para_INfile:
                        if k == 'N_SECTIONS':
                            f.write(k+" = " +str(self.Np_radial)+"\n")
                        elif k == 'z_leading' or k == 'z_trailing':
                        # if  k == 'z_trailing':
                            # corr = 0.05*(Para_INfile[k][-1]-Para_INfile[k][0])
                            f.write(k+" = ")
                            f.write(str(Para_INfile[k][0]-corr)+", ")
                            for m in range(1,len((Para_INfile[k]))-1):                   
                                f.write(str(Para_INfile[k][m])+", ")
                            f.write(str(Para_INfile[k][-1]+corr)+"\n")
                        elif k == 'z_hub' :
                            f.write(k+" = ")
                            for m in range(0,len((Para_INfile[k]))-1):                   
                                f.write(str(Para_INfile[k][m]-corr)+", ")
                            f.write(str(Para_INfile[k][-1]-corr)+"\n")
                        elif k == 'z_shroud' :
                            f.write(k+" = ")
                            for m in range(0,len((Para_INfile[k]))-1):                   
                                f.write(str(Para_INfile[k][m]+corr)+", ")
                            f.write(str(Para_INfile[k][-1]+corr)+"\n")
                        else:
                            f.write(k+" = ")
                            if isinstance(Para_INfile[k], str):
                                f.write(Para_INfile[k]+"\n")
                            else:
                                for m in range(len((Para_INfile[k]))-1):                   
                                    f.write(str(Para_INfile[k][m])+", ")
                                f.write(str(Para_INfile[k][-1])+"\n")
                    f.close()
                else:
                    f = open(self.newBladerow,"w")
                    f.write("BODY-FORCE = YES\n")
                    f.write("N_POINTS = " +str(self.Np_mesh)+"\n")
                    # f = open(template_dir +"\\Bladerow_"+str(j)+"_BFM.cfg","w")
                    for k in Para_INfile:
                        if k == 'N_SECTIONS':
                            f.write(k+" = " +str(self.Np_radial)+"\n")
                        else:
                            f.write(k+" = ")
                            if isinstance(Para_INfile[k], str):
                                f.write(Para_INfile[k]+"\n")
                            else:
                                for m in range(len((Para_INfile[k]))-1):                   
                                    f.write(str(Para_INfile[k][m])+", ")
                                f.write(str(Para_INfile[k][-1])+"\n")
                    f.close()
                
                os.system("copy " + self.newBladerow +" "+ template_dir +"\\Bladerow_"+str(j)+".cfg")

            

    


    # Function used to read values from the user input file. This function was copied from Parablade.
     def ReadUserInput(name):
        IN = {}
        infile = open(name, 'r')
        for line in infile:
             words = re.split('=| |\n|,|[|]', line)
        if not any(words[0] in s for s in ['\n', '%', ' ', '#']):
            words = list(filter(None, words))
            for i in range(0, len(words)):
                try:
                    words[i] = float(words[i])
                except:
                    words[i] = words[i]
            if len(words[1::1]) == 1 and isinstance(words[1], float):
                IN[words[0]] = [words[1]]
            elif len(words[1::1]) == 1 and isinstance(words[1], str):
                IN[words[0]] = words[1]
            else:
                IN[words[0]] = words[1::1]
        IN['Config_Path'] = name
        return IN








