#!C:\Users\GMrx1\anaconda3

# --------------------------------------------------------------------------------------------------------------- #
# This code serves the purpose of using general engineering inputs for axial turbomachinery design and translate them
# to a suitable 2 or 3-dimensional mesh and body-force method input for SU2. This program works in conjunction with
# Meangen, Parablade(the body-force branch), which generates the detailed blade shape from Meangen output.
# For 3D meshing, Gmesh has to be installed and added to the PYTHONPATH. For 2D meshing, UMG2 has to be installed and
# added to the PATH as well. For body-force analyses in SU2, the feature_bodyforce_turbo has to be cloned and installed
# on the user's machine. Template files for all relevant configuration files can be found in the 'templates' folder and
# all scripts are located in the 'executables' folder.
#
# Author: E.C.Bunschoten
# Institution: TU Delft
# Date: 15/6/2020
# --------------------------------------------------------------------------------------------------------------- #
import sys
import os
import time
import shutil
import subprocess
import numpy as np


# from executable_windows.TURBO3D import TURBO

# Getting the executables directory
#HOME = os.environ["M2BFM"]
HOME = os.getenv('P2BFM')
sys.path.append(HOME + "\\executables_windows")
#sys.path.append(HOME + "\\executables")
# Importing all relevant executables from the installation directory
# from Meangen2Parablade import Meangen2Parablade
from ParabladeInput import ParabladeInput
# from Parablade2UMG2 import WriteUMG, writeStageMesh_BFM, writeStageMesh_Blade
from SU2Writer_para import writeBFMinput, ReadUserInput, writeSU2input
# from Mesh3D import Gmesh3D, Gmesh2D, FullAnnulus
from Mesh3D_ICEM_para import ICEM3D
from MESH_ICEM_2nodes import ICEM3D_2nodes
from MESH_ICEM_1node import ICEM3D_1node
from Mesh3D_ICEM_fullannulus import ICEM3D_fullannulus
from plot_vtk import VTK_PLOT
# from Mesh3D_ICEM_spherefan import ICEM3D_sphere
# from mesh3d_sphere_outogrid import ICEM3D_sphere
# from TURBO3D import TURBO
# from dataPlotter import axial_data_plotter
from CFX_Post_session import CFX_Post_session
# from Paraview_visualize import Paraview_visualize
# from ParaviewPost import AxialMachine
from Comparison_plot import Comparison_plot



# ------------------Reading input file-------------------------------#
# DIR = os.getcwd() 
DIR = HOME
# print('DIR is ',DIR)
# INFile = DIR + "\\templates\\" +sys.argv[-1]
# try:
#     INFile = DIR + "\\templates\\" +sys.argv[-1]
# except:
#     INFile = DIR + "\\" +'M2P.cfg'      # Default File name
# try:
#     IN = ReadUserInput(INFile)
# except:
#     raise Exception('\n\n\n''Something went wrong when reading the configuration file,exiting the program...'
#                     '\n\nTo call MakeBlade.py from terminal type:'
#                     '\n\tMakeBlade.py <configuration file name>')
t_start = time.time()

# # -----------------------Manual input----------------------------------#
# INFile = DIR + '\\templates\\Para_fan_stage.cfg'
# INFile = DIR + '\\templates\\P2BFM_My_fan.cfg'
INFile = DIR + '\\templates\\P2BFM_Rotor67.cfg'
print("P2BFM is processing the file: ", INFile)
IN = ReadUserInput(INFile)
elements_id = str(int(IN['AXIAL_POINTS'][0]))+"x"+str(int(IN['RADIAL_POINTS'][0])) +"x"+str(int(IN['TANGENTIAL_POINTS'][0]))
IN["elements_id"] = elements_id
# Executing Meangen and writing Parablade input files.
P = ParabladeInput(IN)


# # # Extracting stage count and calculating row count.
IN["N_BLADES"]=P.N_b_R[0]
n_stage = int(IN["N_stage"][0])

n_rows = int(IN["N_rows"][0])

if os.path.isdir(DIR+"\\Turbogrid"):
    shutil.rmtree(DIR+"\\Turbogrid")
os.mkdir(DIR+"\\Turbogrid")

# Looping over the number of stages to create folders for each stage and respective bladerow.
for i in range(n_stage):
    if not os.path.isdir(DIR+"\\Stage_"+str(i+1)):
        os.mkdir(DIR+"\\Stage_"+str(i+1))
    for j in range(1,n_rows+1):
        if not os.path.isdir(DIR+"\\Stage_"+str(i+1) + "\\Bladerow_"+str(j)):
            os.mkdir(DIR+"\\Stage_"+str(i+1)+"\\Bladerow_"+str(j))
        shutil.move(DIR + "\\templates\\Bladerow_"+str(j) + ".cfg", DIR+"\\Stage_"+str(i+1)+"\\Bladerow_"+str(j)+"\\Bladerow.cfg")
           
    
    if not os.path.isdir(DIR+"\\Turbogrid\\Stage_"+str(i+1)):
        os.mkdir(DIR+"\\Turbogrid\\Stage_"+str(i+1))
    for j in range(1,n_rows+1):
            if not os.path.isdir(DIR+"\\Turbogrid\\Stage_"+str(i+1) + "\\Bladerow_"+str(j)):
                os.mkdir(DIR+"\\Turbogrid\\Stage_"+str(i+1)+"\\Bladerow_"+str(j))
            shutil.move(DIR + "\\templates\\Turbogrid_Bladerow_"+str(j) + ".cfg", DIR+"\\Turbogrid\\Stage_"+str(i+1)+"\\Bladerow_"+str(j)+"\\Turbogrid_Bladerow.cfg")
            
# print('X_LE is ',P.X_LE[0][0])


# Checking for body-force and/or blade mesh option.
BFM = False
Blade = False
if IN["MESH_BFM"] == 'YES':
    BFM = True
if IN["MESH_BLADE"] == 'YES':
    Blade = True

# Looping over the blade rows to execute Parablade for each blade row and writing UMG2 input files for each blade row
# mesh. These individual meshes will be combined later into a complete machine mesh.
row = 1
for i in range(n_stage):
    for j in range(1,n_rows+1):
        # Moving to current blade row directory.
        # os.chdir(os.getcwd()+"\\Stage_"+str(i+1)+"\\Bladerow_"+str(j))
        os.chdir(DIR+"\\Stage_"+str(i+1)+"\\Bladerow_"+str(j))


        # Checking for blade plot option.
        if IN['PLOT_BLADE'] == 'YES':
            print("plotting blade")
            os.system("python -m PlotBlade Bladerow.cfg > Parablade.out")

        t_start_para = time.time()
        # Executing Parablade.
        print("Running Parablade...")
        os.system("python -m MakeBlade Bladerow.cfg > Parablade.out")
        t_end_para = time.time()
        print("Done! Parablade took "+str(t_end_para-t_start_para)+" seconds")

        if IN["ADJOINT"] == 'YES':
            os.system("sed -i 's/GEOMETRY/SENSITIVITY/g' Bladerow.cfg")
            print("Getting sensitivities for blade row "+str(2*i + j))
            os.system("MakeBlade.py Bladerow.cfg")
            print("Done!")




        #  # ------------ repeat the process for the rotor turbogrid blade geometry ( original one)

        if os.path.isdir(DIR+"\\Turbogrid\\Stage_"+str(i+1)+"\\Bladerow_"+str(j)):
             # Moving to current blade row directory.
            os.chdir(DIR+"\\Turbogrid\\Stage_"+str(i+1)+"\\Bladerow_"+str(j))
            print('la directory è ',os.getcwd())


            # Checking for blade plot option.
            if IN['PLOT_BLADE'] == 'YES':
                print("plotting blade")
                os.system("python -m PlotBlade Turbogrid_Bladerow.cfg > Parablade.out")


            # Executing Parablade.
            t_start_para = time.time()
            print("Running Parablade...")
            os.system("python -m MakeBlade Turbogrid_Bladerow.cfg > Parablade.out")
            t_end_para = time.time()
            print("Done! BFM Parablade took "+str(t_end_para-t_start_para)+" seconds")
            # print("Done!")



            if IN["ADJOINT"] == 'YES':
                os.system("sed -i 's/GEOMETRY/SENSITIVITY/g' Bladerow.cfg")
                print("Getting sensitivities for blade row "+str(2*i + j))
                os.system("MakeBlade.py Bladerow.cfg")
                print("Done!")


        # Updating row count.
        row += 1
        os.chdir(DIR)

# The individual 2D meshes are combined into a full machine mesh. In case the dimension number is 3 and a BFM mesh is
# desired, a suitable 3D mesh will be written. This option is currently not yet available for physical blades.

# if BFM:
#     # Writing BFM input file suitable for SU2 BFM analysis.
#     print("Writing Body-force SU2 input file...", end='     ')
#     writeBFMinput(P)
#     print("Done!")

#     # Writing 3D BFM mesh or combining individual 2D blade row meshes depending on case dimension.
#     if IN['N_dim'][0] == 3:
#         t_start_ICEM = time.time()
#         print("Writing 3D BFM mesh:...")
#         if IN["WEDGE"][0] == 360.0:
#             ICEM=ICEM3D_fullannulus(P,IN)
#         else:
#             if int(IN['TANGENTIAL_POINTS'][0]) == 3:
#                 ICEM=ICEM3D(P,IN)
#             elif int(IN['TANGENTIAL_POINTS'][0]) == 2:
#                 ICEM=ICEM3D_2nodes(P,IN)
#             elif int(IN['TANGENTIAL_POINTS'][0]) == 1:
#                 ICEM=ICEM3D_1node(P,IN)
        
#         t_end_ICEM=time.time()
#         print("Done! ICEM took "+str(t_end_ICEM-t_start_ICEM)+" seconds")
    
#     if not os.path.isdir(DIR+"\\SU2_symulations"):
#         os.mkdir(DIR+"\\SU2_symulations")
#     sym_folder = DIR+"\\SU2_symulations\\BFM_"+elements_id
#     if not os.path.isdir(sym_folder):
#         os.mkdir(sym_folder)
#     shutil.move(DIR + "\\BFM_stage_input.drg", sym_folder+"\\BFM_"+str(P.machineName)+"_geometry.drg")
#     shutil.copy(DIR + "\\templates\\BFM_"+IN['MACHINE_NAME']+".cfg", sym_folder+"\\BFM_"+str(P.machineName)+".cfg")
#     shutil.copy(DIR + "\\MESHOutput\\STAGE_BFM\\FAN_MESH"+elements_id+".cgns", sym_folder+"\\FAN_MESH"+elements_id+".cgns")
#     os.chdir(sym_folder)
    
#     sym_folder_lin = sym_folder.replace("\\","/").replace("C:","/mnt/c")
#     os.system("wsl -e cd "+ sym_folder_lin)
#     os.system('wsl -e "pwd"')
  
    
#     # t_start_SU2 = time.time()
#     # # os.system("wsl -e SU2_CFD BFM_rotor67.cfg > sym.out")
#     # os.system("wsl -e SU2_CFD BFM_"+str(INFile.split('_')[-1])+" > sym.out")
#     # t_end_SU2=time.time()
#     # print("Done! SU2 took "+str(t_end_SU2-t_start_SU2)+" seconds")
#     # sym_folder = DIR+"\\SU2_symulations\\BFM_"+elements_id
#     # f = open(sym_folder+"\\time"+elements_id+".txt",'w')
#     # f.write("time = "+str(t_end_SU2-t_start_SU2)+"s")
#     # f.close()




if IN["CFX_POSTPROCESS"] == 'YES':
    print("Postprocessing CFX simulation data....")


    #--------------------------------------------N.B. !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!------------------------------#
    # Use this just once to create the CFX comparison results after the initial CFX run. It is time consuming and requires to close manually cfdpost.exe

    t_start_CFDPOST=time.time()
    CFX_Session = CFX_Post_session(IN,ICEM,P)
    # os.system("cfdpost.exe -batch " + CFX_Session.file)
    os.system("cfdpost " + CFX_Session.file)
    t_end_CFDPOST=time.time()
    print("Done! CFD POST took "+str(t_end_CFDPOST-t_start_CFDPOST)+" seconds")
  
    # #-----------------------------------------------------------------------------------------------------------------#
    

if IN["BFM_POSTPROCESS"] == 'YES':

    # # # Multiple files----------------------------------------------------------
    # PARAVIEW_results_home = "C:\\Users\\GMrx1\\Desktop\\RESULTS_BACKUP"
    # PARAVIEW_results = []
    # for result in os.listdir(PARAVIEW_results_home):
    #     PARAVIEW_results.append(os.path.join(PARAVIEW_results_home, result))
    # t_start_paraview = time.time()
    # for f in PARAVIEW_results:
    #     os.system("pvpython "+HOME+"\\executable_windows\\ParaviewPost_para.py "+f+" "+INFile)
    # t_end_paraview = time.time()
    # print("Done! Paraview took "+str(t_end_paraview-t_start_paraview)+" seconds")
    # # #----------------------------------------------------------------------------------

    # Single file -----------------------------------------------------------------------------------
    sym_folder = DIR+"\\SU2_symulations\\BFM_"+elements_id
    for file in os.listdir(sym_folder):
            if '.vtu' in file and "surface" not in file:
                f = sym_folder+"\\"+file

    # f = sym_folder+"\\flow"+elements_id+".vtu"
    t_start_paraview = time.time()
    os.system("pvpython "+HOME+"\\executable_windows\\ParaviewPost_para.py "+f+" "+INFile)
    # os.system("pvbatch "+HOME+"\\executable_windows\\ParaviewPost_para.py "+f+" "+INFile)
    t_end_paraview = time.time()
    
    # if IN["WEDGE"][0]==360:
    #     vtk_folder = HOME+"\\PARAVIEW_Post\\vtk_files\\fullannulus\\"+elements_id
    # else:
    #     vtk_folder = HOME+"\\PARAVIEW_Post\\vtk_files\\periodic\\"+elements_id

    # vtk_plotter=VTK_PLOT(vtk_folder,P)
    
    print("Done! Paraview took "+str(t_end_paraview-t_start_paraview)+" seconds")
    if not os.path.isdir(DIR+"\\PARAVIEW_Post\\time"):
        os.mkdir(DIR+"\\PARAVIEW_Post\\time")
    # shutil.copy(sym_folder+"\\time"+elements_id+".txt",DIR+"\\PARAVIEW_Post\\time\\time"+elements_id+".txt")
    # ----------------------------------------------------------------------------------


    # Paraview_visualize(P)
    # os.system("pvpython "+HOME+"\\executable_windows\\Paraview_visualize.py "+INFile)
    # print("Creating data plots...")
    # axial_data_plotter()
    # # CFX_plot("C:\\Users\\GMrx1\\Desktop\\ANSYS_WORKS\\Rotor67\\results.csv")
    # print("Done!")


if IN["PLOT_COMPARISON"] == 'YES':
    CFX_POST_HOME = DIR+"\\CFX_Post"
    PARAVIEW_POST_HOME = DIR + "\\PARAVIEW_Post"

    # # Comparison_plot("C:\\Users\\GMrx1\\Desktop\\ANSYS_WORKS\\Rotor67\\results.csv")
    #Comparison_plot(CFX_POST_HOME, PARAVIEW_POST_HOME)

    
    if IN["WEDGE"][0]==360:
         vtk_folder = HOME+"\\PARAVIEW_Post\\vtk_files\\fullannulus\\"+elements_id
    else:
         vtk_folder = HOME+"\\PARAVIEW_Post\\vtk_files\\periodic\\"+elements_id

    vtk_plotter=VTK_PLOT(vtk_folder,P)
    


if IN['SENSITIVITY'] == 'YES':
    shutil.copy(DIR+"\\PARAVIEW_Post\\objectives\\flow"+elements_id+"_Machine_objectives.txt",DIR+"\\Sensitivity_Analysis\\Bladerow.log")
    # os.mkdir(P2BFM+"\\Sensitivity_Analysis")

if IN['OPTIMIZATION'] == 'YES':
    shutil.copy(DIR+"\\PARAVIEW_Post\\objectives\\flow"+elements_id+"_Machine_objectives.txt",DIR+"\\Optimization\\Bladerow.log")



# print("Total processing time: " + str(time.time() - t_start))