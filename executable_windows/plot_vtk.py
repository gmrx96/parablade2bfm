import numpy as np
import os
import matplotlib
import matplotlib.pyplot as plt
import turbulucid as tbl
import shutil

matplotlib.rc('font',family='Times New Roman')

class VTK_PLOT:

    def __init__(self, folder,P):
        # plot_fs = ['Mach','Pressure','Density','Momentum_vector']
        # unità = {'Mach':'M','Pressure':'P [Pa]','Density':r'${\rho}$ [$\frac{kg}{m^3}$]','Momentum_vector':r'${\dot{m_x}}$ [$\frac{kg*m}{s}$]'}
        plot_fs = ['Mrel','Pressure','Density','Momentum_vector']
        unità = {'Mrel':'M$_{rel}$ [-]','Pressure':'P [Pa]','Density':r'${\rho}$ [$\frac{kg}{m^3}$]','Momentum_vector':r'${\dot{m_x}}$ [$\frac{kg*m}{s}$]'}
        csfont = {'fontname':'Times New Roman'}
        # x_INLET = -0.4
        x_LE = np.linspace(P.X_LE[0][0],P.X_LE[0][-1],num=11)
        x_TE = np.linspace(P.X_TE[0][0],P.X_TE[0][-1],num=11)
        # x_OUTLET = 0.5
        if os.path.isdir(folder+"\\figures"):
            shutil.rmtree(folder+"\\figures")
        files_to_process=[]
        for file in os.listdir(folder):
            files_to_process.append(os.path.join(folder, file))

        if not os.path.isdir(folder+"\\figures"):
            os.system("mkdir "+folder+"\\figures")

        for file in files_to_process:
            # full_file=(os.path.join(folder, file))
            print('Plotting file '+file)
            case = tbl.Case(file)

            k = (case.xlim[-1]-case.xlim[0])/(case.ylim[-1]-case.ylim[0])/2
    


            if "Span" in file:
                for f in plot_fs:

                    h = int(file.split("_")[-1].replace('.vtk',''))
                    plt.figure(figsize=(8,6))
        
                    if 'vector' in f:
                        p = tbl.plot_field(case,case[f.replace("_vector","")][:,0],scaleY=1/k,plotBoundaries=False,colorbar=False)
                        plt.title(f.replace("_vector"," X")+" at "+str(h)+r"0% of the span",**csfont,fontsize = 26, pad =25)
                    else:
                        p = tbl.plot_field(case,case[f],scaleY=1/k,plotBoundaries=False,colorbar=False)
                        plt.title(f+" at "+str(h)+r"0% of the span",**csfont,fontsize = 26, pad =25)
                    clb = tbl.add_colorbar(p)
                    clb.ax.set_title(unità[f],**csfont,fontsize = 22,pad=15)
                    clb.ax.tick_params(labelsize=16)
        
                    plt.xlabel(r'$X$ [m]',fontsize = 22,**csfont)
                    plt.ylabel(r'$\theta$ [rad]',fontsize = 22,**csfont)
                    plt.yticks(np.linspace(case.ylim[0]*k, case.ylim[1]*k, 3), (r'$-\pi$', '0', r'$\pi$'),fontsize = 16)
                    plt.xticks(fontsize=16)


                    #------insert cutting locations------------------------------------------------------------------
                    # plt.axvline(x=x_INLET,color='k', linestyle='--')
                    # arrowprops=dict(arrowstyle='->', color='k', linewidth=1.5)
                    # matplotlib.pyplot.annotate('INLET', (x_INLET,case.ylim[0]*k), xytext=(x_INLET+(case.xlim[-1]-case.xlim[0])/15,case.ylim[0]*k-(case.ylim[-1]-case.ylim[0])*k/10),arrowprops=arrowprops)

                    plt.axvline(x=x_LE[h],color='black', linestyle='--')
                    # matplotlib.pyplot.annotate('LE', (0-0.05,case.ylim[0]/2.2),color='white')
                    arrowprops=dict(arrowstyle='->', color='black', linewidth=1.5)
                    matplotlib.pyplot.annotate('LE', (x_LE[h],case.ylim[0]*k), xytext=(x_LE[h]-(case.xlim[-1]-case.xlim[0])/8,case.ylim[0]*k-(case.ylim[-1]-case.ylim[0])*k/6.5),arrowprops=arrowprops,fontsize = 18)

                    plt.axvline(x=x_TE[h],color='k', linestyle='--')
                    arrowprops=dict(arrowstyle='->', color='k', linewidth=1.5)
                    matplotlib.pyplot.annotate('TE', (x_TE[h],case.ylim[0]*k), xytext=(x_TE[h]+(case.xlim[-1]-case.xlim[0])/15,case.ylim[0]*k-(case.ylim[-1]-case.ylim[0])*k/6.5),arrowprops=arrowprops,fontsize = 18)

                    # plt.axvline(x=x_OUTLET,color='k', linestyle='--')
                    # arrowprops=dict(arrowstyle='->', color='k', linewidth=1.5)
                    # matplotlib.pyplot.annotate(r'OUTLET',(x_OUTLET,case.ylim[0]*k), xytext=(x_OUTLET-(case.xlim[-1]-case.xlim[0])/15,case.ylim[0]*k-(case.ylim[-1]-case.ylim[0])*k/10),arrowprops=arrowprops)
                    
                    #---------------------------------------------------------------------------------------------------------------------------
                    #plt.show()
                    plt.savefig(file.replace(file.split("\\")[-1],"figures\\Span"+str(h)+f.replace("_vector","_x")+".pdf"))
                    plt.close()

            if "Circular" in file:
                case1 = tbl.Case(file)
                print(case1.fields)
    
                for f in plot_fs:
                    plt.figure(figsize=(8,8))
                    if "vector" in f:
                        p = tbl.plot_field(case,case[f.replace("_vector","")][:,0],plotBoundaries=False,colorbar=False)
                        plt.title(f.replace("_vector"," X")+" at the "+file.split("_")[-1].replace('.vtk',''),**csfont,fontsize = 26, pad =25)
                    else:
                        p = tbl.plot_field(case,case[f],plotBoundaries=False,colorbar=False)
                        if f == 'Mrel':
                            plt.title(unità[f]+" at the "+file.split("_")[-1].replace('.vtk',''),**csfont,fontsize = 26,pad=25)
                        else:
                            plt.title(f+" at the "+file.split("_")[-1].replace('.vtk',''),**csfont,fontsize = 26,pad=25)
                    clb = tbl.add_colorbar(p)
                    clb.ax.set_title(unità[f],**csfont,fontsize = 22,pad=15)
                    clb.ax.tick_params(labelsize=16)
        
                    plt.xlabel(r'$Y$ [m]',fontsize = 22,**csfont)
                    plt.ylabel(r'$Z$ [m]',fontsize = 22,**csfont)
                    plt.yticks(fontsize=16)
                    plt.xticks(fontsize=16)
                    plt.savefig(file.replace(file.split("\\")[-1],"figures\\"+file.split("_")[-1].replace('.vtk','')+f.replace("_vector","_x")+".pdf"))
                    plt.close()
                    #plt.show()

            # if "Wedge" in file:

            # if "side" in file: