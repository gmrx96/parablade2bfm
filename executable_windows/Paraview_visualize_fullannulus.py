#### import the simple module from the paraview
from tokenize import group
import numpy as np
import sys
from paraview.simple import *



# inFile = "C:\\Users\\GMrx1\\Desktop\\fullannulus-toperformonworkstation\\Thollet\\BFM60x60x360long\\flow60x60.vtu"
inFile = "C:\\Users\\GMrx1\\Desktop\\importare\\BFM_60x40x240medium\\flow60x40x240.vtu"


class Paraview_visualize:



    # def __init__(self,IN):
    def __init__(self,inFile):     

        #### disable automatic camera reset on 'Show'
        paraview.simple._DisableFirstRenderCameraReset()

        self.flowvtu = XMLUnstructuredGridReader(registrationName='flow.vtu', FileName=[inFile])
        
        self.flowvtu.PointArrayStatus = ['Density', 'Momentum', 'Energy', 'Turb_Kin_Energy', 'Omega', 'Pressure', 'Temperature', 'Mach', 'Pressure_Coefficient', 'Laminar_Viscosity', 'Skin_Friction_Coefficient', 'Heat_Flux', 'Y_Plus', 'Eddy_Viscosity', 'Residual_Density', 'Residual_Momentum', 'Residual_Energy', 'Residual_TKE', 'Residual_Omega', 'n', 'b', 'blockage_gradient', 'body_force_factor', 'rotation_factor', 'blade_count', 'W', 'BF']
    
        # Properties modified on flowvtu
        self.flowvtu.TimeArray = 'None'
        
        self.get_coords()

        self.rad_slice = []
        self.groups=[]
        self.down_roll=[]
        self.up_roll=[]
        self.h_span = np.linspace(self.radial_bounds[0]*1.01,self.radial_bounds[1]*0.99,11)
        for i in range(len(self.h_span)):
            self.create_surface_slice(i)
        
            self.unroll(i)
        
            self.recreate_vtk(i)


    def recreate_vtk(self,i):

        extractCylUnroll = ExtractSurface(registrationName='ExtractSurfaceCylUnroll'+str(i), Input=self.groups[i])
        generateCylUnrollNormals = GenerateSurfaceNormals(registrationName='generateCylUnrollNormals', Input=extractCylUnroll)
        pointDatatoCellDataCylUnroll = PointDatatoCellData(registrationName='pointDatatoCellDataCylUnroll', Input=generateCylUnrollNormals)
        pointDatatoCellDataCylUnroll.PointDataArraytoprocess = ['BF', 'Density', 'Energy', 'Mach', 'Momentum', 'Normals', 'Pressure', 'Pressure_Coefficient', 'Residual_Density', 'Residual_Energy', 'Residual_Momentum', 'Temperature', 'W', 'b', 'blade_count', 'blockage_gradient', 'body_force_factor', 'n', 'rotation_factor']
        
        SaveData('C:/Users/GMrx1//Desktop/demo_turbulucid/UnrolledCylinder_'+str(i)+'.vtk', proxy=pointDatatoCellDataCylUnroll, CellDataArrays=['BF', 'Density', 'Energy', 'Mach', 'Momentum', 'Normals', 'Pressure', 'Pressure_Coefficient', 'Residual_Density', 'Residual_Energy', 'Residual_Momentum', 'Temperature', 'W', 'b', 'blade_count', 'blockage_gradient', 'body_force_factor', 'n', 'rotation_factor'])


        # extractUproll = ExtractSurface(registrationName='ExtractSurfaceUproll'+str(i), Input=self.up_roll[i])
        # extractDownroll = ExtractSurface(registrationName='ExtractSurfaceDownroll'+str(i), Input=self.down_roll[i])

        # generateUprollNormals = GenerateSurfaceNormals(registrationName='generateUprollNormals', Input=extractUproll)
        # generateDownrollNormals = GenerateSurfaceNormals(registrationName='generateDownrollNormals', Input=extractDownroll)

        # pointDatatoCellDataUproll = PointDatatoCellData(registrationName='pointDatatoCellDataUproll', Input=generateUprollNormals)
        # pointDatatoCellDataDownroll = PointDatatoCellData(registrationName='pointDatatoCellDataDownroll', Input=generateDownrollNormals)

        # pointDatatoCellDataUproll.PointDataArraytoprocess = ['BF', 'Density', 'Energy', 'Mach', 'Momentum', 'Normals', 'Pressure', 'Pressure_Coefficient', 'Residual_Density', 'Residual_Energy', 'Residual_Momentum', 'Temperature', 'W', 'b', 'blade_count', 'blockage_gradient', 'body_force_factor', 'n', 'rotation_factor']
        # pointDatatoCellDataDownroll.PointDataArraytoprocess = ['BF', 'Density', 'Energy', 'Mach', 'Momentum', 'Normals', 'Pressure', 'Pressure_Coefficient', 'Residual_Density', 'Residual_Energy', 'Residual_Momentum', 'Temperature', 'W', 'b', 'blade_count', 'blockage_gradient', 'body_force_factor', 'n', 'rotation_factor']

        # SaveData('C:/Users/GMrx1//Desktop/demo_turbulucid/Uproll'+str(i)+'.vtk', proxy=pointDatatoCellDataUproll, CellDataArrays=['BF', 'Density', 'Energy', 'Mach', 'Momentum', 'Normals', 'Pressure', 'Pressure_Coefficient', 'Residual_Density', 'Residual_Energy', 'Residual_Momentum', 'Temperature', 'W', 'b', 'blade_count', 'blockage_gradient', 'body_force_factor', 'n', 'rotation_factor'])
        # SaveData('C:/Users/GMrx1//Desktop/demo_turbulucid/Downroll'+str(i)+'.vtk', proxy=pointDatatoCellDataDownroll, CellDataArrays=['BF', 'Density', 'Energy', 'Mach', 'Momentum', 'Normals', 'Pressure', 'Pressure_Coefficient', 'Residual_Density', 'Residual_Energy', 'Residual_Momentum', 'Temperature', 'W', 'b', 'blade_count', 'blockage_gradient', 'body_force_factor', 'n', 'rotation_factor'])




    def unroll(self,i):

        
#         slice_coords = Calculator(Input=self.rad_slice[i])
#         slice_coords.ResultArrayName = 'coordinates'
#         slice_coords.Function = 'coords'

#         slice_unroll = ProgrammableFilter(Input=slice_coords)
#         slice_unroll.OutputDataSetType = 'Same as Input'
#         slice_unroll.Script = """import numpy as np
# coords = inputs[0].PointData["coordinates"]
# BF= inputs[0].PointData["BF"]
# density = inputs[0].PointData["Density"]
# energy = inputs[0].PointData["Energy"]
# mach = inputs[0].PointData["Mach"]
# momentum = inputs[0].PointData["Momentum"]
# pressure = inputs[0].PointData["Pressure"]
# res_dens = inputs[0].PointData["Residual_Density"]
# res_en = inputs[0].PointData["Residual_Energy"]
# res_mom = inputs[0].PointData["Residual_Momentum"]
# temperature = inputs[0].PointData["Temperature"]
# w = inputs[0].PointData["W"]
# b = inputs[0].PointData["b"]

# x =  coords[:, 0]
# y = np.zeros(len(coords[:, 0]))
# for i in range(len(x)):
#     if coords[i, 2] >= 0:
#         y[i]=  np.arctan(coords[i, 1]/coords[i, 2])*np.sqrt(coords[i, 2]**2+coords[i, 1]**2) + 0.5*np.pi * max(coords[:, 1])
#     else:
#         y[i] =  -np.arctan(coords[i, 1]/coords[i, 2])*np.sqrt(coords[i, 2]**2+coords[i, 1]**2) - 0.5*np.pi * max(coords[:, 1])
# z = np.zeros(len(coords[:, 0]))

# output.PointData.append(x, "x")
# output.PointData.append(y, "y")
# output.PointData.append(z, "z")

# output.PointData.append(BF, "BF")
# output.PointData.append(density, "Density")
# output.PointData.append(energy, "Energy")
# output.PointData.append(mach, "Mach")
# output.PointData.append(momentum, "Momentum")
# output.PointData.append(pressure, "Pressure")
# output.PointData.append(res_dens, "Residual_Density")
# output.PointData.append(res_en, "Residual_Energy")
# output.PointData.append(res_mom, "Residual_Momentum")
# output.PointData.append(temperature, "Temperature")
# output.PointData.append(w, "W")
# output.PointData.append(b, "b")

# """
#         unrolled_coords = Calculator(Input=slice_unroll)
#         unrolled_coords.CoordinateResults = 1
#         unrolled_coords.Function = 'iHat*x+jHat*y+kHat*z'
        




# import numpy as np
# coords = inputs[0].PointData["coordinates"]
# Mach = inputs[0].PointData["Mach"]
# #print(dir(inputs[0]))
# #print(vars(inputs[0]))
# a=inputs[0].FieldData
# #print(a)
# x, y, z = coords[:, 0], np.zeros(len(coords[:, 0])), np.arctan(coords[:, 2]/coords[:, 1])*np.sqrt(coords[:, 2]**2+coords[:, 1]**2)
# output.PointData.append(y, "y")
# output.PointData.append(Mach, "m")

        up_clip = Clip(registrationName='up_clip', Input=self.rad_slice[i])
        up_clip.ClipType = 'Plane'
        up_clip.ClipType.Origin = [0.0, 0.0, 0.0]
        up_clip.ClipType.Normal = [0.0, -1.0, 0.0]

        up = Calculator(registrationName='Up_view', Input=up_clip)
        up.CoordinateResults = -1
        up.Function = 'coordsX*iHat+ ((atan(coordsY/coordsZ)+(1-(coordsZ/abs(coordsZ)))*1.570796326)*(coordsZ^2+coordsY^2)^(0.5))*jHat+0*kHat'
        
        
        down_clip = Clip(registrationName='down_clip', Input=self.rad_slice[i])
        down_clip.ClipType = 'Plane'
        down_clip.ClipType.Origin = [0.0, 0.0, 0.0]
        down_clip.ClipType.Normal = [0.0, +1.0, 0.0]

        down = Calculator(registrationName='Down_view', Input=down_clip)
        down.CoordinateResults = 1
        down.Function = 'coordsX*iHat+ ((atan(coordsY/coordsZ)-(1-(coordsZ/abs(coordsZ)))*1.570796326)*(coordsZ^2+coordsY^2)^(0.5))*jHat+0*kHat'
        
        self.down_roll.append(down)
        self.up_roll.append(up)
        groupDatasets1 = GroupDatasets(registrationName='unrolled_domain'+str(i), Input=[down, up])
        groupDatasets1.BlockNames = ['Down_view', 'Up_view']

        mergeBlocks1 = MergeBlocks(registrationName='UnrolledCylinder_'+str(i), Input=groupDatasets1)
        self.groups.append(mergeBlocks1)
        
      





    def create_surface_slice(self,i):
        rad_slice = Slice(registrationName="Span_"+str(i),Input=self.flowvtu)
        rad_slice.SliceType = "Cylinder"
        rad_slice.SliceType.Axis = [1,0,0]
        rad_slice.SliceType.Radius = self.h_span[i]
        rad_slice.SliceType.Center = [0.0, 0.0, 0.0]
        self.rad_slice.append(rad_slice)
       




        

    def get_coords(self):
        coords = Calculator(Input=self.flowvtu)
        coords.ResultArrayName = 'coordinates'
        coords.Function = 'coords'

        boundsfunction = ProgrammableFilter(Input=coords)
        boundsfunction.OutputDataSetType = 'vtkTable'
        boundsfunction.Script = """import numpy as np 
coords = inputs[0].PointData["coordinates"]
    
x, y, z = coords[:, 0], coords[:, 1], coords[:, 2]

x_min, y_min, z_min = min(x), min(y), min(z)
x_max, y_max, z_max = max(x), max(y), max(z)

x_bounds = np.array([x_min, x_max])
y_bounds = np.array([y_min, y_max])
z_bounds = np.array([z_min, z_max])
output.RowData.append(x_bounds, "x_bounds")
output.RowData.append(y_bounds, "y_bounds")
output.RowData.append(z_bounds, "z_bounds")"""

        bounds_data = paraview.servermanager.Fetch(boundsfunction)
        
        self.axial_bounds = [bounds_data.GetRowData().GetArray("x_bounds").GetValue(0),
                        bounds_data.GetRowData().GetArray("x_bounds").GetValue(1)]
        z_bounds = [bounds_data.GetRowData().GetArray("z_bounds").GetValue(0),
                        bounds_data.GetRowData().GetArray("z_bounds").GetValue(1)]
        y_bounds = [bounds_data.GetRowData().GetArray("y_bounds").GetValue(0),
                        bounds_data.GetRowData().GetArray("y_bounds").GetValue(1)]
        # self.radial_bounds = [np.sqrt(z_bounds[0]**2 + y_bounds[0]**2), np.sqrt(z_bounds[1]**2 + y_bounds[1]**2)]
        self.y_bounds=y_bounds
        self.z_bounds=z_bounds
        Delete(boundsfunction)
        Delete(coords)
        
        # slice1 = Slice(registrationName='Slice1', Input=self.flowvtu)
        # slice1.SliceType = 'Plane'
        # slice1.SliceType.Origin = [self.axial_bounds[-1]*0.95, 0.0, 0.0]
        # slice1.SliceType.Normal = [1.0, 0.0, 0.0]
        
        
        # clip1 = Clip(registrationName='Clip1', Input=slice1)
        # clip1.ClipType = 'Plane'
        # clip1.ClipType.Origin = [0.0, 0.0, 0.0]
        # clip1.ClipType.Normal = [0.0, -1.0, 0.0]

        # slice2 = Slice(registrationName='Slice2', Input=clip1)
        # slice2.SliceType = 'Plane'
        # slice2.SliceType.Origin = [self.axial_bounds[-1]*0.95, 0.0, 0.0]
        # slice2.SliceType.Normal = [0.0, 0.0, 1.0]

        # coords2 = Calculator(registrationName='Calculator2',Input=slice2)
        # coords2.ResultArrayName = 'coordinates'
        # coords2.Function = 'coords'
        
        slice1 = Slice(registrationName='Slice1', Input=self.flowvtu)
        slice1.SliceType = 'Plane'
        slice1.SliceType.Origin = [0.0, 0.0, 0.0]
        slice1.SliceType.Normal = [0.0, 0.0, 1.0]
        
        
        clip1 = Clip(registrationName='Clip1', Input=slice1)
        clip1.ClipType = 'Plane'
        clip1.ClipType.Origin = [0.0, 0.0, 0.0]
        clip1.ClipType.Normal = [0.0, -1.0, 0.0]

        clip2 = Clip(registrationName='Clip2', Input=clip1)
        clip2.ClipType = 'Plane'
        clip2.ClipType.Origin = [self.axial_bounds[-1]*0.95, 0.0, 0.0]
        clip2.ClipType.Normal = [-1.0, 0.0, 0.0]

        coords2 = Calculator(registrationName='Calculator2',Input=clip2)
        coords2.ResultArrayName = 'coordinates'
        coords2.Function = 'coords'

        boundsfunction2 = ProgrammableFilter(registrationName='newfilter',Input=coords2)
        boundsfunction2.OutputDataSetType = 'vtkTable'
        boundsfunction2.Script = """import numpy as np 
coords = inputs[0].PointData["coordinates"]
    
x, y, z = coords[:, 0], coords[:, 1], coords[:, 2]

x_min, y_min, z_min = min(x), min(y), min(z)
x_max, y_max, z_max = max(x), max(y), max(z)

x_bounds = np.array([x_min, x_max])
y_bounds = np.array([y_min, y_max])
z_bounds = np.array([z_min, z_max])
output.RowData.append(x_bounds, "x_bounds")
output.RowData.append(y_bounds, "y_bounds")
output.RowData.append(z_bounds, "z_bounds")"""
        
        babbo = paraview.servermanager.Fetch(boundsfunction2)
        self.radial_bounds = [babbo.GetRowData().GetArray("y_bounds").GetValue(0),
                        bounds_data.GetRowData().GetArray("y_bounds").GetValue(1)]
        self.babbo = babbo              

        Delete(boundsfunction2)
        Delete(coords2)
        Delete(clip1)
        Delete(clip2)
        Delete(slice1)

        # print("radial b are ",self.radial_bounds)




    
# Paraview_visualize(inFile)

controllo=Paraview_visualize(inFile)
