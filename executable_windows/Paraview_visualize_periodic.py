

#### import the simple module from the paraview
import numpy as np
import sys
from paraview.simple import *


# Defining the path to the Meangen2BFM installation folder  (here on windows I must fully specify it!!!)
HOME = "C:\\Users\\GMrx1\\Desktop\\github\\Parablade2BFM"        
# Appending the executables folder to the path in order for the other scripts to be accessed
sys.path.append(HOME + "\\executables")

# inFile = sys.argv[-1]   # Meangen2BFM configuration file
# inFile = HOME + '\\templates\\Para_fan_stage.cfg'
# from SU2Writer_para import ReadUserInput
# from ParabladeInput import ParabladeInput


# IN = ReadUserInput(inFile)  # Transforming the configuration file into a class containing all the design variables and
                            # simulation settings.


class Paraview_visualize:


    # def __init__(self,IN):
    def __init__(self):

        # Import the required fields from Parablade config file
        # self.IN = IN




        #### disable automatic camera reset on 'Show'
        paraview.simple._DisableFirstRenderCameraReset()

        # create a new 'XML Unstructured Grid Reader'
        # self.flowvtu = XMLUnstructuredGridReader(registrationName='flow.vtu', FileName=[inFile])
        # self.flowvtu = XMLUnstructuredGridReader(registrationName='flow.vtu', FileName=['C:\\Users\\GMrx1\\Desktop\\STAGE_BFM_20axialelements\\RESULTS\\flow.vtu'])
        self.flowvtu = XMLUnstructuredGridReader(registrationName='flow.vtu', FileName=['C:\\Users\\GMrx1\\Desktop\\github\\Parablade2BFM\\SU2_symulations\\BFM_40x40\\flow40x40.vtu'])
        
        self.flowvtu.PointArrayStatus = ['Density', 'Momentum', 'Energy', 'Turb_Kin_Energy', 'Omega', 'Pressure', 'Temperature', 'Mach', 'Pressure_Coefficient', 'Laminar_Viscosity', 'Skin_Friction_Coefficient', 'Heat_Flux', 'Y_Plus', 'Eddy_Viscosity', 'Residual_Density', 'Residual_Momentum', 'Residual_Energy', 'Residual_TKE', 'Residual_Omega', 'n', 'b', 'blockage_gradient', 'body_force_factor', 'rotation_factor', 'blade_count', 'W', 'BF']
    
        # Properties modified on flowvtu
        self.flowvtu.TimeArray = 'None'
        # UpdatePipeline(time=0.0, proxy=flowvtu)
        self.get_coords()

        self.create_donut()

        self.streamtracer()

        self.tube()

        self.make_glyph()

        # self.



    def make_glyph(selph):

        # create a new 'Glyph'
        tube1 = FindSource('tube1')
        glyph1 = Glyph(registrationName='Glyph1', Input=tube1, GlyphType='Arrow')
        glyph1.OrientationArray = ['POINTS', 'Momentum']
        glyph1.ScaleArray = ['POINTS', 'Momentum']
        glyph1.ScaleFactor = 0.0002
        # glyph1.GlyphTransform = 'Transform2'
        # show data in view
        glyph1Display = Show(glyph1)

#     find source
# tube1 = FindSource('tube1')

# # create a new 'Glyph'
# glyph1 = Glyph(registrationName='Glyph1', Input=tube1,
#     GlyphType='Arrow')
# glyph1.OrientationArray = ['POINTS', 'Normals']
# glyph1.ScaleArray = ['POINTS', 'AngularVelocity']
# glyph1.ScaleFactor = 0.06080199480056763
# glyph1.GlyphTransform = 'Transform2'




        

    def tube(self):
        # create a new 'Tube'
        tube1 = Tube(registrationName='tube1', Input=self.streamlines)
        tube1.Scalars = ['POINTS', 'AngularVelocity']
        tube1.Vectors = ['POINTS', 'Momentum']
        tube1.Radius = 0.002
        tube1.RadiusFactor = 1.0
        tube1Display = Show(tube1)
        # tube1 = FindSource('tube1')


    def streamtracer(self):
        streamlines = StreamTracer(registrationName='streamlines', Input=self.donut, SeedType='Line')
        streamlines.Vectors = ['POINTS', 'Momentum']
        streamlines.SeedType.Point1 = [self.axial_bounds[0], self.y_bounds[0], self.z_bounds[1]]
        streamlines.SeedType.Point2 = [self.axial_bounds[0], self.y_bounds[1], self.z_bounds[1]]
        streamlinesDisplay = Show(streamlines)
        ColorBy(streamlinesDisplay, ('POINTS', 'Momentum', 'Magnitude'))
        self.streamlines=streamlines
        print(streamlines.ListProperties())
      




    def create_donut(self):
        # h = self.IN["WEDGE"][0]               # Import rotation angle
        h = 1               # Import rotation angle
        angle = 90                            # Define donut angle
        donut_list = [self.flowvtu]

        for i in range(1,angle):
            T = Transform(Input=self.flowvtu)
            T.Transform = 'Transform'
            # Properties modified on transform1.Transform
            T.Transform.Rotate = [h*i, 0.0, 0.0]
            donut_list.append(T)

      
        donut = AppendDatasets(registrationName='donut', Input=donut_list)
        self.donut = donut
        donutDisplay = GetDisplayProperties(donut)
        donutDisplay.SetRepresentationType('Feature Edges')
        

    def get_coords(self):
        coords = Calculator(Input=self.flowvtu)
        coords.ResultArrayName = 'coordinates'
        coords.Function = 'coords'

        boundsfunction = ProgrammableFilter(Input=coords)
        boundsfunction.OutputDataSetType = 'vtkTable'
        boundsfunction.Script = """import numpy as np 
coords = inputs[0].PointData["coordinates"]
    
x, y, z = coords[:, 0], coords[:, 1], coords[:, 2]

x_min, y_min, z_min = min(x), min(y), min(z)
x_max, y_max, z_max = max(x), max(y), max(z)

x_bounds = np.array([x_min, x_max])
y_bounds = np.array([y_min, y_max])
z_bounds = np.array([z_min, z_max])
output.RowData.append(x_bounds, "x_bounds")
output.RowData.append(y_bounds, "y_bounds")
output.RowData.append(z_bounds, "z_bounds")"""

        bounds_data = paraview.servermanager.Fetch(boundsfunction)
        
        self.axial_bounds = [bounds_data.GetRowData().GetArray("x_bounds").GetValue(0),
                        bounds_data.GetRowData().GetArray("x_bounds").GetValue(1)]
        z_bounds = [bounds_data.GetRowData().GetArray("z_bounds").GetValue(0),
                        bounds_data.GetRowData().GetArray("z_bounds").GetValue(1)]
        y_bounds = [bounds_data.GetRowData().GetArray("y_bounds").GetValue(0),
                        bounds_data.GetRowData().GetArray("y_bounds").GetValue(1)]
        self.radial_bounds = [np.sqrt(z_bounds[0]**2 + y_bounds[0]**2), np.sqrt(z_bounds[1]**2 + y_bounds[1]**2)]
        self.y_bounds=y_bounds
        self.z_bounds=z_bounds
        Delete(boundsfunction)
        Delete(coords)
    

# Paraview_visualize(IN)
controllo=Paraview_visualize()
